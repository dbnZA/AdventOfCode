namespace AdventOfCode.Year2024

open System.Collections.Generic
open Checked
open System.IO
open AdventOfCode

module Input06 =
    let convert (lines: string[]) =
        lines |> Array.map Array.ofSeq |> Array2D.ofArrayRows

    let sample =
        """
....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#...
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/06.txt") |> Seq.toArray |> convert

    let inline stepOne (struct (struct (x, y), (struct (x', y') as direction))) =
        struct (struct (x + x', y + y'), direction)

    let inline stepBackOne (struct (struct (x, y), (struct (x', y') as direction))) =
        struct (struct (x - x', y - y'), direction)

    let inline rot90 (struct (position, struct (x', y'))) = struct (position, struct (y', -x'))

    let rec move input step state (struct (struct (x, y), _) as vector) =
        if x < 0 || y < 0 || x >= Array2D.length1 input || y >= Array2D.length2 input then
            Some state
        elif input[x, y] = '#' then
            move input step state (vector |> stepBackOne |> rot90 |> stepOne)
        else
            match step state vector with
            | Some state -> move input step state (stepOne vector)
            | None -> None

open Input06

type Problem06a() =
    let calculate input =
        let step seen (struct (position, _)) = Some(position :: seen)
        let x, y = input |> Array2D.findByRow '^'
        let (Some positions) = move input step [] ((x, y), (-1, 0))
        positions |> List.distinct |> List.length

    do ``assert`` 41 (calculate sample)

    member _.answer = calculate data

type Problem06b() =
    let calculate (input: _[,]) =
        let visited = HashSet()
        let x, y = input |> Array2D.findByRow '^'
        let start = struct (struct (x, y), struct (-1, 0))

        let step obstructions (struct (struct (x, y) as position, _) as vector) =
            let step (positions: HashSet<_>) position =
                if positions.Add(position) then Some positions else None

            let obstructions =
                if input[x, y] <> 'X' then
                    input[x, y] <- '#'

                    match move input step visited vector with
                    | None -> position :: obstructions
                    | _ -> obstructions
                else
                    obstructions

            input[x, y] <- 'X'
            visited.Clear()
            Some obstructions

        let (Some obstructions) = move (Array2D.copy input) step [] start
        obstructions |> List.distinct |> List.countIf ((<>) (x, y))

    do ``assert`` 6 (calculate sample)

    member _.answer = calculate data
