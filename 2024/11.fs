namespace AdventOfCode.Year2024

open System
open Checked
open System.IO
open AdventOfCode

module Input11 =
    let convert (lines: string[]) =
        lines |> Array.exactlyOne |> _.Split(' ') |> Array.map Int64.Parse

    let sample =
        """
125 17
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/11.txt") |> Seq.toArray |> convert

    let rec log10' n x =
        if x = 0L then n else log10' (n + 1) (x / 10L)

    let rec pow10' n x =
        if n = 0 then x else pow10' (n - 1) (x * 10L)

open Input11

type Problem11a() =
    let calculate input =
        let blink number =
            if number = 0L then
                [ 1L ]
            else
                let n = log10' 0 number

                if n % 2 = 0 then
                    let n10 = pow10' (n / 2) 1L
                    [ number / n10; number % n10 ]
                else
                    [ number * 2024L ]

        let repeat n f x =
            if n = 0 then x else repeat (n - 1) f (f x)

        input |> List.ofArray |> repeat 25 (List.collect blink) |> List.length

    do ``assert`` 55312 (calculate sample)

    member _.answer = calculate data

type Problem11b() =
    let calculate repeat input =
        let countBlink =
            memorize (fun countBlink (struct (repeat, number)) ->
                if repeat = 0 then
                    1L
                elif number = 0L then
                    countBlink (repeat - 1, 1L)
                else
                    let n = log10' 0 number

                    if n % 2 = 0 then
                        let n10 = pow10' (n / 2) 1L
                        countBlink (repeat - 1, number / n10) + countBlink (repeat - 1, number % n10)
                    else
                        countBlink (repeat - 1, number * 2024L))

        input |> Array.sumBy (fun number -> countBlink (repeat, number))

    do ``assert`` 55312L (calculate 25 sample)

    member _.answer = calculate 75 data
