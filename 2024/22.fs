namespace AdventOfCode.Year2024

open Checked
open System
open System.IO
open AdventOfCode

module Input22 =
    let convert (lines: string[]) = lines |> Array.map Int64.Parse

    let sampleA =
        """
1
10
100
2024
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let sampleB =
        """
1
2
3
2024
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/22.txt") |> Seq.toArray |> convert

    let next secret =
        let inline mix secret number = secret ^^^ number
        let inline prune n = n % 16777216L
        let secret = secret * 64L |> mix secret |> prune
        let secret = secret / 32L |> mix secret |> prune
        let secret = secret * 2048L |> mix secret |> prune
        secret

open Input22

type Problem22a() =
    let calculate input =
        let rec repeat n secret =
            if n = 0 then secret else repeat (n - 1) (next secret)

        input |> Array.sumBy (repeat 2000)


    do ``assert`` 37327623L (calculate sampleA)

    member _.answer = calculate data

type Problem22b() =
    let calculate input =
        let strategies secret =
            let prices =
                (secret, [| 0..2000 |])
                ||> Array.mapFold (fun secret _ -> secret % 10L, next secret)
                |> fst

            let diffs = prices |> Array.pairwise |> Array.map ((<||) (-))

            [| for i = 3 to diffs.Length - 1 do
                   struct (diffs[i - 3], diffs[i - 2], diffs[i - 1], diffs[i]), prices[i + 1] |]
            |> Array.distinctBy fst

        input
        |> Array.collect strategies
        |> Array.groupBy fst
        |> Array.maxOf (snd >> Array.sumBy snd)

    do ``assert`` 23L (calculate sampleB)

    member _.answer = calculate data
