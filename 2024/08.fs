namespace AdventOfCode.Year2024

open System.IO
open AdventOfCode

module Input08 =
    let convert (lines: string[]) =
        lines |> Array.map Array.ofSeq |> Array2D.ofArrayRows

    let sample =
        """
............
........0...
.....0......
.......0....
....0.......
......A.....
............
............
........A...
.........A..
............
............
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/08.txt") |> Seq.toArray |> convert

    let countAntinodes input antinodes =
        let antennas =
            input
            |> Array2D.mapi (fun x y c -> c, (x, y))
            |> Array2D.toRows
            |> Array.concat
            |> Array.filter (fst >> (<>) '.')

        antennas
        |> Array.groupBy fst
        |> Array.collect (fun (_, antennas) ->
            Array.allPairs antennas antennas
            |> Array.map (fun ((_, a1), (_, a2)) -> a1, a2)
            |> Array.collect antinodes)
        |> Array.distinct
        |> Array.countIf (fun (x, y) -> x >= 0 && y >= 0 && x < Array2D.length1 input && y < Array2D.length2 input)

open Input08

type Problem08a() =
    let calculate input =
        let antinodes (x1, y1 as a1, (x2, y2 as a2)) =
            if a1 = a2 then
                [||]
            else
                let x' = x2 - x1
                let y' = y2 - y1
                [| x2 + x', y2 + y'; x1 - x', y1 - y' |]

        countAntinodes input antinodes

    do ``assert`` 14 (calculate sample)

    member _.answer = calculate data

type Problem08b() =
    let calculate input =
        let rec seq (x, y) (x', y' as delta) coords =
            let x = x + x'
            let y = y + y'

            if x < 0 || x >= Array2D.length1 input || y < 0 || y >= Array2D.length2 input then
                Array.ofList coords
            else
                let coord = x, y
                seq coord delta (coord :: coords)

        let antinodes (x1, y1 as a1, (x2, y2 as a2)) =
            if a1 = a2 then
                [||]
            else
                let x' = x2 - x1
                let y' = y2 - y1
                Array.append (seq a2 (x', y') [ a2 ]) (seq a1 (-x', -y') [ a1 ])

        countAntinodes input antinodes

    do ``assert`` 34 (calculate sample)

    member _.answer = calculate data
