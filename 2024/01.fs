namespace AdventOfCode.Year2024

open System
open System.IO
open AdventOfCode

module Input01 =
    let (|Int|_|) (value: string) =
        match Int32.TryParse(value) with
        | true, value -> Some value
        | false, _ -> None

    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            match line.Split(" ", StringSplitOptions.RemoveEmptyEntries) with
            | [| Int x; Int y |] -> (x, y)
            | other -> failwith $"Unmatched input: {other}")
        |> Array.unzip

    let sample =
        """
3   4
4   3
2   5
1   3
3   9
3   3
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/01.txt") |> Seq.toArray |> convert

open Input01

type Problem01a() =
    let calculate (left, right) =
        Array.zip (left |> Array.sort) (right |> Array.sort)
        |> Array.sumBy (fun (x, y) -> abs (x - y))

    do ``assert`` 11 (calculate sample)

    member _.answer = calculate data

type Problem01b() =
    let calculate (left, right) =
        let groups = right |> Array.countBy id |> Map.ofArray

        left
        |> Array.sumBy (fun x -> x * (groups |> Map.tryFind x |> Option.defaultValue 0))


    do ``assert`` 31 (calculate sample)

    member _.answer = calculate data
