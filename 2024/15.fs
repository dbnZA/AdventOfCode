namespace AdventOfCode.Year2024

open Checked
open System
open System.IO
open AdventOfCode

module Input15 =
    type Move =
        | Answer of bool
        | TryMove of (int coord * int coord) list

    let convert (lines: string[]) =
        let split = lines |> Array.findIndex ((=) "")
        let map = lines[.. split - 1] |> Array.map Array.ofSeq |> Array2D.ofArrayRows
        let moves = lines[split + 1 ..] |> Array.map List.ofSeq |> List.concat
        map, moves

    let sample =
        """
<<<<<<< HEAD
########
#..O.O.#
##@.O..#
#...O..#
#.#.O..#
#...O..#
#......#
########

<^^>>>vv<v>>v<<
=======
##########
#..O..O.O#
#......O.#
#.OO..O.O#
#..O@..O.#
#O#..O...#
#O..O..O.#
#.OO.O.OO#
#....O...#
##########

<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^
vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<
<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><
^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^
<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>
v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^
>>>>>>> refs/remotes/origin/master
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/15.txt") |> Seq.toArray |> convert

open Input15

type Problem15a() =
    let calculate (map, moves) =
        let rec canMoveStack n coord direction =
            let coord = coord + direction

            match Coord.Array2D.get map coord with
            | '.' -> Some n
            | 'O' -> canMoveStack (n + 1) coord direction
            | '#' -> None

        let rec moveStack n coord direction =
            if n = 0 then
                ()
            else
                let coord' = coord + direction
                Coord.Array2D.get map coord' |> Coord.Array2D.set map coord
                moveStack (n - 1) coord' direction

        let rec move coord =
            function
            | direction :: moves ->
                let direction = Coord.parseCardinalVector direction

                let coord =
                    match canMoveStack 1 coord direction with
                    | Some n ->
                        moveStack n (coord + direction * n) -direction
                        Coord.Array2D.set map coord '.'
                        coord + direction
                    | None -> coord

                move coord moves
            | [] ->
                map
                |> Array2D.mapi (fun x y i ->
                    if i = 'O' then y * 100 + x
                    else 0)
                |> Array2D.sum

        let coord = map |> Array2D.findByRow '@' |> Coord.ofTuple
        move coord moves

    do ``assert`` 2028 (calculate sample)

    member _.answer = calculate data

type Problem15b() =
    let calculate (map, moves) =
        let map =
            map
            |> Array2D.toRows
            |> Array.map (
                Array.collect (function
                    | '#' -> [| '#'; '#' |]
                    | 'O' -> [| '['; ']' |]
                    | '.' -> [| '.'; '.' |]
                    | '@' -> [| '@'; '.' |])
            )
            |> Array2D.ofArrayRows

        let rec moveHorizontal coord direction =
            let coord' = coord + direction

            match Coord.Array2D.get map coord with
            | '.' -> true
            | '['
            | ']'
            | '@' as v when moveHorizontal coord' direction ->
                Coord.Array2D.set map coord' v
                Coord.Array2D.set map coord '.'
                true
            | _ -> false

        let rec moveVertical doMove direction (left, right) =
            match map[left.Y, left.X .. right.X] with
            | [| '#' |]
            | [| '#'; _ |]
            | [| _; '#' |] -> Answer false
            | [| '.' |]
            | [| '.'; '.' |] -> Answer true
            | [| '@' |]
            | [| '['; ']' |] -> TryMove [ left, right ]
            | [| '[' |] -> TryMove [ left, Coord.moveRight right ]
            | [| ']' |] -> TryMove [ Coord.moveLeft left, right ]
            | [| ']'; '[' |] -> TryMove [ Coord.moveLeft left, left; right, Coord.moveRight right ]
            | [| ']'; '.' |] -> TryMove [ Coord.moveLeft left, left ]
            | [| '.'; '[' |] -> TryMove [ right, Coord.moveRight right ]
            |> function
                | Answer canMove -> canMove
                | TryMove moves ->
                    let canMove =
                        moves
                        |> List.forall (fun (left, right) ->
                            moveVertical doMove direction (left + direction, right + direction))

                    if doMove && canMove then
                        moves
                        |> List.iter (fun (left, right) ->
                            let left' = left + direction
                            let right' = right + direction
                            map[left'.Y, left'.X .. right'.X] <- map[left.Y, left.X .. right.X]
                            map[left.Y, left.X .. right.X] <- Array.create (right.X - left.X + 1) '.')

                    canMove

        let rec move coord =
            function
            | direction :: moves ->
                let direction = direction |> Coord.parseCardinalVector

                match direction with
                | { X = 0 } when moveVertical false direction (coord, coord) ->
                    moveVertical true direction (coord, coord) |> ignore
                    move (coord + direction) moves
                | { Y = 0 } when moveHorizontal coord direction -> move (coord + direction) moves
                | _ -> move coord moves
            | [] ->
                map |> Array2D.toRows |> Array.iter (String >> printfn "%s")

                map
                |> Array2D.mapi (fun y x v -> if v = '[' then 100 * y + x else 0)
                |> Array2D.sum

        let coord = map |> Coord.Array2D.findByRow '@'
        move coord moves

    do ``assert`` 9021 (calculate sample)

    member _.answer = calculate data
