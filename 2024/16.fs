namespace AdventOfCode.Year2024

open System.Collections.Generic
open Checked
open System
open System.IO
open AdventOfCode

module Input16 =
    let convert (lines: string[]) =
        lines |> Array.map Array.ofSeq |> Array2D.ofArrayRows

    let sample =
        """
#################
#...#...#...#..E#
#.#.#.#.#.#.#.#.#
#.#.#.#...#...#.#
#.#.#.#.###.#.#.#
#...#.#.#.....#.#
#.#.#.#.#.#####.#
#.#...#.#.#.....#
#.#.#####.#.###.#
#.#.#.......#...#
#.#.###.#####.###
#.#.#...#.....#.#
#.#.#.#####.###.#
#.#.#.........#.#
#.#.#.#########.#
#S#.............#
#################
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/16.txt") |> Seq.toArray |> convert

    let inline toIndex coord =
        match coord with
        | { X = -1; Y = 0 } -> 0
        | { X = 1; Y = 0 } -> 1
        | { X = 0; Y = -1 } -> 2
        | { X = 0; Y = 1 } -> 3

    let inline toCoord index =
        match index with
        | 0 -> { X = -1; Y = 0 }
        | 1 -> { X = 1; Y = 0 }
        | 2 -> { X = 0; Y = -1 }
        | 3 -> { X = 0; Y = 1 }

    let rec move turns input bestScore =
        function
        | [], [] -> bestScore
        | nextPositions, [] -> move (turns + 1) input bestScore ([], nextPositions)
        | nextPositions, (struct (_, _, score) as position :: positions) when score / 1_000 > turns ->
            move turns input bestScore (position :: nextPositions, positions)
        | nextPositions, struct (coord, ({ X = x; Y = y } as direction), score) :: positions ->
            let positions =
                if Array.get (Coord.Array2D.get bestScore coord) (toIndex direction) > score then
                    Array.set (Coord.Array2D.get bestScore coord) (toIndex direction) score

                    let positions =
                        if Coord.Array2D.get input (coord + direction) <> '#' then
                            struct (coord + direction, direction, score + 1) :: positions
                        else
                            positions

                    let nextPositions =
                        struct (coord, { X = y; Y = x }, score + 1_000)
                        :: struct (coord, { X = -y; Y = -x }, score + 1_000)
                        :: nextPositions
                    nextPositions, positions
                else
                    nextPositions, positions

            move turns input bestScore positions

open Input16

type Problem16a() =
    let calculate input =
        let start = input |> Coord.Array2D.findByRow 'S'
        let ``end`` = input |> Coord.Array2D.findByRow 'E'
        let bestScore = input |> Array2D.map (fun _ -> Array.create 4 Int32.MaxValue)
        let bestScore = move 0 input bestScore ([], [ start, { X = 1; Y = 0 }, 0 ])
        Array.min (Coord.Array2D.get bestScore ``end``)

    do ``assert`` 11_048 (calculate sample)

    member _.answer = calculate data

type Problem16b() =
    let calculate input =
        let rec moves bestScore (paths: HashSet<_>) =
            function
            | [] -> paths.Count
            | (coord, direction) :: visiting when not (paths.Contains(coord)) ->
                let score = Coord.Array2D.get bestScore coord |> Array.min

                let toVisit =
                    Coord.cardinalVectors
                    |> List.choose (fun direction' ->
                        let coord' = coord + direction'

                        if Coord.Array2D.get input coord' <> '#' then
                            let score' = Coord.Array2D.get bestScore coord' |> Array.min

                            if
                                score' + 1 = score
                                || score' + 1001 = score
                                || (direction = direction' && score' - 999 = score)
                            then
                                Some(coord', direction')
                            else
                                None
                        else
                            None)

                paths.Add(coord) |> ignore
                moves bestScore paths (toVisit @ visiting)
            | _ :: visiting -> moves bestScore paths visiting

        let start = input |> Coord.Array2D.findByRow 'S'
        let ``end`` = input |> Coord.Array2D.findByRow 'E'
        let bestScore = input |> Array2D.map (fun _ -> Array.create 4 Int32.MaxValue)
        let bestScore = move 0 input bestScore ([], [ start, { X = 1; Y = 0 }, 0 ])
        moves bestScore (HashSet()) [ ``end``, Coord.origin ]

    do ``assert`` 64 (calculate sample)

    member _.answer = calculate data
