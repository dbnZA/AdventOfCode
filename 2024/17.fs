namespace AdventOfCode.Year2024

open System.Reflection.Emit
open Checked
open System
open System.IO
open AdventOfCode

#nowarn "FS0104"

module Input17 =
    type Register(registers: int64[]) =
        member _.A
            with get () = registers[0]
            and set value = registers[0] <- value

        member _.B
            with get () = registers[1]
            and set value = registers[1] <- value

        member _.C
            with get () = registers[2]
            and set value = registers[2] <- value

        member _.Item
            with get index = registers[index]

    [<Struct>]
    type Action =
        | Next
        | Jump of Index: int
        | Output of Value: int8

    type OpCode =
        | adv = 0y
        | bxl = 1y
        | bst = 2y
        | jnz = 3y
        | bxc = 4y
        | out = 5y
        | bdv = 6y
        | cdv = 7y

    let convert (lines: string[]) =
        let registers =
            lines[0..2] |> Array.map (fun line -> Int64.Parse(line.Split(' ')[2]))

        let program = (lines[4].Split(' ')[1]).Split(',') |> Array.map SByte.Parse
        registers, program

    let sampleA =
        """
Register A: 729
Register B: 0
Register C: 0

Program: 0,1,5,4,3,0
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let sampleB =
        """
Register A: 2024
Register B: 0
Register C: 0

Program: 0,3,5,4,3,0
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/17.txt") |> Seq.toArray |> convert

open Input17

type Problem17a() =
    let comboOperand (registers: Register) =
        function
        | 0y
        | 1y
        | 2y
        | 3y as v -> int64 v
        | 4y
        | 5y
        | 6y as v -> registers[int32 v - 4]

    let rec tick program doOutput (regs: Register) state pointer =
        if pointer < 0 || pointer >= Array.length program then
            state
        else
            let opcode = program[pointer]
            let operand = program[pointer + 1]

            match LanguagePrimitives.EnumOfValue opcode with
            | OpCode.adv ->
                regs.A <- regs.A / (1L <<< int (comboOperand regs operand))
                Next
            | OpCode.bxl ->
                regs.B <- regs.B ^^^ int64 operand
                Next
            | OpCode.bst ->
                regs.B <- comboOperand regs operand % 8L
                Next
            | OpCode.jnz -> if regs.A = 0 then Next else Jump(int32 operand)
            | OpCode.bxc ->
                regs.B <- regs.B ^^^ regs.C
                Next
            | OpCode.out -> Output(int8 (comboOperand regs operand % 8L))
            | OpCode.bdv ->
                regs.B <- regs.A / (1L <<< int (comboOperand regs operand))
                Next
            | OpCode.cdv ->
                regs.C <- regs.A / (1L <<< int (comboOperand regs operand))
                Next
            |> function
                | Next -> tick program doOutput regs state (pointer + 2)
                | Jump pointer -> tick program doOutput regs state pointer
                | Output value ->
                    let struct (state, ``continue``) = doOutput state value

                    if ``continue`` then
                        tick program doOutput regs state (pointer + 2)
                    else
                        state

    let calculate (registers, program) =
        let saveOutput output value = struct (value :: output, true)

        //(compile program).Invoke(registers, [], saveOutput)
        tick program saveOutput (Register(registers)) [] 0
        |> List.rev
        |> List.map _.ToString()
        |> String.concat ","

    do ``assert`` "4,6,3,5,6,3,5,2,1,0" (calculate sampleA)

    member _.answer = calculate data

type Problem17b() =
    let compile program =
        let builder =
            DynamicMethod("AoC202417", typeof<int32>, [| typeof<int64[]>; typeof<int8[]> |])

        let il = builder.GetILGenerator()
        let regA = il.DeclareLocal(typeof<int64>)
        let regB = il.DeclareLocal(typeof<int64>)
        let regC = il.DeclareLocal(typeof<int64>)
        let index = il.DeclareLocal(typeof<int32>)
        let ret = il.DefineLabel()
        let retFail = il.DefineLabel()
        let jumpTable = program |> Array.map (fun _ -> il.DefineLabel())
        il.Emit(OpCodes.Ldarg_0)
        il.Emit(OpCodes.Ldc_I4_0)
        il.Emit(OpCodes.Ldelem_I8)
        il.Emit(OpCodes.Stloc, regA)
        il.Emit(OpCodes.Ldarg_0)
        il.Emit(OpCodes.Ldc_I4_1)
        il.Emit(OpCodes.Ldelem_I8)
        il.Emit(OpCodes.Stloc, regB)
        il.Emit(OpCodes.Ldarg_0)
        il.Emit(OpCodes.Ldc_I4_2)
        il.Emit(OpCodes.Ldelem_I8)
        il.Emit(OpCodes.Stloc, regC)
        il.Emit(OpCodes.Ldc_I4_0)
        il.Emit(OpCodes.Stloc, index)

        let comboOperand =
            function
            | 0y
            | 1y
            | 2y
            | 3y as v -> il.Emit(OpCodes.Ldc_I8, int64 v)
            | 4y -> il.Emit(OpCodes.Ldloc, regA)
            | 5y -> il.Emit(OpCodes.Ldloc, regB)
            | 6y -> il.Emit(OpCodes.Ldloc, regC)

        let rec emit pointer =
            if pointer < 0 || pointer >= Array.length program then
                ()
            else
                let opcode = program[pointer]
                let operand = program[pointer + 1]

                il.MarkLabel(jumpTable[pointer])

                match LanguagePrimitives.EnumOfValue opcode with
                | OpCode.adv ->
                    il.Emit(OpCodes.Ldloc, regA)
                    comboOperand operand
                    il.Emit(OpCodes.Conv_Ovf_I4)
                    il.Emit(OpCodes.Shr)
                    il.Emit(OpCodes.Stloc, regA)
                | OpCode.bxl ->
                    il.Emit(OpCodes.Ldloc, regB)
                    comboOperand operand
                    il.Emit(OpCodes.Xor)
                    il.Emit(OpCodes.Stloc, regB)
                | OpCode.bst ->
                    comboOperand operand
                    il.Emit(OpCodes.Ldc_I8, 8L)
                    il.Emit(OpCodes.Rem)
                    il.Emit(OpCodes.Stloc, regB)
                | OpCode.jnz ->
                    il.Emit(OpCodes.Ldloc, regA)
                    il.Emit(OpCodes.Ldc_I8, 0L)
                    il.Emit(OpCodes.Ceq)
                    il.Emit(OpCodes.Brfalse, jumpTable[int operand])
                | OpCode.bxc ->
                    il.Emit(OpCodes.Ldloc, regB)
                    il.Emit(OpCodes.Ldloc, regC)
                    il.Emit(OpCodes.Xor)
                    il.Emit(OpCodes.Stloc, regB)
                | OpCode.out ->
                    comboOperand operand
                    il.Emit(OpCodes.Ldc_I8, 8L)
                    il.Emit(OpCodes.Rem)
                    il.Emit(OpCodes.Ldarg_1)
                    il.Emit(OpCodes.Ldloc, index)
                    il.Emit(OpCodes.Ldelem_I1)
                    il.Emit(OpCodes.Conv_I8)
                    il.Emit(OpCodes.Ceq)
                    il.Emit(OpCodes.Brfalse, retFail)
                    il.Emit(OpCodes.Ldloc, index)
                    il.Emit(OpCodes.Ldc_I4_1)
                    il.Emit(OpCodes.Add)
                    il.Emit(OpCodes.Stloc, index)
                | OpCode.bdv ->
                    il.Emit(OpCodes.Ldloc, regA)
                    comboOperand operand
                    il.Emit(OpCodes.Conv_Ovf_I4)
                    il.Emit(OpCodes.Shr)
                    il.Emit(OpCodes.Stloc, regB)
                | OpCode.cdv ->
                    il.Emit(OpCodes.Ldloc, regA)
                    comboOperand operand
                    il.Emit(OpCodes.Shr)
                    il.Emit(OpCodes.Stloc, regC)
                | _ -> ()

                emit (pointer + 2)

        emit 0
        il.Emit(OpCodes.Br, ret)
        il.MarkLabel(retFail)
        il.Emit(OpCodes.Ldc_I4_M1)
        il.Emit(OpCodes.Stloc, index)
        il.MarkLabel(ret)
        il.Emit(OpCodes.Ldloc, index)
        il.Emit(OpCodes.Ret)
        builder.CreateDelegate<Func<int64[], int8[], int32>>()

    let calculate (registers: int64[], program) =
        let program' = compile program

        let rec findProgram program registerA =
            registers[0] <- registerA

            if program'.Invoke(registers, program) = program.Length then
                registerA
            else
                findProgram program (registerA + 1L)

        let rec iterProgram program n registerA =
            if n = Array.length program then
                registerA
            else
                findProgram program[^n..] (registerA <<< 3) |> iterProgram program (n + 1)

        iterProgram program 0 0L

    do ``assert`` 117440L (calculate sampleB)

    member _.answer = calculate data
