namespace AdventOfCode.Year2024

open System
open System.IO
open AdventOfCode
open Checked

#nowarn "FS0040"

module Input21 =
    let numericKeypad =
        [| [| '7'; '8'; '9' |]
           [| '4'; '5'; '6' |]
           [| '1'; '2'; '3' |]
           [| '.'; '0'; 'A' |] |]
        |> Array2D.ofArrayRows

    let directionalKeypad =
        [| [| '.'; '^'; 'A' |]; [| '<'; 'v'; '>' |] |] |> Array2D.ofArrayRows

    let coordA = directionalKeypad |> Coord.Array2D.findByRow 'A'

    let convert (lines: string[]) = lines

    let sample =
        """
029A
980A
179A
456A
379A
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/21.txt") |> Seq.toArray |> convert

    let vertical { X = x; Y = _ } =
        if x < 0 then
            [ for _ in 1 .. -x -> '<' ]
        else
            [ for _ in 1..x -> '>' ]

    let horizontal { X = _; Y = y } =
        if y < 0 then
            [ for _ in 1 .. -y -> '^' ]
        else
            [ for _ in 1..y -> 'v' ]

    let directions =
        memorize (fun _ (keypad, button, coord) ->
            let endCoord = keypad |> Coord.Array2D.findByRow button
            let v = endCoord - coord

            [ if Coord.Array2D.get keypad (coord + { v with Y = 0 }) <> '.' then
                  [ yield! vertical v; yield! horizontal v; 'A' ]
              if v.Y <> 0 && Coord.Array2D.get keypad (coord + { v with X = 0 }) <> '.' then
                  [ yield! horizontal v; yield! vertical v; 'A' ] ],
            endCoord)

    let rec plan =
        memorize (fun _ (keypad, n, button, coord) ->
            let seq, coord = directions (keypad, button, coord)

            let length =
                if n = 0 then
                    seq |> List.minOf List.length |> int64
                else
                    seq |> List.minOf (fun seq -> plans (directionalKeypad, n - 1, seq, coordA))

            length, coord)

    and plans (keypad, n, seq, coord) =
        ((0L, coord), seq)
        ||> Seq.fold (fun (count, coord) button ->
            let count', coord = plan (keypad, n, button, coord)
            count + count', coord)
        |> fst

    let complexity n input =
        let coordA = numericKeypad |> Coord.Array2D.findByRow 'A'

        input
        |> Array.sumBy (fun (seq: string) ->
            plans (numericKeypad, n, List.ofSeq seq, coordA)
            |> ((*) (Int64.Parse(seq[..^1]))))

open Input21

type Problem21a() =
    let calculate = complexity 2

    do ``assert`` 126384L (calculate sample)

    member _.answer = calculate data

type Problem21b() =
    let calculate = complexity 25

    member _.answer = calculate data
