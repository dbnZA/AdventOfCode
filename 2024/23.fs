namespace AdventOfCode.Year2024

open System.IO
open AdventOfCode

module Input23 =
    let (|Split|) (sep: string) (value: string) = value.Split(sep)

    let convert (lines: string[]) =
        let input =
            lines
            |> Array.map (function
                | Split "-" [| a; b |] -> a, b)

        Array.append input (input |> Array.map (fun (a, b) -> b, a))
        |> Array.groupBy fst
        |> Array.map (fun (a, x) -> a, x |> Array.map snd |> List.ofArray)
        |> Map.ofArray

    let sample =
        """
kh-tc
qp-kh
de-cg
ka-co
yn-aq
qp-ub
cg-tb
vc-aq
tb-ka
wh-tc
yn-cg
kh-ub
ta-co
de-co
tc-td
tb-wq
wh-td
ta-ka
td-qp
aq-cg
wq-ub
ub-vc
de-ta
wq-aq
wq-vc
wh-yn
ka-de
kh-ta
co-tc
wh-qp
tb-vc
td-yn
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/23.txt") |> Seq.toArray |> convert

open Input23

type Problem23a() =
    let calculate connections =
        let findCycle node1 =
            [ let links = connections |> Map.find node1

              for node2 in links do
                  for node3 in connections |> Map.find node2 do
                      if links |> List.contains node3 then
                          Set.ofArray [| node1; node2; node3 |] ]

        connections
        |> Map.keys
        |> Seq.collect findCycle
        |> Seq.distinct
        |> Seq.filter (Set.exists (fun (node: string) -> node[0] = 't'))
        |> Seq.length

    do ``assert`` 7 (calculate sample)

    member _.answer = calculate data

type Problem23b() =
    let calculate connections =
        let rec traverse seen common node =
            let seen' = seen |> Set.add node

            let common =
                node :: (connections |> Map.find node) |> Set.ofList |> Set.intersect common

            if Set.isSubset seen' common then
                let next = Set.difference common seen' |> Set.filter (fun next -> next > node)

                if Set.isEmpty next then
                    seen'
                else
                    next |> Seq.map (traverse seen' common) |> Seq.maxBy Set.count
            else
                seen

        connections
        |> Map.toSeq
        |> Seq.map (fun (node, next) -> traverse (Set.singleton node) (Set.ofList (node :: next)) node)
        |> Seq.maxBy Set.count
        |> Set.toList
        |> List.sort
        |> String.concat ","

    do ``assert`` "co,de,ka,ta" (calculate sample)

    member _.answer = calculate data
