namespace AdventOfCode.Year2024

open System.Text.RegularExpressions
open System
open System.IO
open AdventOfCode

module Input03 =
    type Inst =
        | Do
        | Dont
        | Mul of int * int

    let convert (lines: string[]) =
        let line = String.concat "" lines

        Regex.Matches(line, @"(?<inst>don't)\(\)|(?<inst>do)\(\)|(?<inst>mul)\((?<left>\d+),(?<right>\d+)\)")
        |> Seq.map (fun m ->
            match m.Groups["inst"].Value with
            | "don't" -> Dont
            | "do" -> Do
            | "mul" -> Mul(Int32.Parse(m.Groups["left"].Value), Int32.Parse(m.Groups["right"].Value)))

    let sample =
        """
xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/03.txt") |> Seq.toArray |> convert

open Input03

type Problem03a() =
    let calculate input =
        input
        |> Seq.sumBy (function
            | Mul(x, y) -> x * y
            | _ -> 0)

    do ``assert`` 161 (calculate sample)

    member _.answer = calculate data

type Problem03b() =
    let calculate input =
        ((0, true), input)
        ||> Seq.fold (fun (sum, state) inst ->
            match inst with
            | Do -> sum, true
            | Dont -> sum, false
            | Mul(x, y) when state = true -> sum + x * y, state
            | Mul _ when state = false -> sum, state)
        |> fst

    do ``assert`` 48 (calculate sample)

    member _.answer = calculate data
