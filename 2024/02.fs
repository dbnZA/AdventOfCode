namespace AdventOfCode.Year2024

open System
open System.IO
open AdventOfCode

module Input02 =
    let convert (lines: string[]) =
        lines |> Array.map (fun line -> line.Split(' ') |> Array.map Int32.Parse)

    let sample =
        """
7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/02.txt") |> Seq.toArray |> convert

open Input02

type Problem02a() =
    let calculate input =
        let isSafe line =
            let line = line |> Array.pairwise

            line |> Array.forall (fun (x, y) -> y - x >= 1 && y - x <= 3)
            || (line |> Array.forall (fun (x, y) -> y - x <= -1 && y - x >= -3))

        input |> Array.countIf isSafe

    do ``assert`` 2 (calculate sample)

    member _.answer = calculate data

type Problem02b() =
    let calculate input =
        let rec isSafe upDown skipped m n line =
            if Array.length line <= n then
                true
            else
                let x = line[m]
                let y = line[n]

                if (y - x) * upDown >= 1 && (y - x) * upDown <= 3 then
                    isSafe upDown skipped n (n + 1) line
                elif skipped then
                    if m = 0 then isSafe upDown true (m + 1) n line else false
                else
                    isSafe upDown true m (n + 1) line

        input
        |> Array.countIf (fun line -> isSafe 1 false 0 1 line || isSafe -1 false 0 1 line)

    do ``assert`` 4 (calculate sample)

    member _.answer = calculate data
