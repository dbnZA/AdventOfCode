namespace AdventOfCode.Year2024

open Checked
open System
open System.IO
open AdventOfCode

module Input24 =
    let (|Split|) (sep: string) (value: string) = value.Split(sep)
    let (|Bool|) (value: string) = Int64.Parse(value)

    let convert (lines: string[]) =
        let split = lines |> Array.findIndex ((=) "")

        let initialStates =
            lines[.. split - 1]
            |> Array.map (function
                | Split ": " [| wire; Bool state |] -> wire, state)

        let logicGates =
            lines[split + 1..]
            |> Array.map (function
                | Split " " [| input1; gate; input2; "->"; output |] -> (input1, input2), gate, output)
            |> List.ofArray

        initialStates, logicGates

    let sample =
        """
x00: 1
x01: 0
x02: 1
x03: 1
x04: 0
y00: 1
y01: 1
y02: 1
y03: 1
y04: 1

ntg XOR fgs -> mjb
y02 OR x01 -> tnw
kwq OR kpj -> z05
x00 OR x03 -> fst
tgd XOR rvg -> z01
vdt OR tnw -> bfw
bfw AND frj -> z10
ffh OR nrd -> bqk
y00 AND y03 -> djm
y03 OR y00 -> psh
bqk OR frj -> z08
tnw OR fst -> frj
gnj AND tgd -> z11
bfw XOR mjb -> z00
x03 OR x00 -> vdt
gnj AND wpb -> z02
x04 AND y00 -> kjc
djm OR pbm -> qhw
nrd AND vdt -> hwm
kjc AND fst -> rvg
y04 OR y02 -> fgs
y01 AND x02 -> pbm
ntg OR kjc -> kwq
psh XOR fgs -> tgd
qhw XOR tgd -> z09
pbm OR djm -> kpj
x03 XOR y03 -> ffh
x00 XOR y04 -> ntg
bfw OR bqk -> z06
nrd XOR fgs -> wpb
frj XOR qhw -> z04
bqk OR frj -> z07
y03 OR x01 -> nrd
hwm AND bqk -> z03
tgd XOR rvg -> z12
tnw OR pbm -> gnj
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/24.txt") |> Seq.toArray |> convert

open Input24

type Problem24a() =
    let calculate (initialStates, logicGates) =
        let rec propagate states =
            function
            | [], [] ->
                states
                |> Map.toList
                |> List.filter (fun (wire: string, _) -> wire.StartsWith("z"))
                |> List.sortDescending
                |> List.fold (fun number (_, state) ->
                    (number <<< 1) + state) 0L
            | deferredGates, [] -> propagate states ([], deferredGates)
            | deferredGates, ((input1, input2), logic, output as gate)::gates ->
                match states |> Map.tryFind input1, states |> Map.tryFind input2 with
                | Some state1, Some state2 ->
                    let states =
                        states
                        |> Map.add output
                            (match logic with
                             | "AND" -> state1 &&& state2
                             | "OR" -> state1 ||| state2
                             | "XOR" -> state1 ^^^ state2)
                    propagate states (deferredGates, gates)
                | _ -> propagate states (gate :: deferredGates, gates)

        let states = Map.ofArray initialStates

        propagate states ([], logicGates)

    do ``assert`` 2024L (calculate sample)

    member _.answer = calculate data

type Problem24b() =
    let calculate input = 0

    do ``assert`` 0 (calculate sample)

    member _.answer = calculate data
