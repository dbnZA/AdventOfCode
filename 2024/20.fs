namespace AdventOfCode.Year2024

open Checked
open System
open System.IO
open AdventOfCode

module Input20 =
    let convert (lines: string[]) =
        lines |> Array.map Array.ofSeq |> Array2D.ofArrayRows

    let sample =
        """
###############
#...#...#.....#
#.#.#.#.#.###.#
#S#...#.#.#...#
#######.#.#.###
#######.#.#...#
#######.#.###.#
###..E#...#...#
###.#######.###
#...###...#...#
#.#####.#.###.#
#.#...#.#.#...#
#.#.#.#.#.#.###
#...#...#...###
###############
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/20.txt") |> Seq.toArray |> convert

    let rec move input moveSteps steps =
        function
        | [], [] -> ()
        | nextMoves, [] -> move input moveSteps (steps + 1) ([], nextMoves)
        | nextMoves, coord :: moves ->
            match Coord.Array2D.tryGet input coord with
            | ValueSome cell when cell <> '#' && steps < Coord.Array2D.get moveSteps coord ->
                Coord.Array2D.set moveSteps coord steps
                move input moveSteps steps (Coord.cardinalPoints coord @ nextMoves, moves)
            | _ -> move input moveSteps steps (nextMoves, moves)

    let calculate offsets threshold input =
        let forwardSteps = input |> Array2D.map (fun _ -> Int32.MaxValue)
        let backSteps = input |> Array2D.map (fun _ -> Int32.MaxValue)
        let start = input |> Coord.Array2D.findByRow 'S'
        let finish = input |> Coord.Array2D.findByRow 'E'
        move input forwardSteps 0 ([], [ start ])
        move input backSteps 0 ([], [ finish ])

        let totalSteps = Coord.Array2D.get forwardSteps finish

        forwardSteps
        |> Coord.Array2D.mapi (fun coord forwardStep ->
            if forwardStep <> Int32.MaxValue then
                offsets
                |> List.countIf (fun (offset, n) ->
                    match Coord.Array2D.tryGet backSteps (coord + offset) with
                    | ValueSome backStep when backStep <> Int32.MaxValue -> totalSteps - forwardStep - backStep - n > threshold
                    | _ -> false)
            else
                0)
        |> Array2D.sum

open Input20

type Problem20a() =
    let calculate =
        Coord.cardinalVectors
        |> List.map (fun v -> v * 2, 1)
        |> calculate

    do ``assert`` 10 (calculate 10 sample)

    member _.answer = calculate 100 data

type Problem20b() =
    let calculate =
        [ for x = -20 to 20  do
              let n = 20 - abs x

              for y = -n to n do
                  { X = x; Y = y }, abs x + abs y - 1 ]
        |> calculate

    do ``assert`` 41 (calculate 70 sample)

    member _.answer = calculate 100 data
