namespace AdventOfCode.Year2024

open System.IO
open AdventOfCode

module Input19 =
    let convert (lines: string[]) = lines[0].Split(", "), lines[2..]

    let sample =
        """
r, wr, b, g, bwu, rb, gb, br

brwrr
bggr
gbbr
rrbgbr
ubwu
bwurrg
brgr
bbrgwb
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/19.txt") |> Seq.toArray |> convert

open Input19

type Problem19a() =
    let calculate (towels: string[], patterns) =
        let patterns = patterns |> Array.sortDescending

        let rec countMatch pattern =
            if pattern = "" then
                true
            else
                towels
                |> Array.exists (fun towel ->
                    if pattern.EndsWith(towel) then
                        countMatch pattern[.. ^towel.Length]
                    else
                        false)

        patterns |> Array.countIf countMatch

    do ``assert`` 6 (calculate sample)

    member _.answer = calculate data

type Problem19b() =
    let calculate (towels: string[], patterns) =
        let patterns = patterns |> Array.sortDescending

        let countMatch =
            memorize (fun countMatch pattern ->
                if pattern = "" then
                    1L
                else
                    towels
                    |> Array.sumBy (fun towel ->
                        if pattern.EndsWith(towel) then
                            countMatch pattern[.. ^towel.Length]
                        else
                            0L))

        patterns |> Array.sumBy countMatch

    do ``assert`` 16L (calculate sample)

    member _.answer = calculate data
