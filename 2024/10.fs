namespace AdventOfCode.Year2024

open Checked
open System.IO
open AdventOfCode

module Input10 =
    let convert (lines: string[]) =
        lines
        |> Array.map (Seq.map (fun (c: char) -> int c - int '0') >> Array.ofSeq)
        |> Array2D.ofArrayRows

    let sample =
        """
89010123
78121874
87430965
96549874
45678903
32019012
01329801
10456732
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/10.txt") |> Seq.toArray |> convert

open Input10

type Problem10a() =
    let calculate input =
        let heads =
            input
            |> Array2D.copy
            |> Array2D.mapi (fun x y height -> if height = 9 then [ x, y ] else [])

        for height = 9 downto 0 do
            for x = 0 to Array2D.length1 input - 1 do
                for y = 0 to Array2D.length2 input - 1 do
                    if input[x, y] = height then
                        for x', y' in input |> Array2D.permuteMoves (x, y) do
                            if input[x', y'] = height - 1 then
                                heads[x', y'] <- heads[x', y'] @ heads[x, y]

        input
        |> Array2D.mapi (fun x y height ->
            if height = 0 then
                heads[x, y] |> List.distinct |> List.length
            else
                0)
        |> Array2D.sum

    do ``assert`` 36 (calculate sample)

    member _.answer = calculate data

type Problem10b() =
    let calculate input =
        let heads =
            input
            |> Array2D.copy
            |> Array2D.mapi (fun x y height -> if height = 9 then 1 else 0)

        for height = 9 downto 0 do
            for x = 0 to Array2D.length1 input - 1 do
                for y = 0 to Array2D.length2 input - 1 do
                    if input[x, y] = height then
                        for x', y' in input |> Array2D.permuteMoves (x, y) do
                            if input[x', y'] = height - 1 then
                                heads[x', y'] <- heads[x', y'] + heads[x, y]

        input
        |> Array2D.mapi (fun x y height -> if height = 0 then heads[x, y] else 0)
        |> Array2D.sum

    do ``assert`` 81 (calculate sample)

    member _.answer = calculate data
