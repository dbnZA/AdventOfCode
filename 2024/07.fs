namespace AdventOfCode.Year2024

open System
open System.IO
open AdventOfCode

module Input07 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let line =
                line.Split([| ':'; ' ' |], StringSplitOptions.RemoveEmptyEntries)
                |> Array.map Int64.Parse

            line[0], line[1..] |> List.ofArray)

    let sample =
        """
190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/07.txt") |> Seq.toArray |> convert

open Input07

type Problem07a() =
    let calculate input =
        let rec op total x =
            function
            | [] when x = total -> true
            | [] -> false
            | _ when x > total -> false
            | y :: values -> op total (x * y) values || op total (x + y) values

        input
        |> Array.sumBy (fun (total, x :: values) -> if op total x values then total else 0L)

    do ``assert`` 3749L (calculate sample)

    member _.answer = calculate data

type Problem07b() =
    let calculate input =
        let inline (.||.) x y =
            let rec log10' n x =
                if x = 0L then n else log10' (n * 10L) (x / 10L)

            x * (log10' 1L y) + y

        let rec op total x =
            function
            | [] when x = total -> true
            | [] -> false
            | _ when x > total -> false
            | y :: values -> op total (x * y) values || op total (x + y) values || op total (x .||. y) values

        input
        |> Array.sumBy (fun (total, x :: values) -> if op total x values then total else 0L)

    do ``assert`` 11387L (calculate sample)

    member _.answer = calculate data
