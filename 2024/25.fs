namespace AdventOfCode.Year2024

open Checked
open System.IO
open AdventOfCode

module Input25 =
    type Schematic =
        | Lock of int[]
        | Key of int[]

    let convert (lines: string[]) =
        let columns (schema: string[]) =
            [| for c in 0..4 -> Array.countIf ((=) '#') [| for r in 0..4 -> schema[r][c] |] |]

        lines
        |> Array.splitInto (lines.Length / 8 + 1)
        |> Array.map (fun schema ->
            columns schema[1..5]
            |> match schema[0], schema[6] with
               | "#####", _ -> Lock
               | _, "#####" -> Key)


    let sample =
        """
#####
.####
.####
.####
.#.#.
.#...
.....

#####
##.##
.#.##
...##
...#.
...#.
.....

.....
#....
#....
#...#
#.#.#
#.###
#####

.....
.....
#.#..
###..
###.#
###.#
#####

.....
.....
.....
#....
#.#..
#.#.#
#####
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/25.txt") |> Seq.toArray |> convert

open Input25

type Problem25() =
    let calculate input =
        input
        |> Array.partition (function
            | Lock _ -> true
            | Key _ -> false)
        ||> Array.allPairs
        |> Array.countIf (fun (Lock lock, Key key) -> Array.zip lock key |> Array.forall (fun (l, k) -> l + k <= 5))

    do ``assert`` 3 (calculate sample)

    member _.answer = calculate data
