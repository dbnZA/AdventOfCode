namespace AdventOfCode.Year2024

open Checked
open System.IO
open AdventOfCode

module Input04 =
    let convert (lines: string[]) =
        lines |> Array.map Array.ofSeq |> Array2D.ofArrayRows

    let sample =
        """
MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/04.txt") |> Seq.toArray |> convert

open Input04

type Problem04a() =
    let calculate (input: _[,]) =
        let rec find (x, y) (x', y' as direction) =
            function
            | [] -> true
            | _ when x < 0 || y < 0 || x = Array2D.length1 input || y = Array2D.length2 input -> false
            | c :: word when input[x, y] = c -> find (x + x', y + y') direction word
            | _ -> false

        let nextMatch coord word =
            [ 1, 1; 1, 0; 1, -1; 0, -1; -1, -1; -1, 0; -1, 1; 0, 1 ]
            |> List.sumBy (fun direction -> if find coord direction word then 1 else 0)

        let mutable count = 0

        for x = 0 to Array2D.length1 input - 1 do
            for y = 0 to Array2D.length2 input - 1 do
                count <- count + nextMatch (x, y) [ 'X'; 'M'; 'A'; 'S' ]

        count


    do ``assert`` 18 (calculate sample)

    member _.answer = calculate data

type Problem04b() =
    let calculate (input: _[,]) =
        let isMS (x, y) (x', y') =
            input[x + x', y + y'] = 'M' && input[x - x', y - y'] = 'S'

        let isMatch coord =
            [ 1, 1; 1, -1; -1, -1; -1, 1; 1, 1 ]
            |> List.pairwise
            |> List.sumBy (fun (direction1, direction2) ->
                if isMS coord direction1 && isMS coord direction2 then
                    1
                else
                    0)

        let mutable count = 0

        for x = 1 to Array2D.length1 input - 2 do
            for y = 1 to Array2D.length2 input - 2 do
                if input[x, y] = 'A' then
                    count <- count + isMatch (x, y)

        count

    do ``assert`` 9 (calculate sample)

    member _.answer = calculate data
