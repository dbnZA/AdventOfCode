namespace AdventOfCode.Year2024

open Checked
open System
open System.IO
open AdventOfCode

module Input05 =
    let convert (lines: string[]) =
        let (|Int|_|) (value: string) =
            match Int32.TryParse(value) with
            | true, value -> Some value
            | false, _ -> None

        let divide = lines |> Array.findIndex ((=) "")

        let order =
            lines[0 .. divide - 1]
            |> Array.map (fun line ->
                match line.Split('|') with
                | [| Int before; Int after |] -> after, before)
            |> Array.groupBy fst
            |> Array.map (fun (after, pairs) -> after, pairs |> Array.map snd)
            |> Map.ofArray

        let pages =
            lines[divide + 1 ..]
            |> Array.map (fun line -> line.Split(',') |> Array.map Int32.Parse |> List.ofArray)

        order, pages


    let sample =
        """
47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/05.txt") |> Seq.toArray |> convert

    let rec isOrdered order seenPages =
        function
        | [] -> true
        | page :: nextPages ->
            match order |> Map.tryFind page with
            | Some mustSeeBefore when
                mustSeeBefore
                |> Array.exists (fun mustSeeBefore -> nextPages |> List.contains mustSeeBefore)
                ->
                false
            | _ -> isOrdered order (page :: seenPages) nextPages

open Input05

type Problem05a() =
    let calculate (order, pages) =
        pages
        |> Array.filter (isOrdered order [])
        |> Array.sumBy (fun pages -> pages[pages.Length / 2])

    do ``assert`` 143 (calculate sample)

    member _.answer = calculate data

type Problem05b() =
    let calculate (order, pages) =
        let isAfter left right =
            match order |> Map.tryFind right with
            | Some before -> before |> Array.contains left
            | None -> false

        let reorder pages =
            pages
            |> List.sortWith (fun left right ->
                if left = right then 0
                elif isAfter left right then -1
                elif isAfter right left then 1
                else 0)

        pages
        |> Array.filter (isOrdered order [] >> not)
        |> Array.map reorder
        |> Array.sumBy (fun pages -> pages[pages.Length / 2])

    do ``assert`` 123 (calculate sample)

    member _.answer = calculate data
