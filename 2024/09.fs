namespace AdventOfCode.Year2024

open Checked
open System.IO
open AdventOfCode

module Input09 =

    let convert (lines: string[]) =
        lines |> Array.exactlyOne |> Array.ofSeq |> Array.map (fun c -> int c - int '0')

    let sample =
        """
2333133121414131402
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/09.txt") |> Seq.toArray |> convert

open Input09

module Problem09a =
    type Type =
        | File
        | Space

    type Position = Position of Type: Type * Index: int * Length: int * Position: int

    module Position =
        let index (Position(_, index, _, position)) = index, position

        let private switch =
            function
            | File -> Space
            | Space -> File

        let rec next diskMap (Position(``type``, index, length, position)) =
            let position = position + 1

            if position >= length then
                let index = index + 1
                let length = Array.get diskMap index
                let position = Position(switch ``type``, index, length, 0)
                if length = 0 then next diskMap position else position
            else
                Position(``type``, index, length, position)

        let rec previousFile diskMap (Position(``type``, index, length, position)) =
            let position = position - 1

            if position < 0 then
                let index = index - 2
                let length = Array.get diskMap index
                let position = Position(``type``, index, length, length - 1)

                if length = 0 then
                    previousFile diskMap position
                else
                    position
            else
                Position(``type``, index, length, position)

        let id (Position(_, index, _, _)) = index / 2

open Problem09a

type Problem09a() =
    let calculate input =
        let rec checksum index head tail value =
            if Position.index head > Position.index tail then
                value
            else
                match head with
                | Position(File, _, _, _) ->
                    checksum (index + 1L) (Position.next input head) tail (value + index * int64 (Position.id head))
                | Position(Space, _, _, _) ->
                    checksum
                        (index + 1L)
                        (Position.next input head)
                        (Position.previousFile input tail)
                        (value + index * int64 (Position.id tail))

        checksum 0L (Position(File, 0, input[0], 0)) (Position(File, input.Length - 1, input[^0], input[^0] - 1)) 0L

    do ``assert`` 1928L (calculate sample)

    member _.answer = calculate data


module Problem09b =
    [<Struct>]
    type Entry = { Index: int; Id: int; Length: int }

open Problem09b

type Problem09b() =
    let calculate (input: int[]) =
        let files, spaces =
            input
            |> Array.mapi (fun i length -> if i % 2 = 0 then i / 2, length else -1, length)
            |> Array.mapFold
                (fun index (id, length) ->
                    { Index = index
                      Id = id
                      Length = length },
                    index + length)
                0
            |> fst
            |> Array.partition (fun entry -> entry.Id <> -1)

        let firstEmpty = Array.zeroCreate 10

        let rec findSpace index length =
            if index >= spaces.Length then
                None
            elif spaces[index].Length >= length then
                firstEmpty[length] <- max firstEmpty[length] index
                Some index
            else
                findSpace (index + 1) length

        let rec fill index =
            if index < 0 then
                files
                |> Array.sumBy (fun entry ->
                    int64 (entry.Length * (entry.Length - 1) / 2 + entry.Length * entry.Index)
                    * int64 entry.Id)
            else
                let length = files[index].Length

                match findSpace firstEmpty[length] length with
                | Some index' when index' < index ->
                    let space = spaces[index']

                    files[index] <-
                        { files[index] with
                            Index = space.Index }

                    spaces[index'] <-
                        { space with
                            Index = space.Index + length
                            Length = space.Length - length }
                | _ -> ()

                fill (index - 1)

        fill (files.Length - 1)

    do ``assert`` 2858L (calculate sample)

    member _.answer = calculate data
