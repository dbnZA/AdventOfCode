namespace AdventOfCode.Year2024

open Checked
open System
open System.IO
open AdventOfCode
open AdventOfCode.FracOps

module Input13 =
    [<Struct>]
    type Game = Game of ButtonA: int64 coord * ButtonB: int64 coord * Prize: int64 coord

    let convert (lines: string[]) =
        let parseXY offset (line: string) =
            let line = line.Split(' ')

            { X = Int64.Parse(line[offset][2..^1])
              Y = Int64.Parse(line[offset + 1][2..]) }

        let button = parseXY 2
        let prize = parseXY 1

        [| for i in 0..4 .. Array.length lines - 1 do
               Game(button lines[i], button lines[i + 1], prize lines[i + 2]) |]

    let sample =
        """
Button A: X+94, Y+34
Button B: X+22, Y+67
Prize: X=8400, Y=5400

Button A: X+26, Y+66
Button B: X+67, Y+21
Prize: X=12748, Y=12176

Button A: X+17, Y+86
Button B: X+84, Y+37
Prize: X=7870, Y=6450

Button A: X+69, Y+23
Button B: X+27, Y+71
Prize: X=18641, Y=10279
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/13.txt") |> Seq.toArray |> convert

open Input13

type Problem13a() =
    let calculate input =
        let rec play (Game(buttonA, buttonB, prize) as game) buttonAPresses cost =
            let position = buttonA * buttonAPresses

            if position .<=. prize then
                let buttonBPresses, miss = Math.DivRem(prize.X - position.X, buttonB.X)

                if miss = 0 && position + buttonB * buttonBPresses = prize then
                    play game (buttonAPresses + 1L) (min (buttonAPresses * 3L + buttonBPresses) cost)
                else
                    play game (buttonAPresses + 1L) cost
            else
                cost

        input
        |> Array.sumBy (fun game ->
            match play game 0 Int64.MaxValue with
            | Int64.MaxValue -> 0L
            | totalPresses -> totalPresses)

    do ``assert`` 480L (calculate sample)

    member _.answer = calculate data

type Problem13b() =
    let calculate resize input =
        let play (Game(a, b, p)) =
            // p.X = a.X * a' + b.X * b'
            // p.Y = a.Y * a' + b.Y * b'
            let z = b.X ./. b.Y
            // p.X - p.Y * z = (a.X - a.Y * z) * a'
            let a' = (p.X - p.Y * z) / (a.X - a.Y * z)
            let b' = (p.X - a.X * a') / b.X

            match a', b' with
            | Frac.IsInteger a', Frac.IsInteger b' -> Some(a' * 3L + b')
            | _ -> None

        input
        |> Array.map (fun (Game(buttonA, buttonB, prize)) -> Game(buttonA, buttonB, prize + resize))
        |> Array.sumBy (fun game ->
            match play game with
            | Some cost -> cost
            | None -> 0L)

    do ``assert`` 480L (calculate 0L sample)

    member _.answer = calculate 10_000_000_000_000L data
