namespace AdventOfCode.Year2024

open System
open System.IO
open AdventOfCode

module Input14 =
    let (|Int|) value = Int32.Parse(value)

    let (|Coord|) (value: string) =
        match (value.Split('=')[1]).Split(',') with
        | [| Int x; Int y |] -> { X = x; Y = y }

    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            match line.Split(' ') with
            | [| Coord p; Coord v |] -> struct (p, v))

    let sample =
        """
p=0,4 v=3,-3
p=6,3 v=-1,-3
p=10,3 v=-1,2
p=2,0 v=2,-1
p=0,0 v=1,3
p=3,0 v=-2,-2
p=7,6 v=-1,-3
p=3,0 v=-1,-2
p=9,3 v=2,3
p=7,3 v=-1,2
p=2,4 v=2,-3
p=9,5 v=-3,-3
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/14.txt") |> Seq.toArray |> convert

open Input14

type Problem14a() =
    let calculate box seconds input =
        let move (struct (p, v)) = p + v * seconds |> Coord.modulo box

        let quadrant p =
            if p.X = box.X / 2 || p.Y = box.Y / 2 then
                None
            else
                Some(p.X > (box.X / 2), p.Y > (box.Y / 2))

        input
        |> Array.map move
        |> Array.choose quadrant
        |> Array.groupBy id
        |> Array.map (snd >> _.Length)
        |> Array.reduce (*)

    do ``assert`` 12 (calculate { X = 11; Y = 7 } 100 sample)

    member _.answer = calculate { X = 101; Y = 103 } 100 data

type Problem14b() =
    let calculate box input =
        let move seconds (struct (p, v: _ coord)) =
            struct (p + v * seconds |> Coord.modulo box, v)

        let rec hasNeighbours p grid ordinalVectors =
            match ordinalVectors with
            | v :: ordinalVectors ->
                if Coord.Array2D.tryGet grid (p + v) |> ValueOption.defaultValue false then
                    hasNeighbours p grid ordinalVectors
                else
                    false
            | [] -> true

        let rec loop n seconds input =
            let n = n + seconds
            let input = input |> Array.map (move seconds)
            let grid = Coord.Array2D.create box false
            input |> Array.iter (fun (struct (p, _)) -> Coord.Array2D.set grid p true)

            let neighbours =
                input
                |> Array.countIf (fun (struct (p, _)) -> hasNeighbours p grid Coord.ordinalVectors)

            if neighbours > 100 then n else loop n seconds input

        loop 0 1 input

    member _.answer = calculate { X = 101; Y = 103 } data
