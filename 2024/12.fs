namespace AdventOfCode.Year2024

open System.IO
open AdventOfCode

module Input12 =
    let convert (lines: string[]) =
        lines |> Array.map Array.ofSeq |> Array2D.ofArrayRows

    let sample =
        """
RRRRIICCFF
RRRRIICCCF
VVRRRCCFFF
VVRCCCJFFF
VVVVCJJCFE
VVIVCCJJEE
VVIIICJJEE
MIIIIIJJEE
MIIISIJEEE
MMMISSJEEE
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/12.txt") |> Seq.toArray |> convert

open Input12

type Problem12a() =
    let calculate input =
        let seen = input |> Array2D.map (fun _ -> false)

        let rec mapRegionAndCost area fence =
            function
            | [] -> area * fence
            | (x, y) :: coords ->
                if not seen[x, y] then
                    seen[x, y] <- true

                    let nextRegion =
                        Array2D.permuteMoves (x, y) input
                        |> List.filter (fun (x', y') -> input[x, y] = input[x', y'])

                    mapRegionAndCost (area + 1) (fence + 4 - nextRegion.Length) (nextRegion @ coords)
                else
                    mapRegionAndCost area fence coords

        (0, input)
        ||> Array2D.foldi (fun x y cost _ ->
            if not seen[x, y] then
                let cost' = mapRegionAndCost 0 0 [ x, y ]
                cost + cost'
            else
                cost)

    do ``assert`` 1930 (calculate sample)

    member _.answer = calculate data

type Problem12b() =
    let calculate input =
        let isCode code coord =
            input |> Array2D.tryGet coord = Some code

        let nextRegions (x, y) =
            [ for dx, dy in [| struct (-1, 0); 1, 0; 0, -1; 0, 1 |] do
                  let coord = x + dx, y + dy

                  if isCode input[x, y] coord then
                      coord ]

        let isEdge coord = nextRegions coord |> List.length < 4

        let seen = input |> Array2D.map (fun _ -> false)

        let rec mapRegionAndCost area sides =
            function
            | [], [] -> area * sides
            | coord :: edges, volume when not (isEdge coord) -> mapRegionAndCost area sides (edges, (coord :: volume))
            | x, y as coord :: edges, volume
            | edges, (x, y as coord :: volume) ->
                if not seen[x, y] then
                    seen[x, y] <- true
                    let code = input[x, y]

                    let isFence (x', y' as coord') (dx, dy) =
                        isCode code coord' && not (isCode code (x' + dx, y' + dy))

                    let isExistingFence coord' d =
                        seen |> Array2D.tryGet coord' = Some true && isFence coord' d

                    let isNewSide (dx, dy as d) =
                        isFence coord d
                        && not (isExistingFence (x + dy, y + dx) d)
                        && not (isExistingFence (x - dy, y - dx) d)

                    let newSides = [ -1, 0; 1, 0; 0, -1; 0, 1 ] |> List.countIf isNewSide

                    mapRegionAndCost (area + 1) (sides + newSides) ((nextRegions coord @ edges), volume)
                else
                    mapRegionAndCost area sides (edges, volume)

        (0, input)
        ||> Array2D.foldi (fun x y cost _ ->
            if not seen[x, y] then
                let cost' = mapRegionAndCost 0 0 ([ x, y ], [])
                cost + cost'
            else
                cost)

    do ``assert`` 1206 (calculate sample)

    member _.answer = calculate data
