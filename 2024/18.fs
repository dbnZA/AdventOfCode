namespace AdventOfCode.Year2024

open System
open System.IO
open AdventOfCode

module Input18 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let [| x; y |] = line.Split(',')

            { X = Int32.Parse(x)
              Y = Int32.Parse(y) })

    let sample =
        """
5,4
4,2
4,5
3,0
2,1
6,3
2,4
1,5
0,6
3,3
2,6
5,1
1,2
5,5
2,5
6,5
1,4
0,4
6,4
1,1
6,1
1,0
0,5
1,6
2,0
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2024/18.txt") |> Seq.toArray |> convert

    let rec explore grid minStep n =
        function
        | [], [] -> ()
        | nextSteps, [] -> explore grid minStep (n + 1) ([], nextSteps)
        | nextSteps, step :: steps ->
            match Coord.Array2D.tryGet grid step, Coord.Array2D.tryGet minStep step with
            | ValueSome cell, ValueSome n' when cell <> '#' && n' > n ->
                Coord.Array2D.set minStep step n
                explore grid minStep n ((Coord.cardinalPoints step) @ nextSteps, steps)
            | _ -> explore grid minStep n (nextSteps, steps)

open Input18

type Problem18a() =
    let calculate n input =
        let grid = Array2D.create n n '.'
        let minStep = Array2D.create n n Int32.MaxValue

        input |> Array.iter (fun coord -> Coord.Array2D.set grid coord '#')

        explore grid minStep 0 ([], [Coord.origin])
        Coord.Array2D.get minStep { X = n - 1; Y = n - 1 }

    do ``assert`` 22 (calculate 7 sample[..11])

    member _.answer = calculate 71 data[..1023]

type Problem18b() =
    let calculate n input =
        let grid = Array2D.create n n '.'

        let rec drop n' =
            let coord = Array.get input n'
            let minStep = Array2D.create n n Int32.MaxValue

            Coord.Array2D.set grid coord '#'
            explore grid minStep 0 ([], [Coord.origin])

            if Coord.Array2D.get minStep { X = n - 1; Y = n - 1 } = Int32.MaxValue then $"{coord.X},{coord.Y}"
            else drop (n' + 1)

        drop 0

    do ``assert`` "6,1" (calculate 7 sample)

    member _.answer = calculate 71 data
