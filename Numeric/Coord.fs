namespace AdventOfCode

open System.Numerics

[<Struct>]
type Coord<'T when 'T :> INumber<'T> and 'T: comparison> =
    { X: 'T
      Y: 'T }

    static member (~-) value : 'T coord = { X = -value.X; Y = -value.Y }

    static member (+)(left, right) : 'T coord =
        { X = left.X + right.X
          Y = left.Y + right.Y }

    static member (+)(left, scalar) : 'T coord =
        { X = left.X + scalar
          Y = left.Y + scalar }

    static member (-)(left, right) : 'T coord =
        { X = left.X - right.X
          Y = left.Y - right.Y }

    static member (*)(left, scalar) : 'T coord =
        { X = left.X * scalar
          Y = left.Y * scalar }

    static member (.<)(left: 'T coord, right) = left.X < right.X

    static member (.<.)(left: 'T coord, right) = left.X < right.X && left.Y < right.Y

    static member (<.)(left: 'T coord, right) = left.Y < right.Y

    static member (.<=)(left: 'T coord, right) = left.X <= right.X

    static member (.<=.)(left: 'T coord, right) = left.X <= right.X && left.Y <= right.Y

    static member (<=.)(left: 'T coord, right) = left.Y <= right.Y

and coord<'T when 'T :> INumber<'T> and 'T: comparison> = Coord<'T>

[<RequireQualifiedAccess>]
module Coord =
    let cardinalVectors =
        [ { X = -1; Y = 0 }; { X = 1; Y = 0 }; { X = 0; Y = -1 }; { X = 0; Y = 1 } ]

    let cardinalPoints coord = cardinalVectors |> List.map ((+) coord)

    let modulo box coord =
        let modulo n x =
            let x = x % n
            if x < 0 then x + n else x

        { X = modulo box.X coord.X
          Y = modulo box.Y coord.Y }

    let moveLeft (coord: 'T coord) = { coord with X = coord.X - 'T.One }

    let moveRight (coord: 'T coord) = { coord with X = coord.X + 'T.One }

    let parseCardinalVector char =
        match char with
        | '^' -> { X = 0; Y = -1 }
        | '>' -> { X = 1; Y = 0 }
        | 'v' -> { X = 0; Y = 1 }
        | '<' -> { X = -1; Y = 0 }

    let ordinalVectors =
        [ { X = 1; Y = 1 }
          { X = 1; Y = 0 }
          { X = 1; Y = -1 }
          { X = 0; Y = 1 }
          { X = 0; Y = 0 }
          { X = 0; Y = -1 }
          { X = -1; Y = 1 }
          { X = -1; Y = 0 }
          { X = -1; Y = -1 } ]

    let ofTuple (x, y) = { X = x; Y = y }

    let ordinalPoints coord = ordinalVectors |> List.map ((+) coord)

    let origin<'T when 'T :> INumber<'T> and 'T: comparison> =
        { X = 'T.Zero; Y = 'T.Zero }

    module Array2D =
        let inline zeroCreate coord = Array2D.zeroCreate coord.Y coord.X

        let inline create coord value = Array2D.create coord.Y coord.X value

        let inline get array coord = Array2D.get array coord.Y coord.X

        let inline mapi projection array =
            Array2D.mapi (fun y x v -> projection { X = x; Y = y } v) array

        let inline findByRow value array =
            let y, x = array |> Array2D.findByRow value
            { X = x; Y = y }

        let inline set array coord value = Array2D.set array coord.Y coord.X value

        let inline tryGet (array: _[,]) coord =
            let xL = array.GetLowerBound(1)
            let yL = array.GetLowerBound(0)
            let xU = xL + array.GetLength(1)
            let yU = yL + array.GetLength(0)

            if coord.X < xL || coord.X >= xU || coord.Y < yL || coord.Y >= yU then
                ValueNone
            else
                ValueSome array[coord.Y, coord.X]
