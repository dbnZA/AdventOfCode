namespace rec AdventOfCode

open System
open System.Numerics

module private NumericLiteralZ =
    let FromZero () = bigint 0
    let FromOne () = bigint 1
    let FromInt32 (x: int) = bigint x

[<Struct>]
type Frac<'T when 'T :> IBinaryInteger<'T> and 'T: comparison> =
    { Numerator: 'T
      Denominator: 'T }

    static member (+)(left: 'T frac, right: 'T frac) : 'T frac =
        let leftNumerator = left.Numerator * right.Denominator
        let rightNumerator = right.Numerator * left.Denominator

        { Numerator = leftNumerator + rightNumerator
          Denominator = left.Denominator * right.Denominator }

    static member (+)(left, right: 'T frac) = Frac.ofNumber left + right
    static member (+)(left: 'T frac, right) = left + Frac.ofNumber right

    static member (~-) value =
        { value with
            Numerator = -value.Numerator }

    static member (-)(left, right) : 'T frac =
        let leftNumerator = left.Numerator * right.Denominator
        let rightNumerator = right.Numerator * left.Denominator

        { Numerator = leftNumerator - rightNumerator
          Denominator = left.Denominator * right.Denominator }

    static member (-)(left, right: 'T frac) = Frac.ofNumber left - right
    static member (-)(left: 'T frac, right) = left - Frac.ofNumber right

    static member (*)(left, right) : 'T frac =
        { Numerator = left.Numerator * right.Numerator
          Denominator = left.Denominator * right.Denominator }

    static member (*)(left, right: 'T frac) = Frac.ofNumber left * right
    static member (*)(left: 'T frac, right) = left * Frac.ofNumber right

    static member (/)(left, right) : 'T frac =
        { Numerator = left.Numerator * right.Denominator
          Denominator = left.Denominator * right.Numerator }

    static member (/)(left, right: 'T frac) = Frac.ofNumber left / right
    static member (/)(left: 'T frac, right) = left / Frac.ofNumber right

and frac<'T when 'T :> IBinaryInteger<'T> and 'T: comparison> = Frac<'T>

module FracOps =
    let inline (./.) numerator denominator =
        { Numerator = numerator
          Denominator = denominator }

[<RequireQualifiedAccess>]
module Frac =
    let simplify (frac: 'T frac) =
        if frac.Denominator = 'T.Zero then
            frac
        else
            let frac =
                if frac.Denominator < 'T.Zero then
                    { Numerator = -frac.Numerator
                      Denominator = -frac.Denominator }
                else
                    frac

            let gcd = gcd (abs frac.Numerator) frac.Denominator

            if gcd = 'T.One then
                frac
            else
                { Numerator = frac.Numerator / gcd
                  Denominator = frac.Denominator / gcd }

    let (|IsInteger|_|) (frac: 'T frac) =
        if frac.Denominator = 'T.Zero then
            None
        else
            let struct (int, rem) = 'T.DivRem(frac.Numerator, frac.Denominator)

            if rem = 'T.Zero then Some int else None

    let ofNumber (value: 'T :> IBinaryInteger<'T>) =
        { Numerator = 'T.CreateChecked(value)
          Denominator = 'T.One }

    let ofFloat (value: float) : bigint frac =
        if Double.IsNaN(value) then
            { Numerator = 0Z; Denominator = 0Z }
        elif Double.IsPositiveInfinity(value) then
            { Numerator = 1Z; Denominator = 0Z }
        elif Double.IsNegativeInfinity(value) then
            { Numerator = -1Z; Denominator = 0Z }
        else
            { Numerator = bigint value
              Denominator = 1Z }

    let toFloat (value: 'T frac) =
        if value.Denominator = 'T.Zero then
            if value.Numerator = 'T.Zero then nan
            elif value.Numerator > 'T.Zero then infinity
            else -infinity
        else
            let value = simplify value
            Double.CreateChecked(value.Numerator) / Double.CreateChecked(value.Denominator)
