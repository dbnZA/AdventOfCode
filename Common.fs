namespace AdventOfCode

open System
open System.IO
open System.Collections.Generic
open System.Numerics

[<AutoOpen>]
module Common =
    let readLineAsInts path =
        use input = File.OpenText(path)
        input.ReadLine().Split(',') |> Array.map Int32.Parse |> Array.toList

    let readRows path =
        [ use input = File.OpenText(path)

          while not input.EndOfStream do
              yield input.ReadLine().Trim() ]

    let ``assert`` expected actual =
        if expected <> actual then
            failwith $"{expected} <> {actual}"

    let inline repeat n ([<InlineIfLambda>] f) x =
        let rec repeat cache n x =
            if n <= 0 then
                x
            else
                match cache |> Map.tryFind x with
                | Some n' ->
                    let target = n' - (n % (n' - n))
                    cache |> Map.findKey (fun _ n' -> n' = target)
                | None -> repeat (cache |> Map.add x n) (n - 1) (f x)

        repeat Map.empty n x

    let rec gcd<'N when 'N :> INumber<'N> and 'N: comparison> (a: 'N) (b: 'N) =
        if b > a then gcd b a
        elif b = 'N.Zero then a
        else gcd b (a % b)

    let memorize f =
        let cache = Dictionary()

        let rec f' args =
            match cache.TryGetValue args with
            | true, value -> value
            | false, _ ->
                let value = f f' args
                cache.Add(args, value)
                value

        f'
