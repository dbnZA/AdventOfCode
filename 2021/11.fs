namespace AdventOfCode.Year2021

open Checked
open System.IO
open AdventOfCode

module Input11 =
    let convert (lines: string[]) =
        let octopi = Array2D.zeroCreate lines.Length lines[0].Length

        for i in 0 .. lines.Length - 1 do
            for j in 0 .. lines[i].Length - 1 do
                octopi[i, j] <- int lines[i].[j] - int '0'

        octopi

    let Sample =
        [| "5483143223"
           "2745854711"
           "5264556173"
           "6141336146"
           "6357385478"
           "4167524645"
           "2176841721"
           "6882881134"
           "4846848554"
           "5283751526" |]
        |> convert

    let Data = File.ReadLines("2021/11.txt") |> Seq.toArray |> convert

module Common11 =
    let permuteMoves map (i, j) =
        let m, n = map |> Array2D.length

        [ 1, 0; 1, 1; 0, 1; -1, 1; -1, 0; -1, -1; 0, -1; 1, -1 ]
        |> List.map (fun (id, jd) -> i + id, j + jd)
        |> List.filter (fun (i, j) -> i >= 0 && i < m && j >= 0 && j < n)

    let step octopi =
        let rec step' (octopi, flashes) =
            let newFlashes =
                octopi
                |> Array2D.foldi
                    (fun i j (flashes: int) v ->
                        if v > 9 then
                            octopi[i, j] <- 0

                            permuteMoves octopi (i, j)
                            |> List.iter (fun (i, j) ->
                                if octopi[i, j] > 0 then
                                    octopi[i, j] <- octopi[i, j] + 1)

                            flashes + 1
                        else
                            flashes)
                    0

            if newFlashes = 0 then
                octopi, flashes
            else
                step' (octopi, flashes + newFlashes)

        (octopi |> Array2D.map (fun v -> v + 1), 0) |> step'

type Problem11a() =
    let calculate steps octopi =
        [ 1..steps ]
        |> List.fold
            (fun (octopi, flashes) _ ->
                let octopi, flash = octopi |> Common11.step
                octopi, flashes + flash)
            (octopi, 0)
        |> snd

    do assert (Input11.Sample |> calculate 100 = 1656)

    member _.answer = Input11.Data |> calculate 100

type Problem11b() =
    let rec calculate step octopi =
        match octopi |> Common11.step with
        | _, 100 -> step + 1
        | octopi, _ -> octopi |> calculate (step + 1)

    do assert (Input11.Sample |> calculate 0 = 195)

    member _.answer = Input11.Data |> calculate 0
