namespace AdventOfCode.Year2021

open Checked
open AdventOfCode

module Input06 =
    let Sample = [ 3; 4; 3; 1; 2 ]

    let Data = readLineAsInts "2021/06.txt"

type Problem06a() =
    let iterDay =
        List.collect (function
            | 0 -> [ 6; 8 ]
            | d -> [ d - 1 ])

    let rec iterDays days school =
        [ 1..days ]
        |> List.fold (fun school _ -> school |> iterDay) school
        |> List.length

    do assert (Input06.Sample |> iterDays 18 = 26)

    do assert (Input06.Sample |> iterDays 80 = 5934)

    member _.answer = Input06.Data |> iterDays 80

type Problem06b() =
    let toBucket school =
        let buckets = school |> List.countBy id |> Map.ofList

        [ for i in 0..8 ->
              buckets
              |> Map.tryFind i
              |> function
                  | Some count -> int64 count
                  | _ -> 0L ]

    let iterDay =
        function
        | gen0 :: school ->
            school
            |> List.mapi (fun i c -> c + if i = 6 then gen0 else 0L)
            |> List.insertAt 8 gen0
        | [] -> failwith "Expected list"

    let rec iterDays days school =
        [ 1..days ]
        |> List.fold (fun school _ -> school |> iterDay) (toBucket school)
        |> List.sum

    do assert (Input06.Sample |> iterDays 18 = 26L)

    do assert (Input06.Sample |> iterDays 80 = 5934L)

    do assert (Input06.Sample |> iterDays 256 = 26984457539L)

    member _.answer = Input06.Data |> iterDays 256
