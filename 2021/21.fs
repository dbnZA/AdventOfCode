namespace AdventOfCode.Year2021

open Checked
open System.Collections.Generic
open System
open AdventOfCode

module Input21 =
    let Sample = (4, 8)

    let Data = (8, 1)

type Problem21a() =
    let (|NextValue|_|) (enumerator: IEnumerator<_>) =
        if enumerator.MoveNext() then
            Some(enumerator.Current, enumerator)
        else
            None

    let rec play rolls ((position, score), player2) =
        function
        | NextValue(roll1, NextValue(roll2, NextValue(roll3, dice))) ->
            let rolls = rolls + 3
            let position = (position + roll1 + roll2 + roll3 - 1) % 10 + 1
            let score = score + position

            if score >= 1000 then
                let _, score = player2
                rolls * score
            else
                play rolls (player2, (position, score)) dice
        | _ -> failwith "Ran out of rolls"

    let calculate (position1, position2) =
        (seq { 1 .. Int32.MaxValue }).GetEnumerator()
        |> play 0 ((position1, 0), (position2, 0))

    do assert (Input21.Sample |> calculate = 739785)

    member _.answer = Input21.Data |> calculate

type Problem21b() =
    let rolls =
        let die = [ 1; 2; 3 ]

        [ for d1 in die do
              for d2 in die do
                  for d3 in die do
                      d1 + d2 + d3 ]
        |> List.countBy id
        |> List.map (fun (roll, count) -> roll, int64 count)

    let zeroCreate maxScore =
        Array4D.zeroCreate 10 (maxScore + 1 + 9) 10 (maxScore + 1 + 9)

    let updateScore maxScore score =
        let newScore = zeroCreate maxScore

        score
        |> Array4D.iteri (fun p1 s1 p2 s2 count ->
            if count = 0L then
                ()
            elif s1 >= maxScore || s2 >= maxScore then
                newScore[p1, s1, p2, s2] <- newScore[p1, s1, p2, s2] + count
            else
                for roll, rollCount in rolls do
                    let p1 = (p1 + roll) % 10
                    let s1 = s1 + (p1 + 1)
                    let count = count * rollCount

                    if s1 >= maxScore then
                        newScore[p1, s1, p2, s2] <- newScore[p1, s1, p2, s2] + count
                    else
                        for roll, rollCount in rolls do
                            let p2 = (p2 + roll) % 10
                            let s2 = s2 + (p2 + 1)
                            let count = count * rollCount
                            newScore[p1, s1, p2, s2] <- newScore[p1, s1, p2, s2] + count)

        newScore

    let calculate maxScore (p1, p2) =
        let score = zeroCreate maxScore
        score[p1 - 1, 0, p2 - 1, 0] <- 1L

        [ 1..maxScore ]
        |> List.fold (fun score _ -> score |> updateScore maxScore) score
        |> Array4D.foldi
            (fun (p1Count, p2Count) _ s1 _ s2 count ->
                if s1 >= maxScore then
                    p1Count + count, p2Count
                elif s2 >= maxScore then
                    p1Count, p2Count + count
                else
                    assert (count = 0L)
                    p1Count, p2Count)
            (0L, 0L)
        |> fun (p1Count, p2Count) -> max p1Count p2Count

    do assert (Input21.Sample |> calculate 21 = 444356092776315L)

    member _.answer = Input21.Data |> calculate 21
