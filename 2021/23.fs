namespace AdventOfCode.Year2021

open Checked
open System.Collections.Generic
open System.IO


module Input23 =
    type Cell =
        | Empty
        | A
        | B
        | C
        | D

    let convert (lines: string[]) =
        let hall = Array.create 11 Cell.Empty

        let sideRoom =
            Array2D.init 4 2 (fun room order ->
                match lines[2 + order][3 + room * 2] with
                | 'A' -> Cell.A
                | 'B' -> Cell.B
                | 'C' -> Cell.C
                | 'D' -> Cell.D
                | char -> failwith $"Unexpected character: {char}")

        hall, sideRoom

    let Sample =
        """
#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let Data = File.ReadLines("2021/23.txt") |> Seq.toArray |> convert

open Input23

module Common23 =
    let inline isComplete (hall, sideRoom: Cell[,]) =
        if hall |> Array.exists ((<>) Cell.Empty) then
            false
        else
            [ 0, Cell.A; 1, Cell.B; 2, Cell.C; 3, Cell.D ]
            |> List.forall (fun (room, amphipod) -> sideRoom[room, *] |> Array.forall ((=) amphipod))

    let moves (hall: Cell[], sideRoom) (extraEnergy, energy) =
        let sideRooms = sideRoom |> Array2D.length2

        let amphipodRoom =
            function
            | Cell.A -> 0
            | Cell.B -> 1
            | Cell.C -> 2
            | Cell.D -> 3

        let amphipodEnergy =
            function
            | Cell.A -> 1
            | Cell.B -> 10
            | Cell.C -> 100
            | Cell.D -> 1000

        let roomIndex room = 2 + 2 * room

        let movesToSideRoom =
            [ for i = 0 to 10 do
                  if hall[i] <> Cell.Empty then
                      let room = amphipodRoom hall[i]
                      let j = roomIndex room
                      let x, y = if i > j then j, i - 1 else i + 1, j

                      if hall[x..y] |> Array.forall ((=) Cell.Empty) then
                          let mutable rank = sideRooms - 1

                          while rank >= 0 && sideRoom[room, rank] = hall[i] do
                              rank <- rank - 1

                          if rank >= 0 && sideRoom[room, rank] = Cell.Empty then
                              let hall = hall |> Array.copy
                              let sideRoom = sideRoom |> Array2D.copy
                              assert (sideRoom[room, rank] = Cell.Empty)
                              sideRoom[room, rank] <- hall[i]
                              hall[i] <- Cell.Empty

                              yield
                                  struct ((hall, sideRoom),
                                          (extraEnergy,
                                           energy + (abs (i - j) + rank + 1) * amphipodEnergy sideRoom[room, rank])) ]

        if not <| List.isEmpty movesToSideRoom then
            movesToSideRoom
        else
            [ let moveToHall i room rank =
                  let hall = hall |> Array.copy
                  let sideRoom = sideRoom |> Array2D.copy
                  assert (hall[i] = Cell.Empty)
                  hall[i] <- sideRoom[room, rank]
                  sideRoom[room, rank] <- Cell.Empty
                  let amphipodEnergy = amphipodEnergy hall[i]
                  let amphipodRoomIndex = amphipodRoom hall[i] |> roomIndex
                  let roomIndex = roomIndex room

                  let extraMoves =
                      if amphipodRoomIndex = roomIndex then
                          (abs (i - roomIndex) - 1) * 2
                      else
                          max 0 (max (i - max amphipodRoomIndex roomIndex) (min amphipodRoomIndex roomIndex - i))
                          * 2

                  struct ((hall, sideRoom),
                          ((extraEnergy + extraMoves * amphipodEnergy),
                           energy + (abs (i - roomIndex) + rank + 1) * amphipodEnergy))

              for room = 0 to 3 do
                  let rank =
                      let mutable rank = 0

                      while rank < sideRooms && sideRoom[room, rank] = Cell.Empty do
                          rank <- rank + 1

                      if
                          rank < sideRooms
                          && sideRoom[room, rank..]
                             |> Array.exists (fun amphipod -> amphipodRoom amphipod <> room)
                      then
                          Some rank
                      else
                          None

                  match rank with
                  | Some rank ->
                      let j = roomIndex room

                      if hall[j] = Cell.Empty then
                          let mutable i = j - 1

                          while i >= 0 && hall[i] = Cell.Empty do
                              yield moveToHall i room rank
                              i <- i - if i <= 1 then 1 else 2

                          let mutable i = j + 1

                          while i < 11 && hall[i] = Cell.Empty do
                              yield moveToHall i room rank
                              i <- i + if i >= 9 then 1 else 2
                  | None -> () ]

    let calculate burrow =
        let rec calculate (plays: PriorityQueue<_, _>) =
            match plays.TryDequeue() with
            | true, burrow, energy ->
                if burrow |> isComplete then
                    energy |> snd
                else
                    plays.EnqueueRange(moves burrow energy)
                    calculate plays
            | false, _, _ -> failwith "no solution found"

        let plays = PriorityQueue()
        plays.Enqueue(burrow, (0, 0))
        calculate plays

open Common23

type Problem23a() =
    do assert (Sample |> calculate = 12521)

    member _.answer = Data |> calculate

type Problem23b() =
    let addAmphipod (hall, sideRoom: Cell[,]) =
        let fold =
            [| [| Cell.D; Cell.C; Cell.B; Cell.A |]; [| Cell.D; Cell.B; Cell.A; Cell.C |] |]

        hall,
        Array2D.init 4 4 (fun room rank ->
            if rank = 0 then sideRoom[room, 0]
            elif rank = 3 then sideRoom[room, 1]
            else fold[rank - 1][room])

    do assert (Sample |> addAmphipod |> calculate = 44169)

    member _.answer = Data |> addAmphipod |> calculate
