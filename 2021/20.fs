namespace AdventOfCode.Year2021

open Checked
open System.IO
open AdventOfCode

module Input20 =
    let convert (lines: string[]) =
        let lines =
            lines |> Array.map (fun line -> line |> Seq.map ((=) '#') |> Seq.toArray)

        lines[0], (lines[2..] |> Array2D.ofArrayRows, false)

    let Sample =
        """
..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let Data = File.ReadLines("2021/20.txt") |> Seq.toArray |> convert

module Common20 =
    let expandImage background image =
        let m, n = image |> Array2D.length
        let newImage = Array2D.create (m + 4) (n + 4) background
        Array2D.blit image 0 0 newImage 2 2 m n
        newImage

    let enhance transform (image, background) =
        let enhance subImage =
            subImage
            |> Array2D.fold (fun value pixel -> value * 2 + if pixel then 1 else 0) 0
            |> Array.get transform

        let m, n = image |> Array2D.length
        let image = image |> expandImage background

        Array2D.init (m + 2) (n + 2) (fun y x -> image[y .. y + 2, x .. x + 2] |> enhance),
        Array2D.create 3 3 background |> enhance

    let rec calculate count (transform, image) =
        if count > 0 then
            let image = image |> enhance transform
            calculate (count - 1) (transform, image)
        else
            image
            |> fst
            |> Array2D.fold (fun count pixel -> if pixel then count + 1 else count) 0

open Common20

type Problem20a() =
    do assert (Input20.Sample |> calculate 2 = 35)

    member _.answer = Input20.Data |> calculate 2

type Problem20b() =
    do assert (Input20.Sample |> calculate 50 = 3351)

    member _.answer = Input20.Data |> calculate 50
