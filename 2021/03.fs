namespace AdventOfCode.Year2021

open Checked
open System
open AdventOfCode

module Input03 =
    let Sample =
        [ "00100"
          "11110"
          "10110"
          "10111"
          "10101"
          "01111"
          "00111"
          "11100"
          "10000"
          "11001"
          "00010"
          "01010" ]

    let Data = readRows "2021/03.txt"

type Problem03a() =
    let countBits =
        function
        | row :: _ as diagnostic ->
            let initCount = List.replicate (row |> String.length) 0

            diagnostic
            |> List.fold
                (fun count row ->
                    row
                    |> List.ofSeq
                    |> List.zip count
                    |> List.map (fun (i, d) -> if d = '1' then i + 1 else i))
                initCount
        | _ -> []

    let parseDiagnostic diagnostic =
        let threshold = (diagnostic |> List.length) / 2
        let frequency = diagnostic |> countBits

        let gamma =
            frequency |> List.fold (fun v i -> v * 2 + if i > threshold then 1 else 0) 0

        let epsilon =
            frequency |> List.fold (fun v i -> v * 2 + if i <= threshold then 1 else 0) 0

        gamma, epsilon

    do assert (Input03.Sample |> parseDiagnostic = (22, 9))

    member _.answer =
        let gamma, epsilon = Input03.Data |> parseDiagnostic
        gamma * epsilon

type Problem03b() =
    let countBit index (diagnostic: string list) =
        diagnostic |> List.sumBy (fun row -> if row.Chars(index) = '1' then 1 else 0)

    let parseDiagnostic filter diagnostic =
        let rec parseDiagnostic' index =
            function
            | [ value ] -> value
            | [] -> failwith "No value found"
            | diagnostic ->
                let filter = filter (diagnostic |> List.length) (diagnostic |> countBit index)

                diagnostic
                |> List.filter (fun row -> row.Chars(index) |> filter)
                |> parseDiagnostic' (index + 1)

        parseDiagnostic' 0 diagnostic |> fun row -> Convert.ToInt32(row, 2)

    let oxygen count frequency =
        (=) (if frequency * 2 >= count then '1' else '0')

    let co2 count frequency =
        (=) (if frequency * 2 >= count then '0' else '1')

    do assert (Input03.Sample |> parseDiagnostic oxygen = 23)

    do assert (Input03.Sample |> parseDiagnostic co2 = 10)

    member _.answer =
        let oxygen = Input03.Data |> parseDiagnostic oxygen
        let co2 = Input03.Data |> parseDiagnostic co2
        oxygen * co2
