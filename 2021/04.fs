namespace AdventOfCode.Year2021

open Checked
open System
open System.IO

type Bingo =
    { Draws: int list
      Boards: int list list }

type Hit =
    | Row of Row: int
    | Column of Column: int

type Board =
    { Board: int list
      Draws: int list
      Hits: Map<Hit, int> }

module Input04 =
    let Sample = {
        Draws = [ 7; 4; 9; 5; 11; 17; 23; 2; 0; 14; 21; 24; 10; 16; 13; 6; 15; 25; 12; 22; 18; 20; 8; 19; 3; 26; 1 ]
        Boards = [ [ 22; 13; 17; 11; 00
                     08; 02; 23; 04; 24
                     21; 09; 14; 16; 07
                     06; 10; 03; 18; 05
                     01; 12; 20; 15; 19 ]
                   [ 03; 15; 00; 02; 22
                     09; 18; 13; 17; 05
                     19; 08; 07; 25; 23
                     20; 11; 10; 24; 04
                     14; 21; 16; 12; 06 ]
                   [ 14; 21; 17; 24; 04
                     10; 16; 15; 09; 19
                     18; 08; 23; 26; 20
                     22; 11; 13; 06; 05
                     02; 00; 12; 03; 07 ] ]
    }

    let Data =
        use input = File.OpenText("2021/04.txt")
        let draws = input.ReadLine().Split(',') |> Array.map Int32.Parse |> Array.toList

        let boards =
            [ while not input.EndOfStream do
                  input.ReadLine() |> ignore

                  yield
                      [ for _ in 1..5 do
                            yield!
                                input.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries)
                                |> Array.map Int32.Parse ] ]

        { Draws = draws; Boards = boards }

module Common04 =
    let rec playBoard draw board =
        let updateHit =
            function
            | Some count -> Some(count + 1)
            | None -> Some 1

        match board.Board |> List.tryFindIndex ((=) draw) with
        | Some index ->
            let column = index % 5
            let row = index / 5

            let board =
                { board with
                    Draws = draw :: board.Draws
                    Hits =
                        board.Hits
                        |> Map.change (Column column) updateHit
                        |> Map.change (Row row) updateHit }

            board,
            board.Hits |> Map.find (Column column) = 5
            || board.Hits |> Map.find (Row row) = 5
        | None -> board, false

    let score board =
        (board.Board
         |> List.filter (fun value -> board.Draws |> List.contains value |> not)
         |> List.sum)
        * List.head board.Draws


type Problem04a() =
    let play bingo =
        let rec play' boards =
            function
            | draw :: draws ->
                let boards, winners = boards |> List.map (Common04.playBoard draw) |> List.unzip

                match winners |> List.tryFindIndex id with
                | Some index -> boards |> List.item index |> Common04.score
                | None -> play' boards draws
            | [] -> failwith "No winners"

        let boards =
            bingo.Boards
            |> List.map (fun board ->
                { Board = board
                  Draws = []
                  Hits = Map.empty })

        bingo.Draws |> play' boards

    do assert (Input04.Sample |> play = 4512)

    member _.answer = Input04.Data |> play

type Problem04b() =
    let play bingo =
        let rec play' boards draws =
            match draws with
            | draw :: draws ->
                match boards with
                | [ board ] ->
                    match board |> Common04.playBoard draw with
                    | board, true -> board |> Common04.score
                    | board, false -> play' [ board ] draws
                | [] -> failwith "Boards draw last"
                | boards ->
                    let boards =
                        boards
                        |> List.map (Common04.playBoard draw)
                        |> List.filter (snd >> not)
                        |> List.map fst

                    play' boards draws
            | [] -> failwith "No last winner"

        let boards =
            bingo.Boards
            |> List.map (fun board ->
                { Board = board
                  Draws = []
                  Hits = Map.empty })

        bingo.Draws |> play' boards

    do assert (Input04.Sample |> play = 1924)

    member _.answer = Input04.Data |> play
