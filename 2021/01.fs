namespace AdventOfCode.Year2021

open Checked
open System
open AdventOfCode

module Input01 =
    let Sample = [ 199; 200; 208; 211; 200; 207; 240; 269; 260; 263 ]

    let Data = readRows "2021/01.txt" |> List.map Int32.Parse

type Problem01a() =
    let largerMeasures measures =
        let rec largerMeasures' count =
            function
            | prevMeasure :: measure :: measures ->
                let count = count + if prevMeasure < measure then 1 else 0
                largerMeasures' count (measure :: measures)
            | [ _ ]
            | [] -> count

        largerMeasures' 0 measures

    do assert (Input01.Sample |> largerMeasures = 7)

    member _.answer = Input01.Data |> largerMeasures

type Problem01b() =
    let largerMeasures measures =
        let rec largerMeasures' count =
            function
            | a1 :: measures ->
                match measures with
                | a2 :: a3 :: a4 :: _ ->
                    let a = a1 + a2 + a3
                    let b = a2 + a3 + a4
                    let count = count + if a < b then 1 else 0
                    largerMeasures' count measures
                | _ -> count
            | [] -> count

        largerMeasures' 0 measures

    do assert (Input01.Sample |> largerMeasures = 5)

    member _.answer = Input01.Data |> largerMeasures
