namespace AdventOfCode.Year2021

open Checked
open System.Collections.Immutable
open System.IO
open AdventOfCode

module Input15 =
    let convert (lines: string[]) =
        let m = lines |> Array.length
        let n = lines[0].Length
        let grid = Array2D.zeroCreate m n
        grid |> Array2D.iteri (fun x y _ -> grid[x, y] <- int (lines[x][y]) - int '0')
        grid

    let Sample =
        [| "1163751742"
           "1381373672"
           "2136511328"
           "3694931569"
           "7463417111"
           "1319128137"
           "1359912421"
           "3125421639"
           "1293138521"
           "2311944581" |]
        |> convert

    let Data = File.ReadLines("2021/15.txt") |> Seq.toArray |> convert

module Common15 =
    let toCumulativeRisk grid =
        let m, n = grid |> Array2D.length
        let weights = Array2D.zeroCreate m n

        for x = m - 1 downto 0 do
            for y = n - 1 downto 0 do
                weights[x, y] <-
                    grid[x, y]
                    + if x = m - 1 && y = n - 1 then 0
                      elif x = m - 1 then weights[x, y + 1]
                      elif y = n - 1 then weights[x + 1, y]
                      else min weights[x, y + 1] weights[x + 1, y]

        weights

    let minRisk grid weights =
        Array2D.get weights 0 0 - Array2D.get grid 0 0

open Common15

type Problem15a() =
    let calculate grid = minRisk grid (grid |> toCumulativeRisk)

    do assert (Input15.Sample |> calculate = 40)

    member _.answer = Input15.Data |> calculate

type Problem15b() =
    let priorityPathSearch grid =
        let m, n = grid |> Array2D.length
        let visited = Array2D.zeroCreate m n

        let (|Empty|Cons|) (sortedSet: ImmutableSortedSet<_>) =
            if sortedSet.IsEmpty then
                Empty
            else
                Cons(sortedSet.Min, sortedSet.Remove(sortedSet.Min))

        let rec search =
            function
            | Cons((weight, 0, 0), _) -> weight - grid[0, 0]
            | Cons((_, x, y), points) when visited[x, y] -> search points
            | Cons((weight, x, y), points) ->
                visited[x, y] <- true

                let points =
                    grid
                    |> Array2D.permuteMoves (x, y)
                    |> List.fold
                        (fun (points: ImmutableSortedSet<_>) (x, y) ->
                            if not visited[x, y] then
                                points.Add((weight + grid[x, y], x, y))
                            else
                                points)
                        points

                search points
            | Empty -> failwith "Unexpected end of search"

        ImmutableSortedSet.Create((grid[m - 1, n - 1], m - 1, n - 1)) |> search

    let expand grid =
        let m, n = grid |> Array2D.length
        let bigGrid = Array2D.zeroCreate (m * 5) (n * 5)

        Array2D.zeroCreate 5 5
        |> Array2D.iteri (fun x y _ ->
            let grid =
                grid
                |> Array2D.map (fun v ->
                    let v = v + x + y
                    v % 10 + v / 10)

            Array2D.blit grid 0 0 bigGrid (x * m) (y * n) m n)

        bigGrid

    let calculate grid = grid |> expand |> priorityPathSearch

    do assert (Input15.Sample |> priorityPathSearch = 40)

    do assert (Input15.Sample |> calculate = 315)

    member _.answer = Input15.Data |> calculate
