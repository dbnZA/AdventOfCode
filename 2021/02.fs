namespace AdventOfCode.Year2021

open Checked
open System
open AdventOfCode

module Input02 =
    let Sample =
        [ "forward", 5; "down", 5; "forward", 8; "up", 3; "down", 8; "forward", 2 ]

    let Data =
        readRows "2021/02.txt"
        |> List.map (fun line ->
            let line = line.Split()
            line[0], Int32.Parse(line[1]))

type Problem02a() =
    let calculatePosition =
        let rec calculatePosition' (position, depth) =
            function
            | ("forward", n) :: instructions -> calculatePosition' (position + n, depth) instructions
            | ("down", n) :: instructions -> calculatePosition' (position, depth + n) instructions
            | ("up", n) :: instructions -> calculatePosition' (position, depth - n) instructions
            | (command, _) :: _ -> failwith $"Unknown command: {command}"
            | [] -> position, depth

        calculatePosition' (0, 0)

    do assert (Input02.Sample |> calculatePosition = (15, 10))

    member _.answer =
        Input02.Data |> calculatePosition |> (fun (position, depth) -> position * depth)

type Problem02b() =
    let calculatePosition =
        let rec calculatePosition' (position, depth, aim) =
            function
            | ("forward", n) :: instructions -> calculatePosition' (position + n, depth + n * aim, aim) instructions
            | ("down", n) :: instructions -> calculatePosition' (position, depth, aim + n) instructions
            | ("up", n) :: instructions -> calculatePosition' (position, depth, aim - n) instructions
            | (command, _) :: _ -> failwith $"Unknown command: {command}"
            | [] -> position, depth

        calculatePosition' (0, 0, 0)

    do assert (Input02.Sample |> calculatePosition = (15, 60))

    member _.answer =
        Input02.Data |> calculatePosition |> (fun (position, depth) -> position * depth)
