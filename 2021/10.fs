namespace AdventOfCode.Year2021

open Checked
open System.IO

module Input10 =
    let convert (lines: string[]) = lines |> Array.toList

    let Sample =
        [| "[({(<(())[]>[[{[]{<()<>>"
           "[(()[<>])]({[<{<<[]>>("
           "{([(<{}[<>[]}>{[]{[(<()>"
           "(((({<>}<{<{<>}{[]{[]{}"
           "[[<[([]))<([[{}[[()]]]"
           "[{[{({}]{}}([{[{{{}}([]"
           "{<[[]]>}<{[{[{[]{()[[[]"
           "[<(<(<(<{}))><([]([]()"
           "<{([([[(<>()){}]>(<<{{"
           "<{([{{}}[<[[[<>{}]]]>[]]" |]
        |> convert

    let Data = File.ReadLines("2021/10.txt") |> Seq.toArray |> convert

type Line =
    | InvalidSyntax of char
    | Incomplete of char list
    | Line of string

module Common10 =
    let parseLine line =
        let rec parseLine' braces symbols =
            match symbols with
            | symbol :: symbols ->
                match symbol with
                | '('
                | '['
                | '{'
                | '<' -> symbols |> parseLine' (symbol :: braces)
                | _ ->
                    match braces with
                    | brace :: braces ->
                        match brace, symbol with
                        | '(', ')'
                        | '[', ']'
                        | '{', '}'
                        | '<', '>' -> symbols |> parseLine' braces
                        | _ -> InvalidSyntax symbol
                    | [] -> InvalidSyntax symbol
            | [] when braces |> List.isEmpty -> Line line
            | [] ->
                braces
                |> List.map (function
                    | '(' -> ')'
                    | '[' -> ']'
                    | '{' -> '}'
                    | '<' -> '>'
                    | brace -> failwith $"Unknown brace: {brace}")
                |> Incomplete

        line |> List.ofSeq |> parseLine' []

type Problem10a() =
    let calculate lines =
        lines
        |> List.map Common10.parseLine
        |> List.choose (function
            | InvalidSyntax ')' -> Some 3
            | InvalidSyntax ']' -> Some 57
            | InvalidSyntax '}' -> Some 1197
            | InvalidSyntax '>' -> Some 25137
            | _ -> None)
        |> List.sum

    do assert (Input10.Sample |> calculate = 26397)

    member _.answer = Input10.Data |> calculate

type Problem10b() =
    let calculate lines =
        let scores =
            lines
            |> List.map Common10.parseLine
            |> List.choose (function
                | Incomplete braces -> Some braces
                | _ -> None)
            |> List.map (
                List.fold
                    (fun score brace ->
                        let points =
                            match brace with
                            | ')' -> 1L
                            | ']' -> 2L
                            | '}' -> 3L
                            | '>' -> 4L
                            | brace -> failwith $"Unknown brace: {brace}"

                        score * 5L + points)
                    0L
            )
            |> List.sort

        scores |> List.item ((scores |> List.length) / 2)

    do assert (Input10.Sample |> calculate = 288957L)

    member _.answer = Input10.Data |> calculate
