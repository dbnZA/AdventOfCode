namespace AdventOfCode.Year2021

open Checked
open System
open AdventOfCode

module Input05 =
    let Sample =
        [ (0, 9), (5, 9)
          (8, 0), (0, 8)
          (9, 4), (3, 4)
          (2, 2), (2, 1)
          (7, 0), (7, 4)
          (6, 4), (2, 0)
          (0, 9), (2, 9)
          (3, 4), (1, 4)
          (0, 0), (8, 8)
          (5, 5), (8, 2) ]

    let Data =
        let point (line: string) =
            let point = line.Split(',')
            Int32.Parse(point[0]), Int32.Parse(point[1])

        readRows "2021/05.txt"
        |> List.map (fun line ->
            let line = line.Split()
            line[0] |> point, line[2] |> point)

module Common05 =
    let score grid =
        grid |> Array2D.sumBy (fun value -> if value >= 2 then 1 else 0)

    let generateGrid lines =
        let step x1 x2 =
            if x1 < x2 then 1
            elif x1 > x2 then -1
            else 0

        let rec generateGrid' (grid: int[,]) =
            function
            | ((x1, y1), (x2, y2)) :: lines ->
                let n = max (abs (x2 - x1)) (abs (y2 - y1))
                let dy = step y1 y2
                let dx = step x1 x2

                for i in 0..n do
                    let y = y1 + i * dy
                    let x = x1 + i * dx
                    grid[y, x] <- grid[y, x] + 1

                generateGrid' grid lines
            | [] -> grid |> score

        let length =
            (lines
             |> List.fold (fun m ((x1, y1), (x2, y2)) -> [ m; x1; y1; x2; y2 ] |> List.max) 0)
            + 1

        let grid = Array2D.zeroCreate length length
        generateGrid' grid lines

type Problem05a() =
    let generateGrid =
        List.filter (fun ((x1, y1), (x2, y2)) -> x1 = x2 || y1 = y2)
        >> Common05.generateGrid

    do assert (Input05.Sample |> generateGrid = 5)

    member _.answer = Input05.Data |> generateGrid

type Problem05b() =
    do assert (Input05.Sample |> Common05.generateGrid = 12)

    member _.answer = Input05.Data |> Common05.generateGrid
