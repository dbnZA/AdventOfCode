namespace AdventOfCode.Year2021

open Checked

module Input17 =
    let Sample = (20, 30), (-10, -5)

    let Data = (209, 238), (-86, -59)

module Common17 =
    type ProbeState =
        | Hit
        | Viable
        | Missed

    let calculateMaxHeight ((x1, x2), (y1, y2)) (dx, dy) =
        let rec path' maxY ((x, y), (dx, dy)) =
            let maxY = max maxY y
            let (x, y), (dx, dy) = (x + dx, y + dy), (max 0 (dx - 1), dy - 1)

            if x1 <= x && x <= x2 && y1 <= y && y <= y2 then Some maxY
            elif x > x2 || y < y1 then None
            else path' maxY ((x, y), (dx, dy))

        path' 0 ((0, 0), (dx, dy))

    let rec foldPaths ((x1, x2), (y1, y2)) folder state =
        [ for dx = 0 to x2 do
              for dy = y1 to -y1 do
                  yield calculateMaxHeight ((x1, x2), (y1, y2)) (dx, dy) ]
        |> List.choose id
        |> List.fold folder state

open Common17

type Problem17a() =
    let calculate target = foldPaths target max 0

    do assert (Input17.Sample |> calculate = 45)

    member _.answer = Input17.Data |> calculate

type Problem17b() =
    let calculate target =
        foldPaths target (fun count _ -> count + 1) 0

    do assert (Input17.Sample |> calculate = 112)

    member _.answer = Input17.Data |> calculate
