namespace AdventOfCode.Year2021

open System.IO

type Node =
    | Start
    | Big of string
    | Small of string
    | End

module Input12 =
    let convert (lines: string[]) =
        let toNode =
            function
            | "start" -> Start
            | "end" -> End
            | node when node = node.ToUpperInvariant() -> Big node
            | node when node = node.ToLowerInvariant() -> Small node
            | node -> failwith $"Unknown node type: {node}"

        lines
        |> Array.map (fun line ->
            let nodes = line.Split('-')
            nodes[0] |> toNode, nodes[1] |> toNode)
        |> Array.toList

    let Sample =
        [| "start-A"; "start-b"; "A-c"; "A-b"; "b-d"; "A-end"; "b-end" |] |> convert

    let Sample2 =
        [| "dc-end"
           "HN-start"
           "start-kj"
           "dc-start"
           "dc-HN"
           "LN-dc"
           "HN-end"
           "kj-sa"
           "kj-HN"
           "kj-dc" |]
        |> convert

    let Sample3 =
        [| "fs-end"
           "he-DX"
           "fs-he"
           "start-DX"
           "pj-DX"
           "end-zg"
           "zg-sl"
           "zg-pj"
           "pj-he"
           "RW-he"
           "fs-DX"
           "pj-RW"
           "zg-RW"
           "start-pj"
           "he-WI"
           "zg-he"
           "pj-fs"
           "start-RW" |]
        |> convert

    let Data = File.ReadLines("2021/12.txt") |> Seq.toArray |> convert

module Common12 =
    let toGraph nodes =
        let addPath a b graph =
            graph
            |> Map.change a (function
                | Some nodes -> Some(b :: nodes)
                | None -> Some [ b ])

        let rec toGraph nodes graph =
            match nodes with
            | (Start, node) :: nodes
            | (node, Start) :: nodes -> graph |> addPath Start node |> toGraph nodes
            | (End, node) :: nodes
            | (node, End) :: nodes -> graph |> addPath node End |> toGraph nodes
            | (Big a, Big b) :: _ -> failwithf $"Cyclical graph detected: {a}<->{b}"
            | (a, b) :: nodes -> graph |> addPath a b |> addPath b a |> toGraph nodes
            | [] -> graph

        toGraph nodes Map.empty

open Common12

type Problem12a() =
    let paths graph =
        let rec paths' paths =
            function
            | (End :: nodes) :: activePaths -> paths' ((End :: nodes) :: paths) activePaths
            | (Small node :: nodes) :: activePaths when nodes |> List.contains (Small node) -> paths' paths activePaths
            | nodes :: activePaths ->
                let expandedPaths =
                    graph |> Map.find (nodes |> List.head) |> List.map (fun node -> node :: nodes)

                expandedPaths @ activePaths |> paths' paths
            | [] -> paths

        paths' [] [ [ Start ] ]

    let calculate nodes =
        nodes |> toGraph |> paths |> List.length

    do assert (Input12.Sample |> calculate = 10)

    do assert (Input12.Sample2 |> calculate = 19)

    member _.answer = Input12.Data |> calculate

type Problem12b() =
    let paths graph =
        let rec paths' paths activePaths =
            let inline search (smallVisited, nodes) activePaths =
                let expandedPaths =
                    graph
                    |> Map.find (nodes |> List.head)
                    |> List.map (fun node -> smallVisited, node :: nodes)

                paths' paths (expandedPaths @ activePaths)

            match activePaths with
            | (_, End :: nodes) :: activePaths -> paths' ((End :: nodes) :: paths) activePaths
            | (smallVisited, Small node :: nodes) :: activePaths when nodes |> List.contains (Small node) ->
                match smallVisited with
                | None -> search (Some node, (Small node) :: nodes) activePaths
                | Some _ -> paths' paths activePaths
            | nodes :: activePaths -> search nodes activePaths
            | [] -> paths

        paths' [] [ None, [ Start ] ]

    let calculate nodes =
        nodes |> toGraph |> paths |> List.length

    do assert (Input12.Sample |> calculate = 36)

    do assert (Input12.Sample2 |> calculate = 103)

    do assert (Input12.Sample3 |> calculate = 3509)

    member _.answer = Input12.Data |> calculate
