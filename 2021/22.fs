namespace AdventOfCode.Year2021

open Checked
open System
open System.IO
open AdventOfCode

module Input22 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let [| state; coordinates |] = line.Split(' ')

            let state =
                match state with
                | "on" -> true
                | "off" -> false

            let [| x; y; z |] = coordinates.Split(",")

            let toRange (axis: string) =
                let [| lower; upper |] = axis.Split("=").[1].Split("..")
                Int32.Parse(lower), Int32.Parse(upper)

            state, (x |> toRange, y |> toRange, z |> toRange))
        |> Array.toList

    let Sample =
        """
on x=-20..26,y=-36..17,z=-47..7
on x=-20..33,y=-21..23,z=-26..28
on x=-22..28,y=-29..23,z=-38..16
on x=-46..7,y=-6..46,z=-50..-1
on x=-49..1,y=-3..46,z=-24..28
on x=2..47,y=-22..22,z=-23..27
on x=-27..23,y=-28..26,z=-21..29
on x=-39..5,y=-6..47,z=-3..44
on x=-30..21,y=-8..43,z=-13..34
on x=-22..26,y=-27..20,z=-29..19
off x=-48..-32,y=26..41,z=-47..-37
on x=-12..35,y=6..50,z=-50..-2
off x=-48..-32,y=-32..-16,z=-15..-5
on x=-18..26,y=-33..15,z=-7..46
off x=-40..-22,y=-38..-28,z=23..41
on x=-16..35,y=-41..10,z=-47..6
off x=-32..-23,y=11..30,z=-14..3
on x=-49..-5,y=-3..45,z=-29..18
off x=18..30,y=-20..-8,z=-3..13
on x=-41..9,y=-7..43,z=-33..15
on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
on x=967..23432,y=45373..81175,z=27513..53682
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let Sample2 =
        """
on x=10..12,y=10..12,z=10..12
on x=11..13,y=11..13,z=11..13
off x=9..11,y=9..11,z=9..11
on x=10..10,y=10..10,z=10..10
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let Data = File.ReadLines("2021/22.txt") |> Seq.toArray |> convert

module Volume =
    let inline intersect ((x11, x12), (y11, y12), (z11, z12)) ((x21, x22), (y21, y22), (z21, z22)) =
        let x1, x2, y1, y2, z1, z2 =
            max x11 x21, min x12 x22, max y11 y21, min y12 y22, max z11 z21, min z12 z22

        if x1 > x2 || y1 > y2 || z1 > z2 then
            None
        else
            Some((x1, x2), (y1, y2), (z1, z2))

    let exclude ((x11, x12), (y11, y12), (z11, z12)) ((x21, x22), (y21, y22), (z21, z22)) =
        let permute i21 i11 i12 i22 =
            [ i21, i11 - 1; i11, i12; i12 + 1, i22 ]

        [ for x1, x2 in permute x21 x11 x12 x22 do
              for y1, y2 in permute y21 y11 y12 y22 do
                  for z1, z2 in permute z21 z11 z12 z22 do
                      if ((x11, x12), (y11, y12), (z11, z12)) <> ((x1, x2), (y1, y2), (z1, z2)) then
                          if x1 <= x2 && y1 <= y2 && z1 <= z2 then
                              yield ((x1, x2), (y1, y2), (z1, z2)) ]

    let area ((x1, x2), (y1, y2), (z1, z2)) =
        int64 (x2 - x1 + 1) * int64 (y2 - y1 + 1) * int64 (z2 - z1 + 1)

    let allArea volumes =
        volumes |> List.fold (fun count volume -> count + area volume) 0L

    let add volume volumes =
        volumes
        |> List.fold
            (fun volumes volume ->
                volumes
                |> List.collect (fun toAdd ->
                    match intersect toAdd volume with
                    | Some intersect -> exclude intersect toAdd
                    | None -> [ toAdd ]))
            [ volume ]
        |> List.append volumes

    let rec remove volume volumes =
        volumes
        |> List.collect (fun area ->
            match intersect volume area with
            | Some intersect -> exclude intersect area
            | None -> [ area ])

    let inArea size (_, ((x1, x2), (y1, y2), (z1, z2))) =
        [ x1; x2; y1; y2; z1; z2 ] |> List.forall (fun a -> abs a <= size)

type Problem22a() =
    let calculate instructions =
        let offset = 50

        instructions
        |> List.fold
            (fun (cube: bool[,,]) (state, ((x1, x2), (y1, y2), (z1, z2))) ->
                let state = Array3D.create (x2 - x1 + 1) (y2 - y1 + 1) (z2 - z1 + 1) state
                cube[x1 + offset .. x2 + offset, y1 + offset .. y2 + offset, z1 + offset .. z2 + offset] <- state
                cube)
            (Array3D.zeroCreate 101 101 101)
        |> Array3D.fold (fun count state -> if state then count + 1 else count) 0

    do assert (Input22.Sample |> List.filter (Volume.inArea 50) |> calculate = 590784)

    member _.answer = Input22.Data |> List.filter (Volume.inArea 50) |> calculate

type Problem22b() =
    let calculate instructions =
        let rec calculate volumes =
            function
            | (state, volume) :: instructions ->
                let volumes = volumes |> (if state then Volume.add else Volume.remove) volume
                calculate volumes instructions
            | [] -> volumes |> Volume.allArea

        calculate [] instructions

    do assert (Input22.Sample2 |> calculate = 39L)

    do assert (Input22.Sample |> List.filter (Volume.inArea 50) |> calculate = 590784L)

    member _.answer = Input22.Data |> calculate
