namespace AdventOfCode.Year2021

open Checked
open System.IO

module Input18 =
    type Node =
        | Pair of Node * Node
        | Value of int

    let convert (lines: string[]) =
        let rec (|Node|_|) =
            function
            | '[' :: Node(left, ',' :: Node(right, ']' :: data)) -> Some(Node(Pair(left, right), data))
            | char :: data when int char >= int '0' && int char <= int '9' ->
                Some(Node(Value(int char - int '0'), data))
            | [] -> None
            | char :: _ -> failwith $"Unexpected token: {char}"

        lines
        |> Array.map (fun line ->
            line
            |> List.ofSeq
            |> function
                | Node(pair, []) -> pair
                | _ -> failwith $"Unrecognised line: {line}")
        |> Array.toList

    let sample =
        """
[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2021/18.txt") |> Seq.toArray |> convert

open Input18

module Common18 =
    let rec split =
        function
        | Value value when value > 9 ->
            let left = value / 2
            Some(Pair(Value left, Value(value - left)))
        | Value _ -> None
        | Pair(left, right) ->
            match split left with
            | Some left -> Some(Pair(left, right))
            | None ->
                match split right with
                | Some right -> Some(Pair(left, right))
                | None -> None

    let rec addLeft value =
        function
        | Pair(left, right) -> Pair(addLeft value left, right)
        | Value left -> Value(left + value)

    let rec addRight value =
        function
        | Pair(left, right) -> Pair(left, addRight value right)
        | Value right -> Value(right + value)

    let rec explode depth =
        function
        | Pair(Value left, Value right) when depth = 4 -> Some(Some left, Value 0, Some right)
        | Pair(left, right) ->
            match explode (depth + 1) left, right with
            | Some(None, left, None), right -> Some(None, Pair(left, right), None)
            | Some(leftAction, left, Some value), right -> Some(leftAction, Pair(left, addLeft value right), None)
            | Some(Some value, left, None), right -> Some(Some value, Pair(left, right), None)
            | None, _ ->
                match left, explode (depth + 1) right with
                | left, Some(None, right, None) -> Some(None, Pair(left, right), None)
                | left, Some(Some value, right, rightAction) ->
                    Some(None, Pair(addRight value left, right), rightAction)
                | left, Some(None, right, Some value) -> Some(None, Pair(left, right), Some value)
                | _, None -> None
        | Value _ -> None

    let rec reduce node =
        match explode 0 node with
        | Some(_, node, _) -> reduce node
        | None ->
            match split node with
            | Some node -> reduce node
            | None -> node

    let rec magniture =
        function
        | Value value -> value
        | Pair(left, right) -> 3 * magniture left + 2 * magniture right

    let calculate =
        List.reduce (fun left right -> Pair(left, right) |> reduce) >> magniture

    let reducesTo expected actual =
        ([| actual |] |> convert)[0] |> reduce = ([| expected |] |> convert)[0]

    do
        assert ("[[[[[9,8],1],2],3],4]" |> reducesTo "[[[[0,9],2],3],4]")
        assert ("[7,[6,[5,[4,[3,2]]]]]" |> reducesTo "[7,[6,[5,[7,0]]]]")
        assert ("[[6,[5,[4,[3,2]]]],1]" |> reducesTo "[[6,[5,[7,0]]],3]")

        assert
            ("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"
             |> reducesTo "[[3,[2,[8,0]]],[9,[5,[7,0]]]]")

        assert
            ("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]"
             |> reducesTo "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")

open Common18

type Problem18a() =
    do assert (sample |> calculate = 4140)

    member _.answer = data |> calculate

type Problem18b() =
    let calculate numbers =
        [ for left in numbers do
              for right in numbers do
                  yield [ left; right ] ]
        |> List.map calculate
        |> List.max

    do assert (sample |> calculate = 3993)

    member _.answer = data |> calculate
