namespace AdventOfCode.Year2021

open Checked
open System.IO

type OperatorType =
    | Sum = 0
    | Product = 1
    | Minimum = 2
    | Maximum = 3
    | GreaterThan = 5
    | LessThan = 6
    | EqualTo = 7

type Packet = { Version: int; Payload: PacketData }

and PacketData =
    | LiteralValue of int64
    | Operator of OperatorType * Packet list

module Input16 =
    let hex =
        [ '0', [ 0; 0; 0; 0 ]
          '1', [ 0; 0; 0; 1 ]
          '2', [ 0; 0; 1; 0 ]
          '3', [ 0; 0; 1; 1 ]
          '4', [ 0; 1; 0; 0 ]
          '5', [ 0; 1; 0; 1 ]
          '6', [ 0; 1; 1; 0 ]
          '7', [ 0; 1; 1; 1 ]
          '8', [ 1; 0; 0; 0 ]
          '9', [ 1; 0; 0; 1 ]
          'A', [ 1; 0; 1; 0 ]
          'B', [ 1; 0; 1; 1 ]
          'C', [ 1; 1; 0; 0 ]
          'D', [ 1; 1; 0; 1 ]
          'E', [ 1; 1; 1; 0 ]
          'F', [ 1; 1; 1; 1 ] ]
        |> Map.ofList

    let convert (lines: string[]) =
        lines[0] |> Seq.collect (fun char -> hex |> Map.find char) |> Seq.toList

    let Data = File.ReadLines("2021/16.txt") |> Seq.toArray |> convert

module Common16 =
    let (|Int|_|) bits data =
        if data |> List.length < bits then
            None
        else
            let bits, data = data |> List.splitAt bits
            Some(bits |> List.fold (fun value bit -> value * 2 + bit) 0, data)

    let (|Literal|_|) data =
        let rec parseLiteral literal =
            function
            | 1 :: Int 4 (value, data) -> parseLiteral (literal * 16L + int64 value) data
            | 0 :: Int 4 (value, data) -> Some(literal * 16L + int64 value, data)
            | _ -> None

        parseLiteral 0L data

    let rec (|Packets|) =
        function
        | Packet(packet, Packets(packets, data)) -> packet :: packets, data
        | data -> [], data

    and (|Packet|_|) =
        function
        | Int 3 (version, Int 3 (4, Literal(value, data))) ->
            Some(
                { Version = version
                  Payload = LiteralValue value },
                data
            )
        | Int 3 (version, Int 3 (typeId, 0 :: Int 15 (length, data))) ->
            match data |> List.splitAt length with
            | Packets(packets, []), data ->
                Some(
                    { Version = version
                      Payload = Operator(enum typeId, packets) },
                    data
                )
            | _ -> failwith "Data after packets"
        | Int 3 (version, Int 3 (typeId, 1 :: Int 11 (packets, data))) ->
            let packets, data =
                [ 1L .. packets ]
                |> List.fold
                    (fun (packets, data) _ ->
                        match data with
                        | Packet(packet, data) -> packet :: packets, data
                        | _ -> failwith "Expected packet, none found")
                    ([], data)

            Some(
                { Version = version
                  Payload = Operator(enum typeId, packets |> List.rev) },
                data
            )
        | _ -> None

    let parse =
        function
        | Packets(packets, _) -> packets

open Common16

type Problem16a() =
    let calculate data =
        let rec countVersion versions packets =
            match packets with
            | packet :: packets ->
                let versions =
                    versions
                    + packet.Version
                    + match packet.Payload with
                      | Operator(_, subPackets) -> subPackets |> countVersion 0
                      | LiteralValue _ -> 0

                packets |> countVersion versions
            | [] -> versions

        data |> parse |> countVersion 0

    do
        assert
            ([| "D2FE28" |] |> Input16.convert |> parse = [ { Version = 6
                                                              Payload = LiteralValue 2021L } ])

    do
        assert
            ([| "38006F45291200" |] |> Input16.convert |> parse = [ { Version = 1
                                                                      Payload =
                                                                        Operator(
                                                                            OperatorType.LessThan,
                                                                            [ { Version = 6
                                                                                Payload = LiteralValue 10L }
                                                                              { Version = 2
                                                                                Payload = LiteralValue 20L } ]
                                                                        ) } ])

    do assert ([| "8A004A801A8002F478" |] |> Input16.convert |> calculate = 16)
    do assert ([| "620080001611562C8802118E34" |] |> Input16.convert |> calculate = 12)
    do assert ([| "C0015000016115A2E0802F182340" |] |> Input16.convert |> calculate = 23)
    do assert ([| "A0016C880162017C3686B18A3D4780" |] |> Input16.convert |> calculate = 31)

    member _.answer = Input16.Data |> calculate

type Problem16b() =
    let calculate data =
        let rec calculate =
            function
            | { Payload = LiteralValue value } -> value
            | { Payload = Operator(OperatorType.Sum, packets) } -> packets |> List.sumBy calculate
            | { Payload = Operator(OperatorType.Product, packets) } ->
                packets |> List.fold (fun value packet -> value * calculate packet) 1L
            | { Payload = Operator(OperatorType.Minimum, packets) } -> packets |> List.map calculate |> List.min
            | { Payload = Operator(OperatorType.Maximum, packets) } -> packets |> List.map calculate |> List.max
            | { Payload = Operator(OperatorType.GreaterThan, [ p1; p2 ]) } ->
                if calculate p1 > calculate p2 then 1L else 0L
            | { Payload = Operator(OperatorType.LessThan, [ p1; p2 ]) } ->
                if calculate p1 < calculate p2 then 1L else 0L
            | { Payload = Operator(OperatorType.EqualTo, [ p1; p2 ]) } -> if calculate p1 = calculate p2 then 1L else 0L
            | packet -> failwith $"Unknown packet: {packet}"

        data |> parse |> List.head |> calculate

    do assert ([| "C200B40A82" |] |> Input16.convert |> calculate = 3L)
    do assert ([| "04005AC33890" |] |> Input16.convert |> calculate = 54L)
    do assert ([| "880086C3E88112" |] |> Input16.convert |> calculate = 7L)
    do assert ([| "CE00C43D881120" |] |> Input16.convert |> calculate = 9L)
    do assert ([| "D8005AC2A8F0" |] |> Input16.convert |> calculate = 1L)
    do assert ([| "F600BC2D8F" |] |> Input16.convert |> calculate = 0L)
    do assert ([| "9C005AC2F8F0" |] |> Input16.convert |> calculate = 0L)
    do assert ([| "9C0141080250320F1802104A08" |] |> Input16.convert |> calculate = 1L)

    member _.answer = Input16.Data |> calculate
