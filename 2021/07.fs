namespace AdventOfCode.Year2021

open Checked
open AdventOfCode

module Input07 =
    let Sample = [ 16; 1; 2; 0; 4; 2; 7; 1; 2; 14 ]

    let Data = readLineAsInts "2021/07.txt"

type Problem07a() =
    let minimiseCost positions =
        let cost position =
            List.sumBy (fun offset -> abs (position - offset))

        [ List.min positions .. List.max positions ]
        |> List.map (fun position -> cost position positions)
        |> List.min

    do assert (Input07.Sample |> minimiseCost = 37)

    member _.answer = Input07.Data |> minimiseCost

type Problem07b() =
    let minimiseCost positions =
        let cost position =
            List.sumBy (fun offset ->
                let n = abs (position - offset)
                n * (n + 1) / 2)

        [ List.min positions .. List.max positions ]
        |> List.map (fun position -> cost position positions)
        |> List.min

    do assert (Input07.Sample |> minimiseCost = 168)

    member _.answer = Input07.Data |> minimiseCost
