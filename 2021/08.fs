namespace AdventOfCode.Year2021

open Checked
open System
open System.IO

module Input08 =
    let Sample = [
        [ "be"; "cfbegad"; "cbdgef"; "fgaecd"; "cgeb"; "fdcge"; "agebfd"; "fecdb"; "fabcd"; "edb" ], [ "fdgacbe"; "cefdb"; "cefbgd"; "gcbe" ]
        [ "edbfga"; "begcd"; "cbg"; "gc"; "gcadebf"; "fbgde"; "acbgfd"; "abcde"; "gfcbed"; "gfec" ], [ "fcgedb"; "cgb"; "dgebacf"; "gc" ]
        [ "fgaebd"; "cg"; "bdaec"; "gdafb"; "agbcfd"; "gdcbef"; "bgcad"; "gfac"; "gcb"; "cdgabef" ], [ "cg"; "cg"; "fdcagb"; "cbg" ]
        [ "fbegcd"; "cbd"; "adcefb"; "dageb"; "afcb"; "bc"; "aefdc"; "ecdab"; "fgdeca"; "fcdbega" ], [ "efabcd"; "cedba"; "gadfec"; "cb" ]
        [ "aecbfdg"; "fbg"; "gf"; "bafeg"; "dbefa"; "fcge"; "gcbea"; "fcaegb"; "dgceab"; "fcbdga" ], [ "gecf"; "egdcabf"; "bgf"; "bfgea" ]
        [ "fgeab"; "ca"; "afcebg"; "bdacfeg"; "cfaedg"; "gcfdb"; "baec"; "bfadeg"; "bafgc"; "acf" ], [ "gebdcfa"; "ecba"; "ca"; "fadegcb" ]
        [ "dbcfg"; "fgd"; "bdegcaf"; "fgec"; "aegbdf"; "ecdfab"; "fbedc"; "dacgb"; "gdcebf"; "gf" ], [ "cefg"; "dcbef"; "fcge"; "gbcadfe" ]
        [ "bdfegc"; "cbegaf"; "gecbf"; "dfcage"; "bdacg"; "ed"; "bedf"; "ced"; "adcbefg"; "gebcd" ], [ "ed"; "bcgafe"; "cdgba"; "cbgef" ]
        [ "egadfb"; "cdbfeg"; "cegd"; "fecab"; "cgb"; "gbdefca"; "cg"; "fgcdab"; "egfdb"; "bfceg" ], [ "gbdfcae"; "bgc"; "cg"; "cgb" ]
        [ "gcafb"; "gcf"; "dcaebfg"; "ecagb"; "gf"; "abcdeg"; "gaef"; "cafbge"; "fdbac"; "fegbdc" ], [ "fgae"; "cfgab"; "fg"; "bagce" ]
    ]

    let Data =
        File.ReadAllLines("2021/08.txt")
        |> Array.map (fun line ->
            let line = line.Split('|', StringSplitOptions.TrimEntries)
            line[0].Split() |> Array.toList, line[1].Split() |> Array.toList)
        |> Array.toList

type Problem08a() =
    let count1478 (digits: (string list * string list) list) =
        digits
        |> List.map snd
        |> List.collect (fun digits ->
            digits
            |> List.filter (fun digit -> [ 2; 3; 4; 7 ] |> List.contains digit.Length))
        |> List.length

    do assert (Input08.Sample |> count1478 = 26)

    member _.answer = Input08.Data |> count1478

type Problem08b() =
    let decodeDigit (digits, output) =
        let digits = digits |> List.map Set.ofSeq

        let findDigit length =
            digits |> List.find (fun digit -> digit |> Set.count = length)

        let filterDigit length =
            digits |> List.filter (fun digit -> digit |> Set.count = length)

        let pickMissing count digit digits =
            digits
            |> List.find (Set.intersect digit >> Set.count >> (=) (Set.count digit - count))

        let digit1 = findDigit 2
        let digit4 = findDigit 4
        let digit7 = findDigit 3
        let digit8 = findDigit 7

        let digit069 = filterDigit 6
        let digit6 = digit069 |> pickMissing 1 digit1
        let digit0 = digit069 |> List.filter ((<>) digit6) |> pickMissing 1 digit4
        let digit9 = digit069 |> List.find (fun digit -> digit <> digit0 && digit <> digit6)

        let digit235 = filterDigit 5
        let digit5 = digit235 |> pickMissing 1 digit6
        let digit3 = digit235 |> pickMissing 0 digit1
        let digit2 = digit235 |> List.find (fun digit -> digit <> digit3 && digit <> digit5)

        output
        |> List.map Set.ofSeq
        |> List.map (fun digit ->
            if digit = digit0 then 0
            elif digit = digit1 then 1
            elif digit = digit2 then 2
            elif digit = digit3 then 3
            elif digit = digit4 then 4
            elif digit = digit5 then 5
            elif digit = digit6 then 6
            elif digit = digit7 then 7
            elif digit = digit8 then 8
            elif digit = digit9 then 9
            else failwith $"Unknown digit: %A{digit}")
        |> List.fold (fun value digit -> value * 10 + digit) 0

    let decodeDigits digits =
        digits |> List.map decodeDigit |> List.sum

    do assert (Input08.Sample |> decodeDigits = 61229)

    member _.answer = Input08.Data |> decodeDigits
