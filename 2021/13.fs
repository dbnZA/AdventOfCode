namespace AdventOfCode.Year2021

open Checked
open System
open System.IO
open AdventOfCode

type Axis =
    | X of int
    | Y of int

module Input13 =
    let convert (lines: string[]) =
        let dots, folds = lines |> Array.splitAt (lines |> Array.findIndex ((=) ""))

        let dots =
            dots
            |> Array.map (fun line ->
                let line = line.Split(',')
                Int32.Parse(line[0]), Int32.Parse(line[1]))
            |> Array.toList

        let folds =
            folds
            |> Array.tail
            |> Array.map (fun line ->
                match line.Split(' ').[2].Split('=') with
                | [| "y"; value |] -> Y(Int32.Parse(value))
                | [| "x"; value |] -> X(Int32.Parse(value))
                | line -> failwith $"Unable to parse axis: {line}")
            |> Array.toList

        dots, folds

    let Sample =
        [| "6,10"
           "0,14"
           "9,10"
           "0,3"
           "10,4"
           "4,11"
           "6,0"
           "6,12"
           "4,1"
           "0,13"
           "10,12"
           "3,4"
           "3,0"
           "8,4"
           "1,10"
           "2,14"
           "8,10"
           "9,0"
           ""
           "fold along y=7"
           "fold along x=5" |]
        |> convert

    let Data = File.ReadLines("2021/13.txt") |> Seq.toArray |> convert

module Common13 =
    let toPaper dots =
        let paper =
            Array2D.zeroCreate ((dots |> List.maxOf fst) + 1) ((dots |> List.maxOf snd) + 1)

        dots |> List.iter (fun (x, y) -> paper[x, y] <- true)
        paper

    let fold paper axis =
        let reflectPoint axis n y lookup =
            let foldStart = axis - ((n - 1) - axis)

            if y >= foldStart then
                lookup ((n - 1) - (y - foldStart))
            else
                false

        let foldedPaper, getFoldedPoint =
            match axis with
            | Y axis ->
                let reflectPoint = reflectPoint axis (paper |> Array2D.length2)
                Array2D.zeroCreate (paper |> Array2D.length1) axis, (fun x y -> reflectPoint y (fun y -> paper[x, y]))
            | X axis ->
                let reflectPoint = reflectPoint axis (paper |> Array2D.length1)
                Array2D.zeroCreate axis (paper |> Array2D.length2), (fun x y -> reflectPoint x (fun x -> paper[x, y]))

        foldedPaper
        |> Array2D.iteri (fun x y _ -> foldedPaper[x, y] <- paper[x, y] || getFoldedPoint x y)

        foldedPaper

    let calculate (dots, folds) =
        folds |> List.fold fold (dots |> toPaper) |> Array2D.sumBy Convert.ToInt32

open Common13

type Problem13a() =
    let calculate (dots, folds) =
        (dots, [ folds |> List.head ]) |> calculate

    do assert (Input13.Sample |> calculate = 17)

    do assert (Input13.Sample |> Common13.calculate = 16)

    member _.answer = Input13.Data |> calculate

type Problem13b() =
    do assert (Input13.Sample |> calculate = 16)

    member _.answer =
        let dots, folds = Input13.Data

        folds
        |> List.fold fold (dots |> toPaper)
        |> fun array ->
            let m, n = array |> Array2D.length

            [| for j = 0 to n - 1 do
                   yield '|'
                   for i in 0 .. m - 1 -> if array[i, j] then '#' else ' ' |]
        |> Array.append [| '\n' |]
        |> String
