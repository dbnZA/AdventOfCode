namespace AdventOfCode.Year2021

open Checked
open System
open System.IO

module Input24 =
    type Register =
        | W
        | X
        | Y
        | Z

    type Field =
        | Register of Register
        | Constant of int64

    type Instruction =
        | Inp of Register
        | Add of Register * Field
        | Mul of Register * Field
        | Div of Register * Field
        | Mod of Register * Field
        | Eql of Register * Field

    let convert (lines: string[]) =
        let (|Register|_|) =
            function
            | 'w' :: chars -> Some(Register.W, chars)
            | 'x' :: chars -> Some(Register.X, chars)
            | 'y' :: chars -> Some(Register.Y, chars)
            | 'z' :: chars -> Some(Register.Z, chars)
            | _ -> None

        let (|Field|) =
            function
            | Register(register, chars) -> Register register, chars
            | chars -> Constant(Int64.Parse(String(chars |> List.toArray))), []

        let (|Instruction|) =
            function
            | 'i' :: 'n' :: 'p' :: ' ' :: Register(reg, []) -> Inp reg
            | 'a' :: 'd' :: 'd' :: ' ' :: Register(reg, ' ' :: Field(fld, [])) -> Add(reg, fld)
            | 'm' :: 'u' :: 'l' :: ' ' :: Register(reg, ' ' :: Field(fld, [])) -> Mul(reg, fld)
            | 'd' :: 'i' :: 'v' :: ' ' :: Register(reg, ' ' :: Field(fld, [])) -> Div(reg, fld)
            | 'm' :: 'o' :: 'd' :: ' ' :: Register(reg, ' ' :: Field(fld, [])) -> Mod(reg, fld)
            | 'e' :: 'q' :: 'l' :: ' ' :: Register(reg, ' ' :: Field(fld, [])) -> Eql(reg, fld)
            | line -> failwith $"Unmatched line: {String(line |> List.toArray)}"

        lines
        |> Array.map (
            List.ofSeq
            >> function
                | Instruction ins -> ins
        )
        |> Array.toList

    let Data = File.ReadLines("2021/24.txt") |> Seq.toArray |> convert

open Input24

module Common24 =
    type Direction =
        | Up
        | Down

    let solve direction parameters =
        let parameters = parameters |> List.toArray
        let input = Array.zeroCreate 16
        let z = Array.zeroCreate 15

        let start, step, stop =
            match direction with
            | Up -> 1L, 1L, (fun input -> input > 9L)
            | Down -> 9L, -1L, (fun input -> input < 1L)

        let rec solve i =
            let inline next i =
                input[i] <- input[i] + step
                solve i

            let inline nextInput () =
                input[i + 1] <- start
                solve (i + 1)

            if i = 15 then
                input[1..14] |> Array.toList
            elif stop input[i] then
                next (i - 1)
            else
                let a, b, c = parameters[i - 1]
                let w = input[i]
                let z' = z[i - 1]
                z[i] <- if z' % 26L + b <> w then z' / a * 26L + w + c else z' / a

                if a = 26L then
                    if z' % 26L + b = w then nextInput () else next i
                else if z' % 26L + b <> w then
                    nextInput ()
                else
                    next i

        input[1] <- start
        solve 1

    let rec toModalParameters parameters =
        function
        | [] -> parameters |> List.rev
        | Inp W :: Mul(X, Constant 0L) :: Add(X, Register Z) :: Mod(X, Constant 26L) :: Div(Z, Constant a) :: Add(X,
                                                                                                                  Constant b) :: Eql(X,
                                                                                                                                     Register W) :: Eql(X,
                                                                                                                                                        Constant 0L) :: Mul(Y,
                                                                                                                                                                            Constant 0L) :: Add(Y,
                                                                                                                                                                                                Constant 25L) :: Mul(Y,
                                                                                                                                                                                                                     Register X) :: Add(Y,
                                                                                                                                                                                                                                        Constant 1L) :: Mul(Z,
                                                                                                                                                                                                                                                            Register Y) :: Mul(Y,
                                                                                                                                                                                                                                                                               Constant 0L) :: Add(Y,
                                                                                                                                                                                                                                                                                                   Register W) :: Add(Y,
                                                                                                                                                                                                                                                                                                                      Constant c) :: Mul(Y,
                                                                                                                                                                                                                                                                                                                                         Register X) :: Add(Z,
                                                                                                                                                                                                                                                                                                                                                            Register Y) :: instructions ->
            toModalParameters ((a, b, c) :: parameters) instructions
        | instructions -> failwith $"Unrecognised parameters: {instructions}"

    let calculate direction instructions =
        instructions
        |> toModalParameters []
        |> solve direction
        |> List.fold (fun number digit -> number * 10L + digit) 0L

open Common24

type Problem24a() =
    member _.answer = Data |> calculate Down

type Problem24b() =
    member _.answer = Data |> calculate Up
