namespace AdventOfCode.Year2021

open Checked
open System.IO

module Input14 =
    let convert (lines: string[]) =
        let template :: _ :: inserts = lines |> Array.toList

        let inserts =
            inserts
            |> List.map (fun line ->
                let line = line.Split(' ')
                (line[0][0], line[0][1]), line[2][0])
            |> Map.ofList

        template |> Array.ofSeq, inserts

    let Sample =
        [| "NNCB"
           ""
           "CH -> B"
           "HH -> N"
           "CB -> H"
           "NH -> C"
           "HB -> C"
           "HC -> B"
           "HN -> C"
           "NN -> C"
           "BH -> H"
           "NC -> B"
           "NB -> B"
           "BN -> B"
           "BB -> N"
           "BC -> B"
           "CC -> N"
           "CN -> C" |]
        |> convert

    let Data = File.ReadLines("2021/14.txt") |> Seq.toArray |> convert

type Problem14a() =
    let expand inserts template =
        let n = template |> Array.length
        let result = Array.zeroCreate (n * 2 - 1)

        for i in 0 .. n - 2 do
            result[i * 2] <- template[i]
            result[i * 2 + 1] <- inserts |> Map.find (template[i], template[i + 1])

        result[(n - 1) * 2] <- template[n - 1]
        result

    let calculate runs (template, inserts) =
        let template =
            [ 1..runs ] |> List.fold (fun template _ -> expand inserts template) template

        let frequency = template |> Array.countBy id |> Array.sortByDescending snd
        snd frequency[0] - snd frequency[(frequency |> Array.length) - 1]

    do assert (Input14.Sample |> calculate 10 = 1588)

    member _.answer = Input14.Data |> calculate 10

type Problem14b() =

    let convert (template, inserts) =
        let lookup = inserts |> Map.toArray
        let inserts = lookup |> Array.mapi (fun i (pair, _) -> pair, i) |> Map.ofArray

        let lookup =
            lookup
            |> Array.map (fun (pair, insert) ->
                let char1, char2 = pair
                pair, (inserts |> Map.find (char1, insert), inserts |> Map.find (insert, char2)))

        let template =
            let frequency = Array.zeroCreate (inserts |> Map.count)

            for i in 0 .. (template |> Array.length) - 2 do
                let index = inserts |> Map.find (template[i], template[i + 1])
                frequency[index] <- frequency[index] + 1L

            frequency

        template, lookup

    let expand lookup (template: int64[]) =
        let n = lookup |> Array.length
        let result = Array.zeroCreate n

        for i in 0 .. n - 1 do
            let pair1, pair2 = lookup[i] |> snd
            result[pair1] <- result[pair1] + template[i]
            result[pair2] <- result[pair2] + template[i]

        result

    let calculate runs (template: char[], inserts) =
        let fistChar = template[0]
        let template, lookup = convert (template, inserts)

        let template =
            [ 1..runs ] |> List.fold (fun template _ -> expand lookup template) template

        let frequency =
            template
            |> Array.mapi (fun i freq -> lookup[i] |> fst |> snd, freq)
            |> Array.append [| fistChar, 1L |]
            |> Array.groupBy fst
            |> Array.map (fun (char, counts) -> char, counts |> Array.sumBy snd)
            |> Array.sortByDescending snd

        snd frequency[0] - snd frequency[(frequency |> Array.length) - 1]

    do assert (Input14.Sample |> calculate 10 = 1588L)

    do assert (Input14.Sample |> calculate 40 = 2188189693529L)

    member _.answer = Input14.Data |> calculate 40
