namespace AdventOfCode.Year2021

open Checked
open System
open System.IO
open AdventOfCode

module Input09 =
    let convert (rows: string[]) =
        let map = Array2D.zeroCreate rows.Length (rows[0].Length)

        rows
        |> Array.iteri (fun i row -> row |> Seq.iteri (fun j v -> map[i, j] <- Int32.Parse(string v)))

        map

    let Sample =
        [|
            "2199943210"
            "3987894921"
            "9856789892"
            "8767896789"
            "9899965678"
        |]
        |> convert

    let Data = File.ReadLines("2021/09.txt") |> Seq.toArray |> convert

module Common09 =
    let lowPoints map =
        seq {
            let m, n = map |> Array2D.length

            for i = 0 to m - 1 do
                for j = 0 to n - 1 do
                    let v = map[i, j]

                    let isLowest =
                        map
                        |> Array2D.permuteMoves (i, j)
                        |> List.map (fun (i, j) -> v < map[i, j])
                        |> List.forall id

                    if isLowest then
                        yield i, j
        }

type Problem09a() =
    let risk map =
        map |> Common09.lowPoints |> Seq.sumBy (fun (i, j) -> map[i, j] + 1)

    do assert (Input09.Sample |> risk = 15)

    member _.answer = Input09.Data |> risk

type Problem09b() =
    let basin (map: int[,]) (i, j) =
        let rec basin' points =
            function
            | (i, j) :: toVisit ->
                if points |> Set.contains (i, j) then
                    toVisit |> basin' points
                else
                    let v = map[i, j]

                    map
                    |> Array2D.permuteMoves (i, j)
                    |> List.filter (fun (i, j) -> map[i, j] > v && map[i, j] < 9)
                    |> List.append toVisit
                    |> basin' (points |> Set.add (i, j))
            | [] -> points

        basin' Set.empty [ i, j ]

    let score map =
        Common09.lowPoints map
        |> Seq.map (basin map >> Set.count)
        |> Seq.sortDescending
        |> Seq.take 3
        |> Seq.reduce (*)

    do assert (Input09.Sample |> score = 1134)

    member _.answer = Input09.Data |> score
