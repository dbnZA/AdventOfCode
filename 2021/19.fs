namespace AdventOfCode.Year2021

open Checked
open System
open System.IO

module Input19 =
    let convert (lines: string[]) =
        lines
        |> Array.fold
            (fun scans line ->
                if line = "" then
                    scans
                elif line.StartsWith("--- scanner ") then
                    [] :: scans
                else
                    let beacons :: scans = scans
                    let beacon = line.Split(',')

                    ((Int32.Parse(beacon[0]), Int32.Parse(beacon[1]), Int32.Parse(beacon[2]))
                     :: beacons)
                    :: scans)
            []
        |> List.map (List.rev >> List.toArray)
        |> List.rev
        |> List.toArray

    let Sample =
        """
--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let Data = File.ReadLines("2021/19.txt") |> Seq.toArray |> convert

module Common19 =
    let sq x = x * x

    let distance (x1, y1, z1) (x2, y2, z2) =
        sq (x2 - x1) + sq (y2 - y1) + sq (z2 - z1)

    let (-.) (x, y, z) (dx, dy, dz) = x - dx, y - dy, z - dz

    let (+.) (x, y, z) (dx, dy, dz) = x + dx, y + dy, z + dz

    let rotations =
        let inversions =
            [ (1, 1, 1)
              (1, 1, -1)
              (1, -1, 1)
              (1, -1, -1)
              (-1, 1, 1)
              (-1, 1, -1)
              (-1, -1, 1)
              (-1, -1, -1) ]

        let rotations = [ (0, 1, 2); (0, 2, 1); (1, 0, 2); (1, 2, 0); (2, 0, 1); (2, 1, 0) ]

        [ for rotation in rotations do
              for inversion in inversions do
                  rotation, inversion ]

    let rotate ((ix, iy, iz), (dx, dy, dz)) (x, y, z) =
        let point = [| x; y; z |]
        dx * point[ix], dy * point[iy], dz * point[iz]

    let tryPrimer scan1 (p11, p12) scan2 (p21, p22) =
        let p = p12 -. p11

        let rec tryPrimer =
            function
            | rotation :: rotations ->
                let p'1 = p21 |> rotate rotation
                let p'2 = p22 |> rotate rotation

                if p'2 -. p'1 = p then
                    let r' = rotate rotation >> (+.) (p11 -. p'1)

                    let overlappingBeacons =
                        scan2 |> Array.map r' |> Set.ofSeq |> Set.intersect (scan1 |> Set.ofSeq)

                    if overlappingBeacons.Count >= 12 then
                        Some r'
                    else
                        tryPrimer rotations
                else
                    tryPrimer rotations
            | [] -> None

        tryPrimer rotations

    let rotateScans sc rotation scans =
        Array.get scans sc |> Array.map rotation |> Set.ofSeq

    let bootstrap scans =
        let orientations = Array.zeroCreate (scans |> Array.length)
        orientations[0] <- Some id

        let rec bootstrap unmatched =
            function
            | ((sc1, p1), (sc2, p2)) :: primers ->
                match (sc1, p1, orientations[sc1]), (sc2, p2, orientations[sc2]) with
                | (sc1, p1, Some r1), (sc2, p2, None)
                | (sc2, p2, None), (sc1, p1, Some r1) ->
                    match tryPrimer scans[sc1] p1 scans[sc2] p2 with
                    | Some r' ->
                        orientations[sc2] <- Some(r' >> r1)
                        bootstrap unmatched primers
                    | None -> bootstrap (((sc1, p1), (sc2, p2)) :: unmatched) primers
                | (_, _, Some _), (_, _, Some _) -> bootstrap unmatched primers
                | _ -> bootstrap (((sc1, p1), (sc2, p2)) :: unmatched) primers
            | [] ->
                if orientations |> Array.forall Option.isSome then
                    orientations
                else
                    bootstrap [] unmatched

        scans
        |> Array.mapi (fun sc beacons ->
            [| for i in [ 0 .. (beacons |> Array.length) - 2 ] do
                   for j in [ i + 1 .. (beacons |> Array.length) - 1 ] do
                       let b1 = beacons[i]
                       let b2 = beacons[j]
                       yield distance b1 b2, (sc, (b1, b2)) |])
        |> Array.collect id
        |> Array.groupBy fst
        |> Array.choose (fun (_, scans) ->
            if scans.Length = 1 then
                None
            else
                Some(scans |> Array.map snd))
        |> Array.collect (fun primer -> Array.allPairs primer primer |> Array.filter (fun (p1, p2) -> p1 <> p2))
        |> Array.toList
        |> List.sortBy (fun ((sc1, _), (sc2, _)) -> sc1, sc2)
        |> bootstrap []

open Common19

type Problem19a() =
    let calculate scans =
        scans
        |> bootstrap
        |> Array.mapi (fun i (Some rotation) -> scans |> rotateScans i rotation)
        |> Array.reduce Set.union
        |> Set.count


    do assert (Input19.Sample |> calculate = 79)

    member _.answer = Input19.Data |> calculate

type Problem19b() =
    let calculate scans =
        scans
        |> bootstrap
        |> Array.map (fun (Some rotation) -> rotation (0, 0, 0))
        |> fun scanners -> Array.allPairs scanners scanners
        |> Array.map (fun ((x1, y1, z1), (x2, y2, z2)) -> abs (x2 - x1) + abs (y2 - y1) + abs (z2 - z1))
        |> Array.max

    do assert (Input19.Sample |> calculate = 3621)

    member _.answer = Input19.Data |> calculate
