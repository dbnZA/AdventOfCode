namespace AdventOfCode.Year2021

open Checked
open System.IO
open AdventOfCode

module Input25 =
    type Cell =
        | Empty = 0
        | East = 1
        | South = 2

    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            line
            |> Seq.map (function
                | '.' -> Cell.Empty
                | '>' -> Cell.East
                | 'v' -> Cell.South)
            |> Seq.toArray)
        |> Array2D.ofArrayRows

    let Sample =
        """
v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let Data = File.ReadLines("2021/25.txt") |> Seq.toArray |> convert

open Input25

type Problem25() =
    let (%) x m = (x % m + m) % m

    let moveEast grid =
        let m, n = grid |> Array2D.length

        Array2D.init m n (fun y x ->
            if grid[y, x] = Cell.Empty && grid[y, (x - 1) % n] = Cell.East then
                Cell.East
            elif grid[y, x] = Cell.East && grid[y, (x + 1) % n] = Cell.Empty then
                Cell.Empty
            else
                grid[y, x])

    let moveSouth grid =
        let m, n = grid |> Array2D.length

        Array2D.init m n (fun y x ->
            if grid[y, x] = Cell.Empty && grid[(y - 1) % m, x] = Cell.South then
                Cell.South
            elif grid[y, x] = Cell.South && grid[(y + 1) % m, x] = Cell.Empty then
                Cell.Empty
            else
                grid[y, x])

    let rec moves count grid =
        let newGrid = grid |> moveEast |> moveSouth
        if grid = newGrid then count else moves (count + 1) newGrid

    let calculate grid = grid |> moves 1

    do assert (Sample |> calculate = 58)

    member _.answer = Data |> calculate
