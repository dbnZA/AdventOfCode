namespace AdventOfCode.Year2022

open System.Numerics
open Checked
open System
open System.IO
open AdventOfCode

module Input11 =
    type Monkey =
        { Id: int
          Items: bigint list
          Operation: bigint -> bigint
          Test: bigint
          DestinationTrue: int
          DestinationFalse: int
          Inspections: int64 }

    let monkey =
        { Id = -1
          Items = []
          Operation = fun _ -> failwith "Unassigned operation"
          Test = BigInteger.MinusOne
          DestinationTrue = -1
          DestinationFalse = -1
          Inspections = 0L }

    let convert (lines: string[]) =
        let rec parse monkeys lines =
            match lines, monkeys with
            | [], monkeys -> monkeys |> List.rev |> List.toArray
            | [||] :: lines, monkeys -> parse monkeys lines
            | [| "Monkey"; id |] :: lines, monkeys ->
                parse
                    ({ monkey with
                        Id = Int32.Parse(id.TrimEnd(':')) }
                     :: monkeys)
                    lines
            | [| "Operation:"; "new"; "="; "old"; "*"; "old" |] :: lines, monkey :: monkeys ->
                parse
                    ({ monkey with
                        Operation = fun i -> i * i }
                     :: monkeys)
                    lines
            | [| "Operation:"; "new"; "="; "old"; op; value |] :: lines, monkey :: monkeys ->
                let op =
                    match op with
                    | "*" -> (*)
                    | "+" -> (+) |> (<|)

                parse
                    ({ monkey with
                        Operation = op (BigInteger.Parse(value)) }
                     :: monkeys)
                    lines
            | [| "Test:"; "divisible"; "by"; value |] :: lines, monkey :: monkeys ->
                parse
                    ({ monkey with
                        Test = BigInteger.Parse(value) }
                     :: monkeys)
                    lines
            | [| "If"; "true:"; "throw"; "to"; "monkey"; id |] :: lines, monkey :: monkeys ->
                parse
                    ({ monkey with
                        DestinationTrue = Int32.Parse(id) }
                     :: monkeys)
                    lines
            | [| "If"; "false:"; "throw"; "to"; "monkey"; id |] :: lines, monkey :: monkeys ->
                parse
                    ({ monkey with
                        DestinationFalse = Int32.Parse(id) }
                     :: monkeys)
                    lines
            | line :: lines, monkey :: monkeys when line[0].StartsWith("Starting") ->
                parse
                    ({ monkey with
                        Items =
                            line[2..]
                            |> Array.map (fun i -> BigInteger.Parse(i.TrimEnd(',')))
                            |> Array.toList }
                     :: monkeys)
                    lines
            | line :: _, _ -> failwith $"Unmatched line: %A{line}"

        lines
        |> Array.map _.Split(' ', StringSplitOptions.RemoveEmptyEntries)
        |> Array.toList
        |> parse []

    let sample =
        """
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/11.txt") |> Seq.toArray |> convert

    let round worry cycle monkeys =
        for i = 0 to Array.length monkeys - 1 do
            let monkey = monkeys[i]

            monkeys[i] <-
                { monkeys[i] with
                    Items = []
                    Inspections = monkey.Inspections + int64 (List.length monkey.Items) }

            for item in monkey.Items do
                let item = monkey.Operation item / worry % cycle

                let destination =
                    if item % monkey.Test = BigInteger.Zero then
                        monkeys[monkey.DestinationTrue]
                    else
                        monkeys[monkey.DestinationFalse]

                monkeys[destination.Id] <-
                    { destination with
                        Items = item :: destination.Items }

        monkeys

    let rounds n worry monkeys =
        let cycle = monkeys |> Array.map _.Test |> Array.reduce (*)

        let monkeys =
            (monkeys |> Array.copy, seq { 1..n })
            ||> Seq.fold (fun state _ -> round worry cycle state)
            |> Array.sortByDescending _.Inspections

        monkeys[0].Inspections * monkeys[1].Inspections

open Input11

type Problem11a() =
    let calculate = rounds 20 (bigint 3)

    do ``assert`` 10605L (calculate sample)

    member _.answer = calculate data

type Problem11b() =
    let calculate = rounds 10_000 BigInteger.One

    //do allCycles data

    do ``assert`` 2713310158L (calculate sample)

    member _.answer = calculate data
