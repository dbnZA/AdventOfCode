namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input20 =
    let convert (lines: string[]) = lines |> Array.map Int64.Parse

    let sample =
        """
1
2
-3
3
-2
0
4
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/20.txt") |> Seq.toArray |> convert

    let rotate (index: ResizeArray<_>) (input: _ seq) =
        let input = ResizeArray(input)
        let len = int64 index.Count - 1L

        for c = 0 to index.Count - 1 do
            let i = index.IndexOf(c)
            let x = input[i]
            index.RemoveAt(i)
            input.RemoveAt(i)
            let j = int32 <| ((int64 i + x) % len + len) % len
            index.Insert(j, c)
            input.Insert(j, x)

        input

    let coords (input: ResizeArray<int64>) =
        let i = input.IndexOf(0)
        let value index = input[(i + index) % input.Count]
        value 1_000 + value 2_000 + value 3_000

open Input20

type Problem20a() =
    let calculate input =
        let len = input |> Array.length
        let index = ResizeArray(seq { for i in 0 .. len - 1 -> i })
        rotate index input |> coords

    do ``assert`` 3L (calculate sample)

    member _.answer = calculate data

type Problem20b() =
    let calculate input =
        let len = input |> Array.length
        let index = ResizeArray(seq { for i in 0 .. len - 1 -> i })

        (ResizeArray(seq { for i in input -> i * 811589153L }), [ 1..10 ])
        ||> List.fold (fun input _ -> rotate index input)
        |> coords

    do ``assert`` 1623178306L (calculate sample)

    member _.answer = calculate data
