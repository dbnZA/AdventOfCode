namespace AdventOfCode.Year2022

open Checked
open System.IO
open AdventOfCode

module Input24 =
    let convert (lines: string[]) =
        lines[1..^1]
        |> Array.map (fun line ->
            line[1..^1]
            |> Array.ofSeq
            |> Array.map (function
                | '.' -> []
                | '<' -> [ 0, -1 ]
                | '>' -> [ 0, 1 ]
                | '^' -> [ -1, 0 ]
                | 'v' -> [ 1, 0 ]))
        |> Array2D.ofArrayRows

    let sample =
        """
#.######
#>>.<^<#
#.<..<<#
#>v.><>#
#<^v^^>#
######.#
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/24.txt") |> Seq.toArray |> convert

    let inline (%) x m = if x < 0 then (x + m) % m else x % m

    let update grid =
        let xMax, yMax = grid |> Array2D.length
        let nextGrid = Array2D.create xMax yMax []

        grid
        |> Array2D.iteri (fun x y blizzards ->
            blizzards
            |> List.iter (fun (dx, dy) ->
                let x = (x + dx) % xMax
                let y = (y + dy) % yMax
                nextGrid[x, y] <- (dx, dy) :: nextGrid[x, y]))

        nextGrid

    let validMoves = [| -1, 0; 1, 0; 0, -1; 0, 1; 0, 0 |]

    let moves start grid (x, y) =
        let xMax, yMax = grid |> Array2D.length

        [ for dx, dy in validMoves do
              let x = x + dx
              let y = y + dy

              if (x, y) = start then
                  yield start
              elif 0 <= x && x < xMax && 0 <= y && y < yMax && List.isEmpty grid[x, y] then
                  yield x, y ]

    let rec stepPaths start finish grid len paths =
        function
        | [] when paths = [] -> failwith "No solution"
        | [] -> stepPaths start finish (update grid) (len + 1) [] (paths |> List.distinct)
        | coord :: _ when coord = finish -> grid, len
        | coord :: oldPaths -> stepPaths start finish grid len ((moves start grid coord) @ paths) oldPaths

open Input24

type Problem24a() =
    let calculate input =
        let x, y = input |> Array2D.length
        stepPaths (-1, 0) (x - 1, y - 1) (update input) 1 [] [ -1, 0 ] |> snd

    do ``assert`` 18 (calculate sample)

    member _.answer = calculate data

type Problem24b() =
    let calculate input =
        let x, y = input |> Array2D.length
        let grid, len = stepPaths (-1, 0) (x - 1, y - 1) (update input) 1 [] [ -1, 0 ]
        let grid, len = stepPaths (x, y - 1) (0, 0) grid len [] [ x, y - 1 ]
        stepPaths (-1, 0) (x - 1, y - 1) grid len [] [ -1, 0 ] |> snd

    do ``assert`` 54 (calculate sample)

    member _.answer = calculate data
