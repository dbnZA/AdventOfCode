namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input14 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            line.Split(" -> ")
            |> Array.map (fun coord ->
                let [| x; y |] = coord.Split(",")
                Int32.Parse(x), Int32.Parse(y))
            |> Array.toList)
        |> Array.toList

    let sample =
        """
498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/14.txt") |> Seq.toArray |> convert

    let createCave fullSize input =
        let allCoord = input |> List.concat
        let xMax = allCoord |> List.maxOf fst
        let yMax = allCoord |> List.maxOf snd

        let grid =
            if fullSize then
                Array2D.zeroCreate 1001 (yMax + 3)
            else
                Array2D.zeroCreate (xMax + 1) (yMax + 1)

        for coords in input do
            for (x1, y1), (x2, y2) in coords |> List.pairwise do
                let dx = x2 - x1
                let dy = y2 - y1

                if dx = 0 then
                    for y in y1 .. sign dy .. y2 do
                        grid[x1, y] <- true
                else
                    for x in x1 .. sign dx .. x2 do
                        grid[x, y1] <- true

        if fullSize then
            for x = 0 to 1000 do
                grid[x, yMax + 2] <- true

        grid

    let pourSand grid =
        let xMax, yMax = grid |> Array2D.length

        let inBounds (x, y) =
            0 <= x && x < xMax && 0 <= y && y < yMax

        let canMove (x, y) = not (inBounds (x, y)) || not grid[x, y]

        let rec move (x, y) =
            if not (inBounds (x, y)) then
                false
            elif canMove (x, y + 1) then
                move (x, y + 1)
            elif canMove (x - 1, y + 1) then
                move (x - 1, y + 1)
            elif canMove (x + 1, y + 1) then
                move (x + 1, y + 1)
            elif grid[x, y] then
                false
            else
                grid[x, y] <- true
                true

        let rec pour count =
            if move (500, 0) then pour (count + 1) else count

        pour 0

open Input14

type Problem14a() =
    let calculate input = input |> createCave false |> pourSand

    do ``assert`` 24 (calculate sample)

    member _.answer = calculate data

type Problem14b() =
    let calculate input = input |> createCave true |> pourSand

    do ``assert`` 93 (calculate sample)

    member _.answer = calculate data
