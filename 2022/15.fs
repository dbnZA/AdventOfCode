namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input15 =
    type View =
        | Start of int
        | Expanding of (int * int) * int
        | Contracting of (int * int) * int

    let convert (lines: string[]) =
        let toCoord [| x; y |] =
            let toInt (value: string) =
                value.Split([| '='; ','; ':' |])[1] |> Int32.Parse

            toInt x, toInt y

        lines
        |> Array.map (fun line ->
            let line = line.Split()
            toCoord line[2..3], toCoord line[8..9])
        |> Array.toList

    let sample =
        """
Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/15.txt") |> Seq.toArray |> convert

    let distance ((x1, y1), (x2, y2)) = abs (x1 - x2) + abs (y1 - y2)

open Input15

type Problem15a() =
    let calculate row input =
        let coverage =
            input
            |> List.choose (fun (xSignal, ySignal as signal, beacon) ->
                let span = distance (signal, beacon) - abs (ySignal - row)

                if span >= 0 then
                    Some [ xSignal - span .. xSignal + span ]
                else
                    None)
            |> List.concat
            |> List.distinct
            |> List.length

        let beacons =
            input
            |> List.choose (fun (_, (x, y)) -> if y = row then Some x else None)
            |> List.distinct
            |> List.length

        coverage - beacons

    do ``assert`` 26 (calculate 10 sample)

    member _.answer = calculate 2_000_000 data

type Problem15b() =
    let calculate box input =
        let boxes row =
            input
            |> List.choose (fun (xSignal, ySignal as signal, beacon) ->
                let size = distance (signal, beacon)

                if row > ySignal + size then
                    None
                elif row < ySignal - size then
                    Some(Start(ySignal - size))
                else
                    let rowSize = size - abs (ySignal - row)
                    let span = (xSignal - rowSize, xSignal + rowSize)

                    if ySignal > row then
                        Some(Expanding(span, ySignal))
                    else
                        Some(Contracting(span, ySignal + size + 1)))

        let nextRow row boxes =
            List.allPairs boxes boxes
            |> List.choose (function
                | Contracting((l1, r1), _), Contracting((l2, r2), _) ->
                    let overlap = max (l2 - r1) (r2 - l1)
                    if overlap > 0 then Some(overlap / 2 + row + 1) else None
                | Start row, _
                | Expanding(_, row), _
                | Contracting(_, row), _ -> Some row)
            |> List.min

        let isOverlap column boxes =
            boxes
            |> List.exists (function
                | Start _ -> false
                | Expanding((l, r), _)
                | Contracting((l, r), _) -> l <= column && column <= r)

        let tryFindEmpty boxes =
            boxes
            |> List.choose (function
                | Start _ -> None
                | Expanding((l, r), _)
                | Contracting((l, r), _) -> Some [ l - 1; r + 1 ])
            |> List.concat
            |> List.tryFind (fun column -> 0 <= column && column <= box && not (isOverlap column boxes))

        let rec findEmpty row =
            let boxes = boxes row

            match tryFindEmpty boxes with
            | Some column -> int64 column * 4_000_000L + int64 row
            | None -> findEmpty (nextRow row boxes)

        findEmpty 0

    do ``assert`` 56000011L (calculate 20 sample)

    member _.answer = calculate 4_000_000 data
