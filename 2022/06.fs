namespace AdventOfCode.Year2022

open Checked
open System.IO
open AdventOfCode

module Input06 =
    let convert (lines: string[]) = lines[0] |> Seq.toArray

    let sample =
        """
mjqjpqmgbljsphdztnvjfqwrcgsmlb
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/06.txt") |> Seq.toArray |> convert

    let rec checkUnique input window i =
        let areUnique =
            Array.sub input i window
            |> Array.groupBy id
            |> Array.forall (fun (_, chars) -> Array.length chars = 1)

        if areUnique then
            i + window
        else
            checkUnique input window (i + 1)


open Input06

type Problem06a() =
    let calculate input = checkUnique input 4 0

    do ``assert`` 7 (calculate sample)

    member _.answer = calculate data

type Problem06b() =
    let calculate input = checkUnique input 14 0

    do ``assert`` 19 (calculate sample)

    member _.answer = calculate data
