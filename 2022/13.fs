namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input13 =
    type Node =
        | List of Node list
        | Value of int

    let convert (lines: string[]) =
        let rec (|TNode|_|) =
            function
            | TValue(value, data) -> Some(value, data)
            | TList(value, data) -> Some(value, data)
            | _ -> None

        and (|TValue|_|) =
            function
            | TDigit(number, TNumberBody(scale, value, data)) -> Some(Value(number * scale + value), data)
            | _ -> None

        and (|TNumberBody|_|) =
            function
            | TDigit(number, TNumberBody(scale, value, data)) -> Some(scale * 10, number * scale + value, data)
            | data -> Some(1, 0, data)

        and (|TDigit|_|) =
            function
            | char :: data when int char >= int '0' && int char <= int '9' -> Some(int char - int '0', data)
            | _ -> None

        and (|TList|_|) =
            function
            | '[' :: ']' :: data -> Some(List [], data)
            | '[' :: TNode(node, TListBody(nodes, data)) -> Some(List(node :: nodes), data)
            | _ -> None

        and (|TListBody|_|) =
            function
            | ']' :: data -> Some([], data)
            | ',' :: TNode(node, TListBody(nodes, data)) -> Some(node :: nodes, data)
            | _ -> None

        lines
        |> Array.choose (fun line ->
            line
            |> List.ofSeq
            |> function
                | [] -> None
                | TNode(node, []) -> Some node
                | _ -> failwith $"Unrecognised line: {line}")
        |> Array.toList

    let sample =
        """
[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/13.txt") |> Seq.toArray |> convert

    let compare pair =
        let rec compare =
            function
            | Value x1, Value x2 when x1 < x2 -> Some true
            | Value x1, Value x2 when x1 > x2 -> Some false
            | Value _, Value _ -> None
            | List x1, List x2 -> compareList (x1, x2)
            | List _ as x1, (Value _ as x2) -> compare (x1, List [ x2 ])
            | Value _ as x1, (List _ as x2) -> compare (List [ x1 ], x2)

        and compareList =
            function
            | [], [] -> None
            | [], _ -> Some true
            | _, [] -> Some false
            | e1 :: x1, e2 :: x2 ->
                match compare (e1, e2) with
                | Some result -> Some result
                | None -> compareList (x1, x2)

        if compare pair = Some true then 1 else -1


open Input13

type Problem13a() =
    let calculate input =
        input
        |> List.chunkBySize 2
        |> List.mapi (fun i [ x1; x2 ] -> if compare (x1, x2) = 1 then i + 1 else 0)
        |> List.sum

    do ``assert`` 13 (calculate sample)

    member _.answer = calculate data

type Problem13b() =
    let calculate input =
        let v2 = List [ List [ Value 2 ] ]
        let v6 = List [ List [ Value 6 ] ]

        let sorted =
            v2 :: v6 :: input
            |> List.toArray
            |> fun array ->
                Array.Sort(array, (fun x1 x2 -> compare (x2, x1)))
                array

        let i2 = sorted |> Array.findIndex ((=) v2)
        let i6 = sorted |> Array.findIndex ((=) v6)
        (i2 + 1) * (i6 + 1)

    do ``assert`` 140 (calculate sample)

    member _.answer = calculate data
