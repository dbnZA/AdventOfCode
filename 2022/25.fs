namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input25 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            line
            |> Seq.toList
            |> List.map (function
                | '=' -> -2L
                | '-' -> -1L
                | '0' -> 0L
                | '1' -> 1L
                | '2' -> 2L))

    let sample =
        """
1=-0-2
12111
2=0=
21
2=01
111
20012
112
1=-1=
1-12
12
1=
122
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/25.txt") |> Seq.toArray |> convert

    let toInt64 = List.fold (fun value digit -> value * 5L + digit) 0L

    let rec toSnafu snafu value =
        if value = 0L then
            snafu |> Array.ofList |> String
        else
            let value, digit = Math.DivRem(value, 5L)

            if digit = 3 then toSnafu ('=' :: snafu) (value + 1L)
            elif digit = 4 then toSnafu ('-' :: snafu) (value + 1L)
            else toSnafu (char (digit + int64 '0') :: snafu) value

open Input25

type Problem25() =
    let calculate input =
        input |> Array.map toInt64 |> Array.sum |> toSnafu []

    do ``assert`` "2=-1=0" (calculate sample)

    member _.answer = calculate data
