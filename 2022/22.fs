namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input22 =
    type Instruction =
        | Right
        | Left
        | Move of int

    let convert (lines: string[]) =
        let len = lines |> Array.length
        let padding = lines[0 .. len - 3] |> Array.maxOf _.Length

        let map =
            lines[0 .. len - 3]
            |> Array.map (fun line -> (line + String.replicate (padding - line.Length) " " |> Array.ofSeq))
            |> Array2D.ofArrayRows

        let rec (|Instructions|) =
            function
            | [] -> []
            | Instruction(instruction, Instructions(instructions)) -> instruction :: instructions

        and (|Instruction|_|) =
            function
            | 'R' :: data -> Some(Right, data)
            | 'L' :: data -> Some(Left, data)
            | TDigit(number, TNumberBody(scale, value, data)) -> Some(Move(number * scale + value), data)
            | _ -> None

        and (|TNumberBody|_|) =
            function
            | TDigit(number, TNumberBody(scale, value, data)) -> Some(scale * 10, number * scale + value, data)
            | data -> Some(1, 0, data)

        and (|TDigit|_|) =
            function
            | char :: data when int char >= int '0' && int char <= int '9' -> Some(int char - int '0', data)
            | _ -> None

        let instructions =
            match lines[len - 1] |> List.ofSeq with
            | Instructions instructions -> instructions

        map, instructions

    let sample =
        """
        ...#
        .#..
        #...
        ....
...#.......#
........#...
..#....#....
..........#.
        ...#....
        .....#..
        .#......
        ......#.

10R5L5R10L4R5L5
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/22.txt") |> Seq.toArray |> convert

    let (%) x m = if x < 0 then (x + m) % m else x % m

    let turnLeft orientation = (orientation - 1) % 4

    let turnRight orientation = (orientation + 1) % 4

    let reverse orientation = (orientation + 2) % 4

    let moves next (grid, instructions) =
        let op (orientation, coord) =
            function
            | Right -> turnRight orientation, coord
            | Left -> turnLeft orientation, coord
            | Move steps -> repeat steps next (orientation, coord)

        ((0, grid |> Array2D.findByColumn '.'), instructions)
        ||> List.fold op
        |> fun (orientation, (x, y)) -> 1000 * (x + 1) + 4 * (y + 1) + orientation

    let direction = [| 0, 1; 1, 0; 0, -1; -1, 0 |]

open Input22

type Problem22a() =
    let calculate (grid, instructions as vector) =
        let xMax, yMax = grid |> Array2D.length

        let next (orientation, coord as vector) =
            let rec next orientation (x, y as coord) =
                let dx, dy = direction[orientation]
                let x = (x + dx) % xMax
                let y = (y + dy) % yMax

                if grid[x, y] = ' ' then next orientation (x, y)
                elif grid[x, y] = '#' then vector
                else orientation, (x, y)

            let o, (x, y) = next orientation coord
            if grid[x, y] = ' ' then vector else o, (x, y)

        moves next (grid, instructions)


    do ``assert`` 6032 (calculate sample)

    member _.answer = calculate data

type Problem22b() =
    let innerCorner grid =
        let xMax, yMax = grid |> Array2D.length

        seq {
            for x = 0 to xMax - 2 do
                for y = 0 to yMax - 2 do
                    let aa = grid[x, y]
                    let ab = grid[x, y + 1]
                    let ba = grid[x + 1, y]
                    let bb = grid[x + 1, y + 1]
                    let isBlank c = 1 - Convert.ToInt32(c <> ' ')

                    if isBlank aa + isBlank ab + isBlank ba + isBlank bb = 1 then
                        match aa, ab, ba, bb with
                        | ' ', _, _, _ -> (2, (x + 1, y + 1)), (3, (x + 1, y + 1))
                        | _, ' ', _, _ -> (3, (x + 1, y)), (0, (x + 1, y))
                        | _, _, _, ' ' -> (0, (x, y)), (1, (x, y))
                        | _, _, ' ', _ -> (1, (x, y + 1)), (2, (x, y + 1))
        }

    let move grid (orientation, (x, y)) =
        let xMax, yMax = grid |> Array2D.length
        let dx, dy = direction[orientation]
        let x = x + dx
        let y = y + dy

        if x < 0 || xMax <= x || y < 0 || yMax <= y || grid[x, y] = ' ' then
            None
        else
            Some(x, y)

    let rec walk grid coords ((lo, left), (ro, right)) =
        let coords = ((turnLeft lo, left), (turnRight ro, right)) :: coords

        match move grid (lo, left), move grid (ro, right) with
        | None, None -> coords
        | Some left, Some right -> walk grid coords ((lo, left), (ro, right))
        | Some left, None -> walk grid coords ((lo, left), (turnRight ro, right))
        | None, Some right -> walk grid coords ((turnLeft lo, left), (ro, right))

    let calculate (grid, instructions) =
        let pairs =
            innerCorner grid
            |> Seq.collect (walk grid [])
            |> Seq.collect (fun ((lo, left), (ro, right)) ->
                [ (reverse lo, left), (ro, right); (reverse ro, right), (lo, left) ])
            |> Map.ofSeq

        let next (orientation, _ as vector) =
            let o, (x, y) =
                match move grid vector with
                | Some coord -> orientation, coord
                | None -> pairs |> Map.find vector

            if grid[x, y] = '#' then vector else o, (x, y)

        moves next (grid, instructions)

    do ``assert`` 5031 (calculate sample)

    member _.answer = calculate data
