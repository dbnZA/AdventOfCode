namespace AdventOfCode.Year2022

open Checked
open System
open System.IO

module Input01 =
    let convert (lines: string[]) =
        let rec calc total totals =
            function
            | [] -> total :: totals
            | None :: input -> calc 0 (total :: totals) input
            | Some value :: input -> calc (total + value) totals input

        lines
        |> Array.map (fun line ->
            match Int32.TryParse(line) with
            | true, value -> Some value
            | false, _ -> None)
        |> Array.toList
        |> calc 0 []

    let Sample =
        """
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let Data = File.ReadLines("2022/01.txt") |> Seq.toArray |> convert

type Problem01a() =
    let calculate = List.max

    do assert (Input01.Sample |> calculate = 24_000)

    member _.answer = Input01.Data |> calculate

type Problem01b() =
    let calculate input =
        input |> List.sortDescending |> List.take 3 |> List.sum

    do assert (Input01.Sample |> calculate = 45000)

    member _.answer = Input01.Data |> calculate
