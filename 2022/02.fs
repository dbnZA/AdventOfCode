namespace AdventOfCode.Year2022

open Checked
open System.IO

module Input02 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            match line |> Seq.toArray with
            | [| first; ' '; second |] -> int first - int 'A', int second - int 'X')

    let Sample =
        """
A Y
B X
C Z
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let Data = File.ReadLines("2022/02.txt") |> Seq.toArray |> convert

open Input02

type Problem02a() =
    let calculate input =
        input
        |> Array.map (fun (first, second) ->
            if (first + 1) % 3 = second then second + 7
            elif first = second then second + 4
            else second + 1)
        |> Array.sum

    do assert (Sample |> calculate = 15)

    member _.answer = Data |> calculate

type Problem02b() =
    let calculate input =
        input
        |> Array.sumBy (fun (shape, outcome) -> outcome * 3 + (shape + 2 + outcome) % 3 + 1)

    do assert (Sample |> calculate = 12)

    member _.answer = Data |> calculate
