namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input05 =
    let convert (lines: string[]) =
        let rec parseCrates crates (line :: lines) =
            if line <> "" then
                parseCrates ([ for i in 1..4 .. line.Length -> line[i] ] :: crates) lines
            else
                let labels :: crates = crates
                let stacks = labels |> List.length

                let crates =
                    crates
                    |> List.map (fun row -> row @ [ for _ in 1 .. stacks - List.length row -> ' ' ])
                    |> List.rev
                    |> List.transpose
                    |> List.map (List.skipWhile ((=) ' '))

                let moves =
                    lines
                    |> List.map (fun line ->
                        let instruction = line.Split()

                        Int32.Parse(instruction[1]), Int32.Parse(instruction[3]) - 1, Int32.Parse(instruction[5]) - 1)

                moves, crates

        lines |> Array.toList |> parseCrates []


    let sample =
        """
    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/05.txt") |> Seq.toArray |> convert

open Input05

type Problem05a() =
    let calculate (moves, crates) =
        let crates = crates |> List.toArray

        let rec move (count, from, ``to``) =
            if count = 0 then
                ()
            else
                let crate :: stack = crates[from]
                crates[from] <- stack
                crates[``to``] <- crate :: crates[``to``]
                move ((count - 1), from, ``to``)

        moves |> List.iter move
        crates |> Array.map List.head |> String

    do ``assert`` "CMZ" (calculate sample)

    member _.answer = calculate data

type Problem05b() =
    let calculate (moves, crates) =
        let crates = crates |> List.toArray

        let move (count, from, ``to``) =
            let crate, stack = crates[from] |> List.splitAt count
            crates[from] <- stack
            crates[``to``] <- crate @ crates[``to``]

        moves |> List.iter move
        crates |> Array.map List.head |> String

    do ``assert`` "MCD" (calculate sample)

    member _.answer = calculate data
