namespace AdventOfCode.Year2022

open Checked
open System.IO
open AdventOfCode

module Input08 =
    let convert (lines: string[]) =
        lines
        |> Array.map (Seq.map (fun char -> int char - int '0') >> Seq.toArray)
        |> Array2D.ofArrayRows

    let sample =
        """
30373
25512
65332
33549
35390
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/08.txt") |> Seq.toArray |> convert

open Input08

type Problem08a() =
    let calculate (input: int[,]) =
        let rec rotate n input visible =
            let visible = visible |> Array2D.rotateLeft

            if n = 4 then
                visible
            else
                let input = input |> Array2D.rotateLeft

                (visible, [ 0 .. Array2D.length1 visible - 1 ])
                ||> List.fold (fun visible r ->
                    visible[r, 0] <- 1

                    (visible, [ 0 .. Array2D.length2 visible - 2 ])
                    ||> List.fold (fun visible c ->
                        if input[r, 0 .. c - 1] |> Array.forall (fun x -> x < input[r, c]) then
                            visible[r, c] <- 1

                        visible))
                |> rotate (n + 1) input

        input
        |> Array2D.length
        ||> Array2D.zeroCreate
        |> rotate 0 input
        |> Array2D.sumBy id

    do ``assert`` 21 (calculate sample)

    member _.answer = calculate data

type Problem08b() =
    let calculate input =
        let rec rotate n input scenic =
            let scenic = scenic |> Array2D.rotateLeft

            if n = 4 then
                scenic
            else
                let input = input |> Array2D.rotateLeft

                (scenic, [ 1 .. Array2D.length1 scenic - 2 ])
                ||> List.fold (fun scenic r ->
                    (scenic, [ 1 .. Array2D.length2 scenic - 2 ])
                    ||> List.fold (fun scenic c ->
                        let visibleTrees =
                            input[r, 0 .. c - 1]
                            |> Array.rev
                            |> Array.takeWhile (fun x -> x < input[r, c])
                            |> Array.length

                        scenic[r, c] <- scenic[r, c] * (visibleTrees + if visibleTrees < c then 1 else 0)
                        scenic))
                |> rotate (n + 1) input

        (input |> Array2D.length ||> Array2D.create) 1 |> rotate 0 input |> Array2D.max

    do ``assert`` 8 (calculate sample)

    member _.answer = calculate data
