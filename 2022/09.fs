namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input09 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let [| direction; steps |] = line.Split()
            direction, Int32.Parse(steps))

    let sample =
        """
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/09.txt") |> Seq.toArray |> convert

    let move direction (hx, hy) =
        match direction with
        | "R" -> hx + 1, hy
        | "U" -> hx, hy + 1
        | "D" -> hx, hy - 1
        | "L" -> hx - 1, hy

    let touch (hx: int, hy: int) (tx, ty as t) =
        if max (abs (hx - tx)) (abs (hy - ty)) <= 1 then
            t
        else
            tx + hx.CompareTo(tx), ty + hy.CompareTo(ty)

    let move1 direction (h, t) =
        let h = move direction h
        h, touch h t

    let moveN direction (rope: (int * int) array) =
        let dup x = x, x
        let h = move direction rope[0]

        (h, rope[1..])
        ||> Array.mapFold (fun h t -> touch h t |> dup)
        |> fst
        |> Array.append [| h |]

open Input09

type Problem09a() =
    let calculate input =
        (((0, 0), (0, 0)), input)
        ||> Array.mapFold (fun state (direction, moves) ->
            (state, [| 1..moves |])
            ||> Array.mapFold (fun state _ ->
                let state = move1 direction state
                snd state, state))
        |> fst
        |> Array.concat
        |> Array.distinct
        |> Array.length

    do ``assert`` 13 (calculate sample)

    member _.answer = calculate data

type Problem09b() =
    let calculate input =
        (Array.create 10 (0, 0), input)
        ||> Array.mapFold (fun state (direction, moves) ->
            (state, [| 1..moves |])
            ||> Array.mapFold (fun state _ ->
                let state = moveN direction state
                Array.last state, state))
        |> fst
        |> Array.concat
        |> Array.distinct
        |> Array.length

    do ``assert`` 1 (calculate sample)

    member _.answer = calculate data
