namespace AdventOfCode.Year2022

open System
open System.IO
open AdventOfCode

module Input04 =
    let convert =
        Array.map (fun (line: string) ->
            let splat [| x; y |] = x, y

            let split (line: string) =
                line.Split('-') |> Array.map Int32.Parse |> splat

            line.Split(',') |> Array.map split |> splat)

    let sample =
        """
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/04.txt") |> Seq.toArray |> convert

open Input04

type Problem04a() =
    let calculate input =
        let overlap (x1, x2) (y1, y2) = x1 <= y1 && x2 >= y2
        input |> Array.countIf (fun (x, y) -> overlap x y || overlap y x)

    do ``assert`` 2 (calculate sample)

    member _.answer = calculate data

type Problem04b() =
    let calculate input =
        let leftOf (_, x2) (y1, _) = x2 < y1

        input |> Array.countIf (fun (x, y) -> not (leftOf x y) && not (leftOf y x))

    do ``assert`` 4 (calculate sample)

    member _.answer = calculate data
