namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input16 =
    type Action =
        | TravelTo of string * int
        | OpenValve of string
        | Finished

    type Path =
        { Visited: string Set
          ReleaseRate: int
          TotalRelease: int
          Action: Action }

    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let line = line.Split()
            let valve = line[1]
            let rate = line[4]
            let tunnels = line[9..]

            valve, (rate.Split('=', ';')[1] |> Int32.Parse, tunnels |> Array.map _.TrimEnd(',') |> Array.toList))
        |> Map.ofArray

    let sample =
        """
Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/16.txt") |> Seq.toArray |> convert

    [<Literal>]
    let maxTicks = 30

    let remap input =
        let rec visit map =
            function
            | [] -> map
            | (length, node) :: nodes ->
                match map |> Map.tryFind node with
                | Some bestLength when bestLength <= length -> visit map nodes
                | _ ->
                    let map = map |> Map.add node length

                    match input |> Map.find node with
                    | rate, _ when rate > 0 -> visit map nodes
                    | _, tunnels -> visit map (List.map (fun valve -> length + 1, valve) tunnels @ nodes)

        input
        |> Map.map (fun _ (rate, tunnels) ->
            rate,
            visit Map.empty (tunnels |> List.map (fun valve -> 1, valve))
            |> Map.toList
            |> List.filter (fun (valve, _) -> input |> Map.find valve |> fst > 0))

    let act input state =
        let travelFrom valve (state: Path) =
            let _, tunnels = input |> Map.find valve
            tunnels |> List.map (fun tunnel -> { state with Action = TravelTo tunnel })

        let state =
            match state.Action with
            | OpenValve valve ->
                { state with
                    ReleaseRate = state.ReleaseRate + fst (input |> Map.find valve)
                    Visited = state.Visited |> Set.add valve }
            | _ -> state

        let state =
            { state with
                TotalRelease = state.TotalRelease + state.ReleaseRate }

        match state.Action with
        | Finished -> [ state ]
        | TravelTo(valve, 1) ->
            let states = travelFrom valve state

            if input |> Map.find valve |> fst = 0 || state.Visited |> Set.contains valve then
                states
            else
                { state with Action = OpenValve valve } :: states
        | TravelTo(valve, length) ->
            [ { state with
                  Action = TravelTo(valve, length - 1) } ]
        | OpenValve valve ->
            if Set.count state.Visited = Map.count input then
                [ { state with Action = Finished } ]
            else
                travelFrom valve state

    let totalRelease tick state =
        state.TotalRelease + (maxTicks + 1 - tick) * state.ReleaseRate


open Input16

type Problem16a() =
    let calculate input =
        let input = remap input

        let rec action acted =
            function
            | [] -> acted
            | state :: toAct -> action (act input state @ acted) toAct

        let state =
            { Visited = Set.empty
              ReleaseRate = 0
              TotalRelease = 0
              Action = TravelTo("AA", 1) }

        ([ state ], [ 1..maxTicks ])
        ||> List.fold (fun states tick ->
            states
            |> List.groupBy _.Action
            |> List.map (fun (_, states) -> states |> List.maxBy (totalRelease tick))
            |> action [])
        |> List.maxOf _.TotalRelease

    do ``assert`` 1651 (calculate sample)

    member _.answer = calculate data

type Problem16b() =
    let calculate input =
        let input = remap input

        let rec action acted =
            function
            | [] -> acted
            | (me, elephant) :: toAct ->
                let acted =
                    let states =
                        List.allPairs (act input me) (act input elephant)
                        |> List.filter (function
                            | { Visited = me }, { Visited = elephant } -> Set.intersect me elephant |> Set.isEmpty)

                    states @ acted

                action acted toAct

        let state =
            { Visited = Set.empty
              ReleaseRate = 0
              TotalRelease = 0
              Action = TravelTo("AA", 1) },
            { Visited = Set.empty
              ReleaseRate = 0
              TotalRelease = 0
              Action = TravelTo("AA", 1) }

        ([ state ], [ 5..maxTicks ])
        ||> List.fold (fun states tick ->
            states
            |> List.groupBy (fun (me, elephant) -> me.Action, elephant.Action)
            |> List.map (fun (_, states) ->
                states
                |> List.maxBy (fun (me, elephant) -> totalRelease tick me + totalRelease tick elephant))
            |> action [])
        |> List.maxOf (fun (me, elephant) -> me.TotalRelease + elephant.TotalRelease)

    do ``assert`` 1707 (calculate sample)

    member _.answer = calculate data
