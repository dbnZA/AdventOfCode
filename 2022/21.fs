namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input21 =
    type Op =
        | Add
        | Subtract
        | Multiply
        | Divide

    type Job =
        | Number of int64
        | Op of string * string * Op

    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let [| name; operation |] = line.Split(':')
            let operations = operation.Split(' ', StringSplitOptions.RemoveEmptyEntries)

            if Array.length operations = 1 then
                name, Number(Int64.Parse operations[0])
            else
                let op =
                    match operations[1] with
                    | "+" -> Add
                    | "-" -> Subtract
                    | "*" -> Multiply
                    | "/" -> Divide

                name, Op(operations[0], operations[2], op))
        |> Map.ofArray

    let sample =
        """
root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/21.txt") |> Seq.toArray |> convert

    let operation =
        function
        | Add -> (+)
        | Subtract -> (-)
        | Multiply -> (*)
        | Divide -> (/)


open Input21

type Problem21a() =
    let rec calc input name =
        match input |> Map.find name with
        | Number value -> value
        | Op(left, right, op) -> operation op (calc input left) (calc input right)

    let calculate input = calc input "root"

    do ``assert`` 152L (calculate sample)

    member _.answer = calculate data

type Problem21b() =
    let rec calc input name =
        match input |> Map.find name with
        | Number _ when name = "humn" -> None
        | Number value -> Some value
        | Op(left, right, op) ->
            match calc input left, calc input right with
            | Some left, Some right -> Some(operation op left right)
            | _ -> None

    let rec solve input name expected =
        match input |> Map.find name with
        | _ when name = "humn" -> expected
        | Op(left, right, op) ->
            match calc input left, calc input right, op with
            | Some value, None, Add -> right, expected - value
            | Some value, None, Subtract -> right, value - expected
            | Some value, None, Multiply -> right, expected / value
            | Some value, None, Divide -> right, value / expected
            | None, Some value, Add -> left, expected - value
            | None, Some value, Subtract -> left, expected + value
            | None, Some value, Multiply -> left, expected / value
            | None, Some value, Divide -> left, expected * value
            ||> solve input

    let calculate input =
        input
        |> Map.change "root" (fun (Some(Op(left, right, _))) -> Some(Op(left, right, Subtract)))
        |> fun input -> solve input "root" 0

    do ``assert`` 301L (calculate sample)

    member _.answer = calculate data
