namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input03 =
    let convert =
        Array.map (fun (line: string) ->
            let len = line.Length / 2
            Set.ofSeq line[.. len - 1], Set.ofSeq line[len..])

    let sample =
        """
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/03.txt") |> Seq.toArray |> convert

    let priority char =
        if Char.IsLower(char) then
            int char - int 'a' + 1
        else
            int char - int 'A' + 27

open Input03

type Problem03a() =
    let calculate =
        Array.sumBy (fun line -> line ||> Set.intersect |> Seq.sumBy priority)

    do assert (sample |> calculate = 157)

    member _.answer = data |> calculate

type Problem03b() =
    let calculate input =
        input
        |> Array.splitInto (input.Length / 3)
        |> Array.sumBy (fun lines ->
            lines
            |> Array.map (fun line -> line ||> Set.union)
            |> Set.intersectMany
            |> Seq.sumBy priority)

    do ``assert`` 70 (sample |> calculate)

    member _.answer = data |> calculate
