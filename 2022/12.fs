namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input12 =
    let convert (lines: string[]) =
        lines |> Array.map Array.ofSeq |> Array2D.ofArrayRows

    let sample =
        """
Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/12.txt") |> Seq.toArray |> convert

    let distance input =
        let distance = (input |> Array2D.length ||> Array2D.create) Int32.MaxValue
        let input = input |> Array2D.copy
        let sx, sy = input |> Array2D.findByColumn 'S'
        let ex, ey = input |> Array2D.findByColumn 'E'
        input[sx, sy] <- 'a'
        input[ex, ey] <- 'z'

        let rec traverse =
            function
            | [] -> ()
            | (x, y, dist) :: coords ->
                if distance[x, y] <= dist then
                    traverse coords
                else
                    distance[x, y] <- dist
                    let height = int input[x, y]

                    input
                    |> Array2D.permuteMoves (x, y)
                    |> List.choose (fun (x, y) ->
                        if int input[x, y] >= height - 1 then
                            Some(x, y, dist + 1)
                        else
                            None)
                    |> List.append coords
                    |> traverse

        traverse [ ex, ey, 0 ]
        distance, sx, sy

open Input12

type Problem12a() =
    let calculate input = input |> distance |||> Array2D.get

    do ``assert`` 31 (calculate sample)

    member _.answer = calculate data

type Problem12b() =
    let calculate input =
        let xLen, yLen = input |> Array2D.length
        let distance, _, _ = distance input

        seq {
            for x = 0 to xLen - 1 do
                for y = 0 to yLen - 1 do
                    if input[x, y] = 'a' then
                        yield distance[x, y]
        }
        |> Seq.min

    do ``assert`` 29 (calculate sample)

    member _.answer = calculate data
