namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input18 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let line = line.Split(',')
            Int32.Parse(line[0]), Int32.Parse(line[1]), Int32.Parse(line[2]))

    let sample =
        """
2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/18.txt") |> Seq.toArray |> convert

    let surfaceArea input =
        input
        |> Array.mapi (fun i (x1, y1, z1) ->
            let overlaps =
                input[i..]
                |> Array.sumBy (fun (x2, y2, z2) -> Convert.ToInt32(abs (x1 - x2) + abs (y1 - y2) + abs (z1 - z2) = 1))

            6 - overlaps * 2)
        |> Array.sum

open Input18

type Problem18a() =
    let calculate = surfaceArea

    do ``assert`` 64 (calculate sample)

    member _.answer = calculate data

type Problem18b() =
    let calculate input =
        let input = Array.map (fun (x, y, z) -> x + 1, y + 1, z + 1) input
        let xMax = Array.maxOf (fun (x, _, _) -> x) input + 1
        let yMax = Array.maxOf (fun (_, y, _) -> y) input + 1
        let zMax = Array.maxOf (fun (_, _, z) -> z) input + 1
        let grid = Array3D.zeroCreate (xMax + 1) (yMax + 1) (zMax + 1)
        input |> Array.iter (fun (x, y, z) -> grid[x, y, z] <- true)

        let rec negative coords =
            function
            | [] -> coords |> List.toArray
            | x, y, z as coord :: toVisit when not grid[x, y, z] ->
                grid[x, y, z] <- true

                let toVisitNext =
                    [ -1, 0, 0; 0, -1, 0; 0, 0, -1; 1, 0, 0; 0, 1, 0; 0, 0, 1 ]
                    |> List.choose (fun (dx, dy, dz) ->
                        let x = x + dx
                        let y = y + dy
                        let z = z + dz

                        if 0 <= x && x <= xMax && 0 <= y && y <= yMax && 0 <= z && z <= zMax then
                            Some(x, y, z)
                        else
                            None)

                negative (coord :: coords) (toVisitNext @ toVisit)
            | _ :: toVisit -> negative coords toVisit

        negative [] [ 0, 0, 0 ]
        |> surfaceArea
        |> fun area ->
            area
            - 2
              * ((xMax + 1) * (yMax + 1) + (yMax + 1) * (zMax + 1) + (xMax + 1) * (zMax + 1))

    do ``assert`` 58 (calculate sample)

    member _.answer = calculate data
