namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input07 =
    type Node =
        | File of string * uint32
        | Directory of string * Node list

    let convert (lines: string[]) =
        let rec convert' nodes stack =
            function
            | [] when stack |> List.isEmpty -> nodes |> List.exactlyOne
            | [] -> convert' nodes stack [ "$ cd .." ]
            | "$ ls" :: lines -> convert' nodes stack lines
            | "$ cd .." :: lines ->
                let (name, upNodes) :: stack = stack
                convert' (Directory(name, nodes) :: upNodes) stack lines
            | line :: lines when line.StartsWith("dir ") -> convert' nodes stack lines
            | line :: lines when line.StartsWith("$ cd ") -> convert' [] ((line[5..], nodes) :: stack) lines
            | line :: lines ->
                let [| size; name |] = line.Split()
                let size = UInt32.Parse(size)
                convert' (File(name, size) :: nodes) stack lines

        lines |> Array.toList |> convert' [] []

    let sample =
        """
$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/07.txt") |> Seq.toArray |> convert

open Input07

type Problem07a() =
    let calculate input =
        let rec size =
            function
            | File(_, size) -> size, 0u
            | Directory(_, nodes) ->
                let totalSize, thresholdSize =
                    nodes
                    |> List.map size
                    |> List.fold (fun (x1, x2) (y1, y2) -> x1 + y1, x2 + y2) (0u, 0u)

                totalSize, thresholdSize + (if totalSize <= 100_000u then totalSize else 0u)

        size input |> snd

    do ``assert`` 95437u (calculate sample)

    member _.answer = calculate data

type Problem07b() =
    let calculate input =
        let rec size =
            function
            | File(_, size) -> size, []
            | Directory(name, nodes) ->
                let totalSize, allSizes =
                    nodes
                    |> List.map size
                    |> List.fold (fun (x1, x2) (y1, y2) -> x1 + y1, x2 @ y2) (0u, [])

                totalSize, (name, totalSize) :: allSizes

        let totalSize, allSizes = size input
        let requiredSize = 30_000_000u - (70_000_000u - totalSize)

        allSizes
        |> List.filter (fun (_, size) -> size >= requiredSize)
        |> List.minBy snd
        |> snd

    do ``assert`` 24933642u (calculate sample)

    member _.answer = calculate data
