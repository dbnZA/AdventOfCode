namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input17 =
    let convert (lines: string[]) = lines[0] |> Seq.toArray

    let sprites =
        [| (1, 4), [ 0, 0; 1, 0; 2, 0; 3, 0 ]
           (3, 3), [ 0, 1; 1, 1; 2, 1; 1, 0; 1, 2 ]
           (3, 3), [ 0, 0; 1, 0; 2, 0; 2, 1; 2, 2 ]
           (4, 1), [ 0, 0; 0, 1; 0, 2; 0, 3 ]
           (2, 2), [ 0, 0; 1, 0; 0, 1; 1, 1 ] |]

    let sample =
        """
>>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/17.txt") |> Seq.toArray |> convert

    let move (input, play) (((height, width), coords), (x, y)) (grid: _ ResizeArray) =
        let yLen = grid.Count
        let xLen = grid[0] |> Array.length
        let inputLength = input |> Array.length

        let tryMove (x, y) =
            0 <= x
            && x + width <= xLen
            && 0 <= y
            && y + height <= yLen
            && coords |> List.forall (fun (dx, dy) -> not (grid[y + dy][x + dx]))

        let rec move play (x, y) =
            let x, y =
                match input[play] with
                | '<' when tryMove (x - 1, y) -> x - 1, y
                | '>' when tryMove (x + 1, y) -> x + 1, y
                | _ -> x, y

            let play = (play + 1) % inputLength

            if tryMove (x, y - 1) then
                move play (x, y - 1)
            else
                coords |> List.iter (fun (dx, dy) -> grid[y + dy][x + dx] <- true)
                play, y + height, grid

        move play (x, y)

    let moves num input =
        let spriteLength = sprites |> Array.length

        let tryFindCycle (states: _ ResizeArray) (heights: int ResizeArray) =
            let keyIndex = states.Count - 1

            let rec compareRanges x1 x2 len =
                if len = 0 then true
                elif states[x1] <> states[x2] then false
                else compareRanges (x1 + 1) (x2 + 1) (len - 1)

            let rec tryFindCycle cycleLength =
                let cycleStart = keyIndex - 2 * cycleLength + 1
                let nextCycle = cycleStart + cycleLength

                if cycleStart < 0 then
                    None
                elif compareRanges cycleStart nextCycle cycleLength then
                    let num = num - int64 cycleStart
                    let cycles, shortCycle = Math.DivRem(num, int64 cycleLength)

                    int64 heights[cycleStart + int shortCycle]
                    + int64 (heights[nextCycle] - heights[cycleStart]) * cycles
                    |> Some
                else
                    tryFindCycle (cycleLength + 1)

            tryFindCycle 1

        let rec moves (grid: _ ResizeArray) (states: _ ResizeArray) (heights: _ ResizeArray) count =
            match tryFindCycle states heights with
            | Some height -> height
            | None when count = num -> int64 heights[heights.Count - 1]
            | None ->
                let play, spriteIndex = states[states.Count - 1]
                let height = heights[heights.Count - 1]
                let (spriteHeight, _), _ as sprite = sprites[spriteIndex]
                grid.AddRange([ for _ in grid.Count .. height + spriteHeight + 3 -> Array.zeroCreate 7 ])
                let play, newHeight, grid = move (input, play) (sprite, (2, height + 3)) grid
                let height = max height newHeight
                states.Add((play, (spriteIndex + 1) % spriteLength))
                heights.Add(height)
                moves grid states heights (count + 1L)

        moves (ResizeArray()) (ResizeArray([ 0, 0 ])) (ResizeArray([ 0 ])) 0L

open Input17

type Problem17a() =
    let calculate = moves 2_022L

    do ``assert`` 3_068L (calculate sample)

    member _.answer = calculate data

type Problem17b() =
    let calculate = moves 1_000_000_000_000L

    do ``assert`` 1_514_285_714_288L (calculate sample)

    member _.answer = calculate data
