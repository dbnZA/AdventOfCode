namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input19 =
    type Rock =
        | Ore = 0
        | Clay = 1
        | Obsidian = 2
        | Geode = 3

    type State = { Collecting: int[]; Stock: int[] }

    let convert (lines: string[]) =
        lines
        |> List.ofArray
        |> List.map (fun line ->
            let [| title; recipes |] = line.Split(':')
            let [| _; id |] = title.Split()

            let robots =
                recipes.Split('.', StringSplitOptions.RemoveEmptyEntries)
                |> List.ofArray
                |> List.map (fun recipe ->
                    let toRock (rock: string) =
                        int (Enum.Parse<Rock>(rock, ignoreCase = true))

                    let recipe = recipe.Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    let robot = recipe[1]
                    let input = Array.zeroCreate 4
                    input[toRock recipe[5]] <- - Int32.Parse(recipe[4])

                    if recipe.Length > 6 then
                        input[toRock recipe[8]] <- - Int32.Parse(recipe[7])

                    toRock robot, input)

            Int32.Parse(id), robots)

    let sample =
        """
Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/19.txt") |> Seq.toArray |> convert

    let reduce states =

        let rec canAdd current updatedStates =
            function
            | [] -> true, updatedStates
            | other :: states ->
                let rec compare state index =
                    if index = 4 then
                        Some state
                    else
                        let comparison = current.Stock[index].CompareTo(other.Stock[index])

                        if state = 0 then compare comparison (index + 1)
                        elif comparison * state = -1 then None
                        else compare state (index + 1)

                match compare 0 0 with
                | Some -1 -> false, other :: updatedStates @ states
                | None -> canAdd current (other :: updatedStates) states
                | _ -> canAdd current updatedStates states

        let rec reduce reduced =
            function
            | [] -> reduced
            | state :: states ->
                match canAdd state [] states with
                | true, states -> reduce (state :: reduced) states
                | false, states -> reduce reduced states

        reduce [] states

    let tick robots state =
        let add array1 array2 =
            Array.zip array1 array2 |> Array.map ((<||) (+))

        let stock = add state.Stock state.Collecting

        let states =
            robots
            |> List.filter (fun (_, inputs) ->
                Array.zip state.Stock inputs
                |> Array.forall (fun (stock, required) -> stock >= -required))
            |> List.map (fun (robot, inputs) ->
                { Stock = add stock inputs
                  Collecting = state.Collecting |> Array.mapi (fun i qty -> if i = robot then qty + 1 else qty) })

        { state with Stock = stock } :: states

    let ticks robots num states =
        let getGeode values = Array.get values (int Rock.Geode)

        let totalGeode num state =
            getGeode state.Stock + num * getGeode state.Collecting

        let maxUsage =
            robots
            |> List.map snd
            |> List.reduce (fun a1 a2 -> Array.zip a1 a2 |> Array.map ((<||) min))
            |> Array.map (~-)

        maxUsage[int Rock.Geode] <- num * (num - 1) / 2

        let rec ticks num states =
            let maxGeodes = states |> List.maxOf (totalGeode num)

            if num = 0 then
                maxGeodes
            else
                let maxConsumption = maxUsage |> Array.map ((*) (num - 1))
                maxConsumption[int Rock.Geode] <- Int32.MaxValue
                let extraQty = num * (num - 1) / 2

                let states =
                    states
                    |> List.filter (fun state -> totalGeode num state + extraQty >= maxGeodes)
                    |> List.collect (tick robots)
                    |> List.filter (fun state -> Array.zip state.Collecting maxUsage |> Array.forall ((<||) (<=)))
                    |> List.groupBy _.Collecting

                    |> List.collect (fun (_, states) ->
                        states
                        |> List.map (fun state ->
                            { state with
                                Stock = Array.zip state.Stock maxConsumption |> Array.map ((<||) min) })
                        |> reduce)

                ticks (num - 1) states

        ticks num states

open Input19

type Problem19a() =
    let calculate input =
        input
        |> List.sumBy (fun (id, robots) ->
            ticks
                robots
                24
                [ { Collecting = [| 1; 0; 0; 0 |]
                    Stock = [| 0; 0; 0; 0 |] } ]
            |> (*) id)

    do ``assert`` 33 (calculate sample)

    member _.answer = calculate data

type Problem19b() =
    let calculate input =
        input
        |> List.truncate 3
        |> List.map (fun (_, robots) ->
            ticks
                robots
                32
                [ { Collecting = [| 1; 0; 0; 0 |]
                    Stock = [| 0; 0; 0; 0 |] } ])
        |> List.reduce (*)

    do ``assert`` 3472 (calculate sample)

    member _.answer = calculate data
