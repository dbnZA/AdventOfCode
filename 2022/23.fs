namespace AdventOfCode.Year2022

open Checked
open System.IO
open AdventOfCode

module Input23 =
    let directions = [| -1, 0; 1, 0; 0, -1; 0, 1 |]

    let permute = [| -1, -1; -1, 0; -1, 1; 0, 1; 1, 1; 1, 0; 1, -1; 0, -1 |]

    let convert (lines: string[]) =
        seq {
            for x = 0 to lines.Length - 1 do
                let line = lines[x]

                for y = 0 to line.Length - 1 do
                    if line[y] = '#' then
                        yield x, y
        }
        |> Seq.toArray

    let sample =
        """
..............
..............
.......#......
.....###.#....
...#...#.#....
....#...##....
...#.###......
...##.#.##....
....#..#......
..............
..............
..............
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/23.txt") |> Seq.toArray |> convert

    let toGrid input =
        let xMin = input |> Array.minOf fst
        let xMax = input |> Array.maxOf fst
        let yMin = input |> Array.minOf snd
        let yMax = input |> Array.maxOf snd

        let grid =
            Array2D.zeroCreateBased (xMin - 1) (yMin - 1) (xMax - xMin + 3) (yMax - yMin + 3)

        input |> Array.iter (fun (x, y) -> grid[x, y] <- -1)
        grid

    let move input orientation =
        let positions = input |> toGrid

        let inline empty (x, y) =
            Array.forall (fun (dx, dy) -> positions[x + dx, y + dy] <> -1)

        let inline isAlone (x, y) = permute |> empty (x, y)

        let scan (x, y) =
            let proposed =
                if isAlone (x, y) then
                    None
                else
                    [| orientation .. orientation + 3 |]
                    |> Array.tryPick (fun orientation ->
                        let dx, dy = directions[orientation % 4]

                        let side =
                            if dx = 0 then
                                [| -1, dy; 0, dy; 1, dy |]
                            else
                                [| dx, -1; dx, 0; dx, 1 |]

                        if side |> empty (x, y) then
                            let x = x + dx
                            let y = y + dy
                            positions[x, y] <- positions[x, y] + 1
                            Some(x, y)
                        else
                            None)

            proposed, (x, y)

        input
        |> Array.map scan
        |> Array.map (function
            | Some(x, y), current -> if positions[x, y] > 1 then current else x, y
            | None, current -> current)

open Input23

type Problem23a() =
    let calculate input =
        let positions = (input, [ 0..9 ]) ||> List.fold move

        let scan op f =
            positions |> Array.map f |> Array.reduce op

        (scan max fst - scan min fst + 1) * (scan max snd - scan min snd + 1)
        - input.Length

    do ``assert`` 110 (calculate sample)

    member _.answer = calculate data

type Problem23b() =
    let calculate input =
        let rec round n input =
            let positions = move input n
            if positions = input then n + 1 else round (n + 1) positions

        round 0 input

    do ``assert`` 20 (calculate sample)

    member _.answer = calculate data
