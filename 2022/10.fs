namespace AdventOfCode.Year2022

open Checked
open System
open System.IO
open AdventOfCode

module Input10 =
    type Op =
        | AddX of int
        | Noop

    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            match line.Split() with
            | [| "addx"; value |] -> AddX(Int32.Parse(value))
            | [| "noop" |] -> Noop)
        |> Array.toList

    let sample =
        """
addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2022/10.txt") |> Seq.toArray |> convert

    let rec eval states ops =
        let state = states |> List.head

        match ops with
        | [] -> states |> List.rev |> List.toArray
        | AddX x :: ops -> eval ((state + x) :: state :: states) ops
        | Noop :: ops -> eval (state :: states) ops

open Input10

type Problem10a() =
    let calculate input =
        let states = input |> eval [ 1 ] |> Array.mapi (fun i state -> (i + 1) * state)

        [ 20..40..220 ] |> List.sumBy (fun i -> states[i - 1])

    do ``assert`` 13140 (calculate sample)

    member _.answer = calculate data

type Problem10b() =
    let calculate input =
        input
        |> eval [ 1 ]
        |> Array.mapi (fun i state ->
            let i = i % 40
            let state = state
            if i - 1 <= state && state <= i + 1 then '#' else ' ')
        |> String

    let expected =
        """
##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....
        """
            .Split()
        |> String.concat ""
        |> fun line -> line.Replace('.', ' ') + " "

    do ``assert`` expected (calculate sample)

    member _.answer = calculate data
