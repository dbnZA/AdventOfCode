namespace AdventOfCode

open System
open System.Diagnostics
open System.IO
open System.Net
open System.Net.Http
open System.Reflection
open System.Text.RegularExpressions
open System.Threading.Tasks

module Program =
    do DotEnv.init

    let private basePath = Uri("https://adventofcode.com")

    let private client =
        let cookies = CookieContainer()
        let token = Environment.GetEnvironmentVariable("AOC_SESSION")

        if String.IsNullOrEmpty(token) then
            failwith "Missing environment variable: AOC_SESSION"

        cookies.Add(basePath, Cookie("session", Environment.GetEnvironmentVariable("AOC_SESSION")))
        new HttpClient(new HttpClientHandler(CookieContainer = cookies), BaseAddress = basePath)

    let private writeInput year problem =
        let directory = year.ToString()
        let inputPath = Path.Combine(directory, $"%02i{problem}.txt")

        if File.Exists(inputPath) then
            failwithf $"file already exists: {inputPath}"

        task {
            let! response = client.GetAsync($"/{year}/day/{problem}/input")
            let response = response.EnsureSuccessStatusCode()
            use inputFile = File.Create(inputPath)
            do! response.Content.CopyToAsync(inputFile)
        }

    let private writeTemplate year problem =
        let directory = year.ToString()
        Directory.CreateDirectory(directory) |> ignore
        let solutionPath = Path.Combine(directory, $"%02i{problem}.fs")

        if File.Exists(solutionPath) then
            failwithf $"file already exists: {solutionPath}"

        let template =
            seq {
                yield $"namespace AdventOfCode.Year{year}"
                yield ""
                yield "open Checked"
                yield "open System"
                yield "open System.IO"
                yield "open AdventOfCode"
                yield ""
                yield $"module Input%02i{problem} ="
                yield "    let convert (lines: string []) = ()"
                yield ""
                yield "    let sample ="
                yield "        \"\"\""
                yield ""
                yield "        \"\"\""
                yield "            .Split(\"\\n\")"
                yield "        |> fun lines -> lines[1..^1]"
                yield "        |> convert"
                yield ""
                yield $"    let data = File.ReadLines(\"{year}/%02i{problem}.txt\") |> Seq.toArray |> convert"
                yield ""
                yield $"open Input%02i{problem}"
                yield ""

                let problem ab =
                    seq {
                        yield $"type Problem%02i{problem}{ab}() ="
                        yield "    let calculate input = 0"
                        yield ""
                        yield "    do ``assert`` 0 (calculate sample)"
                        yield ""
                        yield "    member _.answer = calculate data"
                        yield ""
                    }

                yield! problem "a"
                yield! problem "b"
            }
            |> String.concat "\n"

        File.WriteAllTextAsync(solutionPath, template)

    let createSolution year problem =
        task {
            do! writeTemplate year problem
            do! writeInput year problem
            File.SetLastWriteTimeUtc("AdventOfCode.fsproj", DateTime.UtcNow)
        }

    let private (|Int|_|) (value: string) =
        match Int32.TryParse(value) with
        | true, value -> Some value
        | false, _ -> None

    let private (|Problem|_|) value =
        if Regex.Match(value, @"\d\d[ab]").Success then
            Some value
        elif Regex.Match(value, @"\d[ab]").Success then
            Some $"0{value}"
        else
            None

    let private runAsync (task: Task) = task.GetAwaiter().GetResult()

    [<EntryPoint>]
    let main argv =
        let now = DateTime.UtcNow
        let year = now.Year

        let day =
            lazy
                (if now.Month <> 12 || now.Day > 25 then
                     failwith "Not a valid day, specify day explicitly"

                 now.Day)

        try
            match argv with
            | [| "create"; Int problem |] -> createSolution year problem |> runAsync
            | [| "create" |] -> createSolution year day.Value |> runAsync
            | _ ->
                let problems =
                    Assembly.GetExecutingAssembly().GetExportedTypes()
                    |> Array.where _.Name.StartsWith("Problem")
                    |> Array.sortBy (fun t -> (t.Namespace, t.Name))

                let selectProblem year (problem: string) =
                    let year = year.ToString()

                    problems
                    |> Array.where (fun m -> m.Name.EndsWith(problem) && m.Namespace.EndsWith(year))

                let selectedProblems =
                    match argv with
                    | [| "all" |] -> problems
                    | [| Problem problem |] -> selectProblem year problem
                    | [| Int year; Problem problem |] -> selectProblem year problem
                    | [||] ->
                        let year = year.ToString()
                        let a = $"%02i{day.Value}a"
                        let b = $"%02i{day.Value}b"

                        problems
                        |> Array.where (fun m ->
                            m.Namespace.EndsWith(year) && (m.Name.EndsWith(a) || m.Name.EndsWith(b)))
                    | _ ->
                        printfn "usage:"
                        printfn "   dotnet run"
                        printfn "   dotnet run [problem]"
                        printfn "   dotnet run [year] [problem]"
                        printfn "   dotnet run all"
                        printfn "   dotnet run create"
                        printfn "   dotnet run create [day]"
                        failwith $"invalid arguments: %A{argv}"

                for problem in selectedProblems do
                    let timer = Stopwatch.StartNew()

                    printfn "%s: %O (%ims)"
                    <| problem.Name
                    <| problem.GetProperty("answer").GetValue(Activator.CreateInstance(problem))
                    <| timer.ElapsedMilliseconds

            0
        with e ->
            printfn $"%A{e}\nTotal runtime: ({int (DateTime.UtcNow - now).TotalMilliseconds}ms)"
            1
