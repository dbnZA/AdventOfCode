namespace AdventOfCode

open System
open System.IO

// https://dusted.codes/dotenv-in-dotnet
module DotEnv =
    let private load =
        lazy
            (let filePath = Path.Combine(Directory.GetCurrentDirectory(), ".env")

             if File.Exists(filePath) then
                 File.ReadAllLines(filePath)
                 |> Array.iter (fun line ->
                     match line.Split('=', 2, StringSplitOptions.RemoveEmptyEntries) with
                     | [| key; value |] -> Environment.SetEnvironmentVariable(key, value)
                     | _ -> ())
             else
                 Console.WriteLine "No .env file found.")

    let init = load.Value
