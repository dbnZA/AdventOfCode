namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input05 =
    type RangeMap =
        { SourceStart: int64
          DestinationStart: int64
          Range: int64 }

    let convert (lines: string[]) =
        let seed = lines[0].Split(": ", 2).[1].Split() |> Array.map Int64.Parse

        let rec readLines maps source dest =
            function
            | [] ->
                maps
                |> List.groupBy fst
                |> List.map (fun ((source, dest), maps) -> source, (dest, maps |> List.map snd))
                |> Map.ofList
            | "" :: lines -> readLines maps source dest lines
            | line :: lines when line.EndsWith(" map:") ->
                let [| source; dest |] = line[..^5].Split("-to-")
                readLines maps source dest lines
            | line :: lines ->
                let [| destStart; sourceStart; range |] = line.Split() |> Array.map Int64.Parse

                let map =
                    { SourceStart = sourceStart
                      DestinationStart = destStart
                      Range = range }

                readLines (((source, dest), map) :: maps) source dest lines

        seed, readLines [] "seed" "soil" (lines[3..] |> Array.toList)

    let sample =
        """
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/05.txt") |> Seq.toArray |> convert

open Input05

type Problem05a() =
    let rec findLocation dest id almanac =
        match almanac |> Map.tryFind dest with
        | Some(dest, maps) ->
            let id =
                maps
                |> List.tryPick (fun map ->
                    let offset = id - map.SourceStart

                    if offset >= 0 && offset < map.Range then
                        Some(map.DestinationStart + offset)
                    else
                        None)
                |> Option.defaultValue id

            findLocation dest id almanac
        | None -> id

    let calculate (seeds, almanac) =
        seeds |> Array.minOf (fun id -> findLocation "seed" id almanac)

    do ``assert`` 35L (calculate sample)

    member _.answer = calculate data

type Problem05b() =
    let rec remapId map mappedRanges unmappedRanges =
        function
        | [] -> mappedRanges, unmappedRanges
        | (id, range) :: idRanges ->
            let start = max id map.SourceStart
            let ``end`` = min (id + range) (map.SourceStart + map.Range)

            if start < ``end`` then
                let mappedRange = (start - map.SourceStart) + map.DestinationStart, ``end`` - start

                let unmappedRange =
                    [ id, start - id; ``end``, id + range - ``end`` ]
                    |> List.filter (fun (_, range) -> range > 0)

                remapId map (mappedRange :: mappedRanges) (unmappedRange @ unmappedRanges) idRanges
            else
                remapId map mappedRanges ((id, range) :: unmappedRanges) idRanges

    let rec remapRange mappedRanges unmappedRanges =
        function
        | [] -> unmappedRanges @ mappedRanges
        | map :: maps ->
            let mappedRanges', unmappedRanges = remapId map [] [] unmappedRanges
            remapRange (mappedRanges' @ mappedRanges) unmappedRanges maps

    let rec findLocation dest idRanges almanac =
        match almanac |> Map.tryFind dest with
        | Some(dest, maps) -> findLocation dest (remapRange [] idRanges maps) almanac
        | None -> idRanges

    let calculate (seeds: int64[], almanac) =
        [ for i in 0..2 .. seeds.Length - 2 -> [ seeds[i], seeds[i + 1] ] ]
        |> List.collect (fun idRanges -> findLocation "seed" idRanges almanac)
        |> List.minOf fst

    do ``assert`` 46L (calculate sample)

    member _.answer = calculate data
