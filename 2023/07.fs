namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input07 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let [| hand; rank |] = line.Split()
            hand |> Array.ofSeq, Int32.Parse rank)

    let sample =
        """
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/07.txt") |> Seq.toArray |> convert

    let handValue kinds rank (hand, _) =
        let value =
            hand |> Array.fold (fun value x -> value * 13 + Array.findIndex ((=) x) rank) 0

        let kind =
            match kinds hand with
            | [| 5 |] -> 7
            | [| 4; 1 |] -> 6
            | [| 3; 2 |] -> 5
            | [| 3; 1; 1 |] -> 4
            | [| 2; 2; 1 |] -> 3
            | [| 2; 1; 1; 1 |] -> 2
            | [| 1; 1; 1; 1; 1 |] -> 1
            | x -> failwith $"{x}"

        value + kind * 371293 // 15^5

    let calculate handValue input =
        input
        |> Array.sortBy handValue
        |> Array.mapi (fun i (_, bid) -> (i + 1) * bid)
        |> Array.reduce (+)

open Input07

type Problem07a() =
    let rank = [| '2'; '3'; '4'; '5'; '6'; '7'; '8'; '9'; 'T'; 'J'; 'Q'; 'K'; 'A' |]

    let kinds hand =
        hand |> Array.groupBy id |> Array.map (snd >> _.Length) |> Array.sortDescending

    let calculate = calculate (handValue kinds rank)

    do ``assert`` 6440 (calculate sample)

    member _.answer = calculate data

type Problem07b() =
    let rank = [| 'J'; '2'; '3'; '4'; '5'; '6'; '7'; '8'; '9'; 'T'; 'Q'; 'K'; 'A' |]

    let kinds hand =
        match hand |> Array.countIf ((=) 'J') with
        | 5 -> [| 5 |]
        | js ->
            let kind =
                hand
                |> Array.filter ((<>) 'J')
                |> Array.groupBy id
                |> Array.map (snd >> _.Length)
                |> Array.sortDescending

            kind[0] <- kind[0] + js
            kind

    let calculate = calculate (handValue kinds rank)

    do ``assert`` 5905 (calculate sample)

    member _.answer = calculate data
