namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input01 =
    let data = File.ReadLines("2023/01.txt") |> Seq.toArray

open Input01

type Problem01a() =
    let sample =
        """
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]

    let calculate input =
        input
        |> Array.sumBy (fun line ->
            let fst = line |> Seq.find Char.IsDigit
            let lst = line |> Seq.findBack Char.IsDigit
            Int32.Parse(String [| fst; lst |]))

    do ``assert`` 142 (calculate sample)

    member _.answer = calculate data

type Problem01b() =
    let sample =
        """
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
"""
            .Split("\n")
        |> fun lines -> lines[1..^1]

    let words =
        [ "one"; "two"; "three"; "four"; "five"; "six"; "seven"; "eight"; "nine" ]
        |> List.indexed

    let rec tokenize numbers value =
        if value = "" then
            (numbers |> List.last) * 10 + (numbers |> List.head)
        elif Char.IsDigit value[0] then
            tokenize ((int value[0] - int '0') :: numbers) value[1..]
        else
            match words |> List.tryFind (fun (_, str) -> value.StartsWith(str)) with
            | Some(number, _) -> tokenize (number + 1 :: numbers) value[1..]
            | None -> tokenize numbers value[1..]

    let calculate input = input |> Array.sumBy (tokenize [])

    do ``assert`` 281 (calculate sample)

    member _.answer = calculate data
