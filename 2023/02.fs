namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input02 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let [| game; plays |] = line.Split(": ", 2)

            let plays =
                plays.Split("; ")
                |> Array.map (fun play ->
                    play.Split(", ")
                    |> Array.map (fun cubes ->
                        let [| number; colour |] = cubes.Split()
                        Int32.Parse number, colour))

            Int32.Parse(game.Split()[1]), plays)

    let sample =
        """
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/02.txt") |> Seq.toArray |> convert

open Input02

type Problem02a() =
    let possible = Map.ofList [ "red", 12; "green", 13; "blue", 14 ]

    let calculate input =
        input
        |> Array.sumBy (fun (gameId, plays) ->
            if
                plays
                |> Array.forall (fun cubes ->
                    cubes |> Array.forall (fun (number, colour) -> number <= possible[colour]))
            then
                gameId
            else
                0)

    do ``assert`` 8 (calculate sample)

    member _.answer = calculate data

type Problem02b() =
    let calculate input =
        input
        |> Array.sumBy (fun (_, plays) ->
            let maxColours =
                plays
                |> Array.fold
                    (fun maxColours cubes ->
                        cubes
                        |> Array.fold
                            (fun maxColours (number, colour) ->
                                maxColours
                                |> Map.change colour (function
                                    | Some oldNumber -> Some(max oldNumber number)
                                    | None -> Some number))
                            maxColours)
                    Map.empty

            maxColours |> Map.values |> Seq.reduce (*))

    do ``assert`` 2286 (calculate sample)

    member _.answer = calculate data
