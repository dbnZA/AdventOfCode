namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input09 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line -> line.Split() |> Array.map Int32.Parse |> Array.toList)

    let sample =
        """
0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/09.txt") |> Seq.toArray |> convert

    let rec diff seq =
        function
        | x1 :: (x2 :: _ as n) -> diff (x1 - x2 :: seq) n
        | _ -> List.rev seq

    let rec next n seq =
        let n = List.head seq :: n
        let seq = diff [] seq

        printfn $"{n} {seq}"

        if seq |> List.forall ((=) 0) then
            List.sum n
        else
            next n seq

open Input09

type Problem09a() =
    let calculate input =
        input |> Array.sumBy (List.rev >> (next []))

    do ``assert`` 114 (calculate sample)

    member _.answer = calculate data

type Problem09b() =
    let calculate input = input |> Array.sumBy (next [])

    do ``assert`` 2 (calculate sample)

    member _.answer = calculate data
