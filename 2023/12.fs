namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input12 =
    let convert (lines: string[]) =
        lines
        |> Array.map (fun line ->
            let [| pattern; runs |] = line.Split()
            pattern, runs.Split(",") |> Array.map Int32.Parse |> Array.toList)

    let sample =
        """
???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/12.txt") |> Seq.toArray |> convert

open Input12

module Common12 =
    [<return: Struct>]
    let inline (|Match|_|) expected n (pattern: string) =
        let rec isMatch n i =
            if n = 0 then
                ValueSome pattern[i..]
            elif pattern[i] = expected || pattern[i] = '?' then
                isMatch (n - 1) (i + 1)
            else
                ValueNone

        isMatch n 0

    let permute f (pad, n, pattern, runs) =
        match runs with
        | [] ->
            match pattern with
            | Match '.' n _ -> 1L
            | _ -> 0L
        | run :: runs ->
            let mutable sum = 0L

            for j in (if pad then 1 else 0) .. n do
                match pattern with
                | Match '.' j (Match '#' run pattern) -> sum <- sum + f (true, n - j, pattern, runs)
                | _ -> ()

            sum

open Common12

type Problem12a() =
    let calculate input =
        input
        |> Array.sumBy (fun (pattern: string, runs) ->
            (memorize permute) (false, pattern.Length - List.sum runs, pattern, runs))

    do ``assert`` 21L (calculate sample)

    member _.answer = calculate data

type Problem12b() =
    let calculate input =
        input
        |> Array.sumBy (fun (pattern, runs) ->
            let pattern = Array.replicate 5 pattern |> String.concat "?"
            let runs = List.replicate 5 runs |> List.concat
            (memorize permute) (false, pattern.Length - List.sum runs, pattern, runs))

    do ``assert`` 525152L (calculate sample)

    member _.answer = calculate data
