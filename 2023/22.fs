namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input22 =
    let convert (lines: string[]) =
        let (|Coord|) (value: string) =
            value.Split(',') |> Array.map Int32.Parse |> (fun [| x; y; z |] -> x, y, z)

        let (|Split|) (separator: char) (value: string) = value.Split(separator)

        lines
        |> Array.mapi (fun idx line ->
            match line with
            | Split '~' [| Coord c1; Coord c2 |] -> idx + 1, (c1, c2))
        |> Array.toList

    let sample =
        """
1,0,1~1,2,1
0,0,2~2,0,2
0,2,3~2,2,3
0,0,4~0,2,4
2,0,5~2,2,5
0,1,6~2,1,6
1,1,8~1,1,9
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/22.txt") |> Seq.toArray |> convert

    type Offset =
        | Up = 1
        | Flat = 0
        | Down = -1

    let internalCoords ((x1, y1, z1), (x2, y2, z2)) =
        let inline sort (a1, a2 as a) = if a1 > a2 then (a2, a2) else a
        let x1, x2 = sort (x1, x2)
        let y1, y2 = sort (y1, y2)
        let z1, z2 = sort (z1, z2)

        [ for x in x1..x2 do
              for y in y1..y2 do
                  for z in z1..z2 -> x, y, z ]

    let move (z': Offset) (c1, c2) =
        let (~+) (x, y, z) = x, y, z + int z'
        +c1, +c2

    let sort = List.sortBy (fun (_, ((_, _, z1), (_, _, z2))) -> min z1 z2)

    let blocks (volume: _ array3d) (z': Offset) (idx, block) =
        block
        |> move z'
        |> internalCoords
        |> List.distinctOf (fun (x, y, z) -> volume[x, y, z])
        |> List.filter (fun i -> i <> 0 && i <> idx)

    let setBlock (volume: _ array3d) (z': Offset) coord value =
        internalCoords coord
        |> List.iter (fun (x, y, z) -> volume[x, y, z + int z'] <- value)

    let graph volume settled =
        settled
        |> List.map (fun (idx, _ as block) -> idx, (blocks volume Offset.Down block, blocks volume Offset.Up block))
        |> Map.ofList

    let rec fall (volume: _[,,]) nothingMoved settled stillFalling =
        function
        | [] ->
            match stillFalling with
            | [] when nothingMoved -> graph volume settled
            | [] -> fall volume true [] [] (sort settled)
            | stillFalling -> fall volume nothingMoved settled [] stillFalling
        | idx, coords as block :: falling ->
            if blocks volume Offset.Down block |> List.isEmpty then
                setBlock volume Offset.Flat coords 0
                setBlock volume Offset.Down coords idx
                fall volume false settled ((idx, move Offset.Down coords) :: stillFalling) falling
            else
                fall volume nothingMoved (block :: settled) stillFalling falling

    let volume input =
        let xn = input |> List.maxOf (fun (_, ((x1, _, _), (x2, _, _))) -> max x1 x2)
        let yn = input |> List.maxOf (fun (_, ((_, y1, _), (_, y2, _))) -> max y1 y2)
        let zn = input |> List.maxOf (fun (_, ((_, _, z1), (_, _, z2))) -> max z1 z2)

        let volume =
            Array3D.init (xn + 1) (yn + 1) (zn + 1) (fun _ _ z -> if z = 0 then -1 else 0)

        input |> List.iter (fun (idx, coords) -> setBlock volume Offset.Flat coords idx)
        volume

open Input22

type Problem22a() =

    let calculate input =
        let graph = fall (volume input) false [] [] (sort input)

        let redundantSupport idx =
            match graph |> Map.find idx with
            | [ _ ], _ -> false
            | _, _ -> true

        input
        |> List.sumBy (fun (idx, _) ->
            match graph |> Map.find idx with
            | _, above when above |> List.forall redundantSupport -> 1
            | _, _ -> 0)

    do ``assert`` 5 (calculate sample)

    member _.answer = calculate data

type Problem22b() =
    let rec collapse graph collapsed =
        function
        | [] -> Set.count collapsed - 1
        | idx :: collapsing ->
            let collapsed = collapsed |> Set.add idx

            graph
            |> Map.find idx
            |> snd
            |> List.filter (fun idx ->
                graph
                |> Map.find idx
                |> fst
                |> List.forall (fun idx -> collapsed |> Set.contains idx))
            |> List.append collapsing
            |> collapse graph collapsed

    let calculate input =
        let graph = fall (volume input) false [] [] (sort input)
        input |> List.sumBy (fun (idx, _) -> collapse graph Set.empty [ idx ])

    do ``assert`` 7 (calculate sample)

    member _.answer = calculate data
