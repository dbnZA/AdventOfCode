namespace AdventOfCode.Year2023

open Checked
open System.IO
open AdventOfCode

module Input11 =
    let convert (lines: string[]) =
        lines |> Array.map Seq.toArray |> Array2D.ofArrayRows

    let sample =
        """
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/11.txt") |> Seq.toArray |> convert

    let rec findEmpty empty =
        function
        | (i, r) :: input when r |> Array.forall ((=) '.') -> findEmpty (empty |> Set.add i) input
        | _ :: input -> findEmpty empty input
        | [] -> empty

    let calculate expand input =
        let findEmpty toVectors =
            input |> toVectors |> Array.indexed |> Array.toList |> findEmpty Set.empty

        let emptyRows = findEmpty Array2D.toRows
        let emptyColumns = findEmpty Array2D.toColumns

        let m, n = Array2D.length input
        let mutable lengths = 0L

        for x1 in 0 .. m - 1 do
            for y1 in 0 .. n - 1 do
                if input[x1, y1] = '#' then
                    let mutable x' = 0L

                    for x2 in x1 .. m - 1 do
                        if emptyRows |> Set.contains x2 then
                            x' <- x' + expand

                        let mutable y' = 0L

                        for y2 in y1 - 1 .. -1 .. 0 do
                            if emptyColumns |> Set.contains y2 then
                                y' <- y' + expand

                            if input[x2, y2] = '#' && (x2 > x1 || y2 > y1) then
                                lengths <- lengths + int64 (abs (y2 - y1) + abs (x2 - x1)) + y' + x'

                        let mutable y' = 0L

                        for y2 in y1 .. n - 1 do
                            if emptyColumns |> Set.contains y2 then
                                y' <- y' + expand

                            if input[x2, y2] = '#' && (x2 > x1 || y2 > y1) then
                                lengths <- lengths + int64 (abs (y2 - y1) + abs (x2 - x1)) + y' + x'

        lengths

open Input11

type Problem11a() =
    do ``assert`` 374L (calculate 1L sample)

    member _.answer = calculate 1L data

type Problem11b() =
    do ``assert`` 1030L (calculate 9L sample)
    do ``assert`` 8410L (calculate 99L sample)

    member _.answer = calculate 999_999L data
