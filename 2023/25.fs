namespace AdventOfCode.Year2023

open System.Collections.Generic
open Checked
open System
open System.IO
open AdventOfCode

module Input25 =
    let (|SplitT|) (sep: string) (value: string) = value.Split(sep) |> Array.map _.Trim()

    let convert (lines: string[]) =
        lines
        |> Array.map (function
            | SplitT ":" [| x; SplitT " " y |] -> x, y)

    let sample =
        """
jqt: rhn xhk nvd
rsh: frs pzl lsr
xhk: hfx
cmg: qnr nvd lhk bvb
rhn: xhk bvb hfx
bvb: xhk hfx
pzl: lsr hfx nvd
qnr: nvd
ntq: jqt hfx bvb xhk
nvd: lhk
lsr: lhk
rzs: qnr cmg lsr rsh
frs: qnr lhk lsr
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/25.txt") |> Seq.toArray |> convert

open Input25

type Problem25() =
    let rec traverse graph gens gen =
        function
        | [||] -> gens
        | nodes ->
            nodes
            |> Array.filter (fun nodeId ->
                if Array.get gens nodeId > gen then
                    Array.set gens nodeId gen
                    true
                else
                    false)
            |> Array.collect (Array.get graph)
            |> traverse graph gens (gen + 1)

    let calculate input =
        let nodeIds = Dictionary()

        let getId node =
            match nodeIds.TryGetValue node with
            | true, nodeId -> nodeId
            | false, _ ->
                let nodeId = nodeIds.Count
                nodeIds.Add(node, nodeId)
                nodeId

        let graph =
            input
            |> Array.collect (fun (x, y) ->
                let x = getId x

                y
                |> Array.collect (fun y ->
                    let y = getId y
                    [| x, y; y, x |]))
            |> Array.groupBy fst
            |> Array.sort
            |> Array.map (snd >> Array.map snd)

        seq {
            for x in 0 .. graph.Length - 1 do
                let gens = traverse graph (Array.replicate graph.Length Int32.MaxValue) 0 [| x |]

                let links =
                    gens
                    |> Array.indexed
                    |> Array.groupBy snd
                    |> Array.tryPick (fun (n, genN) ->
                        let links =
                            genN
                            |> Array.collect (fun (x, _) ->
                                graph[x] |> Array.choose (fun y -> if gens[y] = n - 1 then Some(x, y) else None))

                        if links.Length = 3 then Some links else None)

                match links with
                | Some links ->
                    for x, y in links do
                        graph[x] <- graph[x] |> Array.filter ((<>) y)
                        graph[y] <- graph[y] |> Array.filter ((<>) x)

                    let n =
                        traverse graph (Array.replicate graph.Length Int32.MaxValue) 0 [| x |]
                        |> Array.sumBy ((=) Int32.MaxValue >> Convert.ToInt32)

                    n * (graph.Length - n)
                | None -> ()
        }
        |> Seq.head

    do ``assert`` 54 (calculate sample)

    member _.answer = calculate data
