namespace AdventOfCode.Year2023

open Checked
open System.IO
open AdventOfCode

module Input08 =
    type Network<'A, 'State> =
        { Network: Map<string, string * string>
          Location: 'A
          State: 'State
          Advance: 'A -> (string * string -> string) -> Map<string, string * string> -> 'A
          DetermineMove: 'A -> 'State -> Result<int64, 'State> }

    let convert (lines: string[]) =
        lines[0] |> Seq.toList,
        lines[2..]
        |> Array.map (fun line -> line[0..2], (line[7..9], line[12..14]))
        |> Map.ofArray

    let sample1 =
        """
RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let sample2 =
        """
LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/08.txt") |> Seq.toArray |> convert

    let rec followDirections map =
        function
        | [] -> Error map
        | direction :: directions ->
            let direction = if direction = 'R' then snd else fst
            let location = map.Advance map.Location direction map.Network

            match map.DetermineMove location map.State with
            | Ok steps -> Ok steps
            | Error state ->
                followDirections
                    { map with
                        Location = location
                        State = state }
                    directions

    let rec repeatDirections directions map =
        match followDirections map directions with
        | Ok steps -> steps
        | Error map -> repeatDirections directions map

open Input08

type Problem08a() =

    let calculate (directions, network) =
        repeatDirections
            directions
            { Network = network
              Location = "AAA"
              State = 0L
              Advance = fun node direction network -> network |> Map.find node |> direction
              DetermineMove =
                fun node steps ->
                    let steps = steps + 1L
                    if node = "ZZZ" then Ok steps else Error steps }

    do ``assert`` 2L (calculate sample1)

    member _.answer = calculate data

type Problem08b() =
    let calculate (directions, network) =
        let locations =
            network
            |> Map.keys
            |> Seq.filter (fun (node: string) -> node.EndsWith("A"))
            |> Seq.toList

        repeatDirections
            directions
            { Network = network
              Location = locations
              State = 0L, List.replicate locations.Length None
              Advance =
                fun nodes direction network -> nodes |> List.map (fun node -> network |> Map.find node |> direction)
              DetermineMove =
                fun nodes (steps, cycles) ->
                    let steps = steps + 1L

                    let cycles =
                        List.zip cycles nodes
                        |> List.map (fun (cycle, node) ->
                            if node.EndsWith("Z") then
                                match cycle with
                                | Some(Ok _) as cycleFound -> cycleFound
                                | Some(Error start) -> Some(Ok(steps - start))
                                | None -> Some(Error steps)
                            else
                                cycle)

                    let loops = cycles |> List.choose (Option.bind Result.toOption)

                    if List.length loops = nodes.Length then
                        Ok(List.reduce (fun a b -> a * b / gcd a b) loops)
                    else
                        Error(steps, cycles) }

    do ``assert`` 6L (calculate sample2)

    member _.answer = calculate data
