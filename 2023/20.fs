namespace AdventOfCode.Year2023

open System.Collections.Generic
open Checked
open System.IO
open AdventOfCode

module Input20 =
    type Pulse =
        | Low
        | High

    type Module =
        | FlipFlop of State: Pulse
        | Conjunction of State: Map<string, Pulse>
        | Broadcast

    let convert (lines: string[]) =
        let modules =
            lines
            |> Array.map (fun line ->
                let [| name; dest |] = line.Split(" -> ")
                let dest = dest.Split(", ")

                match name[0] with
                | '%' -> name[1..], FlipFlop Low, dest
                | '&' -> name[1..], Conjunction Map.empty, dest
                | _ when name = "broadcaster" -> name, Broadcast, dest)

        let sources =
            modules
            |> Array.collect (fun (name, _, dest) -> dest |> Array.map (fun dest -> name, dest))
            |> Array.groupBy snd
            |> Array.map (fun (dest, x) -> dest, x |> Array.map (fun (source, _) -> source, Low) |> Map.ofArray)
            |> Map.ofArray

        modules
        |> Array.map (function
            | name, Conjunction _, dest -> name, struct (Conjunction sources[name], dest)
            | name, ``module``, dest -> name, struct (``module``, dest))
        |> Map.ofArray
        |> Dictionary


    let sample1 =
        """
broadcaster -> a, b, c
%a -> b
%b -> c
%c -> inv
&inv -> a
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let sample2 =
        """
broadcaster -> a
%a -> inv, con
&inv -> b
%b -> con
&con -> output
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/20.txt") |> Seq.toArray |> convert

    let rec runSignal (modules: Dictionary<_, _>) (queue: Queue<_>) telemetry telemetryData =
        match queue.TryDequeue() with
        | true, (struct (source, dest, signal) as data) ->
            match modules.TryGetValue(dest) with
            | true, struct (``module``, destinations: string array) ->
                match struct (``module``, signal) with
                | Broadcast as x, signal -> ValueSome(struct (x, signal))
                | FlipFlop _, High -> ValueNone
                | FlipFlop Low, Low -> ValueSome(struct (FlipFlop High, High))
                | FlipFlop High, Low -> ValueSome(struct (FlipFlop Low, Low))
                | Conjunction state, signal ->
                    let state = state |> Map.add source signal

                    ValueSome(
                        Conjunction state,
                        if state |> Map.forall (fun _ -> (=) High) then
                            Low
                        else
                            High
                    )
                |> function
                    | ValueSome(``module``, signal) ->
                        for dest' in destinations do
                            queue.Enqueue(dest, dest', signal)

                        modules[dest] <- ``module``, destinations
                    | ValueNone -> ()
            | false, _ -> ()

            runSignal modules queue telemetry (telemetry data telemetryData)
        | false, _ -> telemetryData

open Input20

type Problem20a() =
    let calculate input =
        let input = Dictionary(input |> Seq.cast)

        let counter (struct (_, _, signal)) (lowCount, highCount) =
            match signal with
            | Low -> lowCount + 1, highCount
            | High -> lowCount, highCount + 1

        let signal _ =
            runSignal input (Queue([ struct ("button", "broadcaster", Low) ])) counter (0, 0)

        let lowCount, highCount =
            seq { 1..1000 }
            |> Seq.map signal
            |> Seq.reduce (fun (l1, h1) (l2, h2) -> l1 + l2, h1 + h2)

        lowCount * highCount

    do ``assert`` 32000000 (calculate sample1)

    do ``assert`` 11687500 (calculate sample2)

    member _.answer = calculate data

type Problem20b() =
    let calculate input =
        let input = Dictionary(input |> Seq.cast)

        let rxSource =
            input
            |> Seq.pick (fun i ->
                match i.Value with
                | struct (Conjunction _, [| "rx" |]) -> Some i.Key
                | _ -> None)

        let rxSignal i (struct (_, dest, signal)) cycles =
            if dest = rxSource && signal = High then
                i :: cycles
            else
                cycles

        let queue = Queue()

        let rec findRxSignal i cycles =
            queue.Enqueue(struct ("button", "broadcaster", Low))

            match runSignal input queue (rxSignal i) cycles with
            | [ a; b; c; d ] -> a * b * c * d
            | cycles -> findRxSignal (i + 1L) cycles

        findRxSignal 1L []

    member _.answer = calculate data
