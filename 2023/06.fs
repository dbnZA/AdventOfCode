namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input06 =
    let convert (lines: string[]) =
        Array.zip
            (lines[0].[10..].Split(" ", StringSplitOptions.RemoveEmptyEntries)
             |> Array.map Double.Parse)
            (lines[1].[10..].Split(" ", StringSplitOptions.RemoveEmptyEntries)
             |> Array.map Double.Parse)

    let sample =
        """
Time:      7  15   30
Distance:  9  40  200
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/06.txt") |> Seq.toArray |> convert

open Input06

type Problem06a() =
    let calculate input =
        input
        |> Array.map (fun (s, m) ->
            let l = floor <| s / 2. - sqrt (s ** 2. - 4. * m) / 2. + 1.
            let u = ceil <| s / 2. + sqrt (s ** 2. - 4. * m) / 2. - 1.
            u - l + 1.)
        |> Array.reduce (*)


    do ``assert`` 288. (calculate sample)

    member _.answer = calculate data

type Problem06b() =
    let calculate input =
        let s, m = Array.unzip input
        let s = String.concat "" (s |> Array.map _.ToString()) |> Double.Parse
        let m = String.concat "" (m |> Array.map _.ToString()) |> Double.Parse
        let l = floor <| s / 2. - sqrt (s ** 2. - 4. * m) / 2. + 1.
        let u = ceil <| s / 2. + sqrt (s ** 2. - 4. * m) / 2. - 1.
        u - l + 1.

    do ``assert`` 71503. (calculate sample)

    member _.answer = calculate data
