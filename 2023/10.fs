namespace AdventOfCode.Year2023

open Checked
open System.IO
open AdventOfCode

module Input10 =
    let convert (lines: string[]) =
        lines |> Array.map Seq.toArray |> Array2D.ofArrayRows

    let sample =
        """
7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/10.txt") |> Seq.toArray |> convert

    let coords input =
        input
        |> Array2D.mapi (fun x y ->
            function
            | '|' -> (x - 1, y), (x + 1, y)
            | '-' -> (x, y + 1), (x, y - 1)
            | 'L' -> (x - 1, y), (x, y + 1)
            | 'J' -> (x - 1, y), (x, y - 1)
            | '7' -> (x + 1, y), (x, y - 1)
            | 'F' -> (x + 1, y), (x, y + 1)
            | '.'
            | 'S' -> (x, y), (x, y))

    let rec traverse (input: _[,]) n coords (x, y as current) =
        let c1, c2 = input[x, y]
        let from, _ = List.head coords

        if c1 = from then
            traverse input (n + 1) ((current, n) :: coords) c2
        elif c2 = from then
            traverse input (n + 1) ((current, n) :: coords) c1
        else
            coords

open Input10

type Problem10a() =
    let farthest input =
        let start = (input |> Array2D.findByRow 'S')

        input
        |> Array2D.permuteMoves start
        |> List.map (traverse (input |> coords) 1 [ start, 0 ])
        |> List.filter (_.Length >> (<>) 1)
        |> List.map Set.ofList
        |> Set.intersectMany
        |> Seq.maxBy snd
        |> snd

    let calculate input = farthest input

    do ``assert`` 8 (calculate sample)

    member _.answer = calculate data

type Problem10b() =
    let loop input =
        let start = (input |> Array2D.findByRow 'S')

        input
        |> Array2D.permuteMoves start
        |> List.map (traverse (input |> coords) 1 [ start, 0 ])
        |> List.filter (_.Length >> (<>) 1)
        |> List.collect (List.map fst)
        |> Set.ofList

    let rec markO (input: _[,]) =
        function
        | [] -> ()
        | (x, y) :: coords ->
            if input[x, y] = '.' then
                input[x, y] <- 'O'
                markO input (Array2D.permuteMoves (x, y) input @ coords)
            else
                markO input coords

    let calculate input =
        let m, n = Array2D.length input
        let m, n = m * 2 - 1, n * 2 - 1
        let loop = loop input

        let output =
            Array2D.init m n (fun x y ->
                if x % 2 = 1 || y % 2 = 1 || not (loop |> Set.contains (x / 2, y / 2)) then
                    '.'
                else
                    input[x / 2, y / 2])

        output
        |> coords
        |> Array2D.iteri (fun _ _ ((x1, y1 as c1), (x2, y2 as c2)) ->
            if c1 <> c2 then
                output[x1, y1] <- '#'
                output[x2, y2] <- '#')

        markO
            output
            [ for x in 0 .. m - 1 do
                  x, 0
                  x, n - 1
              for y in 0 .. n - 1 do
                  0, y
                  m - 1, y ]

        let mutable count = 0

        for x in 0..2 .. m - 1 do
            for y in 0..2 .. n - 1 do
                if output[x, y] = '.' then
                    count <- count + 1

        count

    do ``assert`` 1 (calculate sample)

    member _.answer = calculate data
