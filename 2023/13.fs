namespace AdventOfCode.Year2023

open Checked
open System.IO
open AdventOfCode

module Input13 =
    type Mirror =
        | Vertical of int
        | Horizontal of int
        | Nothing

    let convert (lines: string[]) =
        let toArray pattern =
            pattern |> Array.ofList |> Array.rev |> Array2D.ofArrayRows

        let rec toPattern pattern patterns =
            function
            | [] -> (pattern |> toArray) :: patterns
            | "" :: lines -> toPattern [] ((pattern |> toArray) :: patterns) lines
            | line :: lines -> toPattern ((line |> Seq.toArray) :: pattern) patterns lines

        toPattern [] [] (lines |> Array.toList)

    let sample =
        """
#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/13.txt") |> Seq.toArray |> convert

open Input13

type Problem13a() =
    let rec findPattern x i j (pattern: 'a array) =
        if i < 0 || j >= pattern.Length then
            if j - i > 1 then Some x else None
        elif pattern[i] = pattern[j] then
            findPattern x (i - 1) (j + 1) pattern
        else
            findPattern (x + 1) (x + 1) (x + 2) pattern

    let findMirror pattern =
        match findPattern 0 0 1 (Array2D.toRows pattern), findPattern 0 0 1 (Array2D.toColumns pattern) with
        | Some x, None -> Horizontal x
        | None, Some x -> Vertical x

    let calculate input =
        let mirrors = input |> List.map findMirror

        List.sumBy
            (function
            | Vertical x -> x + 1
            | _ -> 0)
            mirrors
        + 100
          * List.sumBy
              (function
              | Horizontal x -> x + 1
              | _ -> 0)
              mirrors


    do ``assert`` 405 (calculate sample)

    member _.answer = calculate data

type Problem13b() =
    let rec findPattern smudges x i j (pattern: 'a array array) =
        if x >= pattern.Length then
            None
        elif i < 0 || j >= pattern.Length then
            if j - i > 1 && smudges = 1 then
                Some x
            else
                findPattern 0 (x + 1) (x + 1) (x + 2) pattern
        else
            match
                Array.zip pattern[i] pattern[j]
                |> Array.sumBy (fun (x, y) -> if x <> y then 1 else 0)
            with
            | 1 when smudges = 0 -> findPattern 1 x (i - 1) (j + 1) pattern
            | 0 -> findPattern smudges x (i - 1) (j + 1) pattern
            | _ -> findPattern 0 (x + 1) (x + 1) (x + 2) pattern

    let findMirror pattern =
        match findPattern 0 0 0 1 (Array2D.toRows pattern), findPattern 0 0 0 1 (Array2D.toColumns pattern) with
        | Some x, None -> Horizontal x
        | None, Some x -> Vertical x

    let calculate input =
        let mirrors = input |> List.map findMirror

        List.sumBy
            (function
            | Vertical x -> x + 1
            | _ -> 0)
            mirrors
        + 100
          * List.sumBy
              (function
              | Horizontal x -> x + 1
              | _ -> 0)
              mirrors

    do ``assert`` 400 (calculate sample)

    member _.answer = calculate data
