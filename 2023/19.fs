namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input19 =
    type Workflow =
        | Condition of (string * char * int) * string
        | GoTo of string

    type Input =
        | Workflow of string * Workflow list
        | Part of Map<string, int>

    let (|TSeq|_|) predicate (value: string) =
        let rec seq i =
            if i >= value.Length then Some(value[..i], "")
            elif predicate value[i] then seq (i + 1)
            else Some(value[.. i - 1], value[i..])

        if predicate value[0] then seq 1 else None

    let (|TWord|_|) =
        function
        | TSeq Char.IsLetter result -> Some result
        | _ -> None

    let (|Int|) (TSeq Char.IsDigit (digits, value)) = Int32.Parse(digits), value

    let (|TLit|_|) (prefix: string) (value: string) =
        if value.StartsWith(prefix) then
            Some value[prefix.Length ..]
        else
            None

    let rec (|TOneOf|_|) prefixes value =
        match prefixes with
        | [] -> None
        | prefix :: prefixes ->
            match value with
            | TLit prefix suffix -> Some(prefix, suffix)
            | TOneOf prefixes result -> Some result
            | _ -> None

    let (|TRule|_|) =
        function
        | TWord(category, TOneOf [ "<"; ">" ] (op, Int(value, TLit ":" (TWord(dest, suffix))))) ->
            Some(Condition((category, op[0], value), dest), suffix)
        | _ -> None

    let rec (|Rules|) =
        function
        | TRule(rule, TLit "," (Rules(rules, suffix))) -> rule :: rules, suffix
        | TWord(dest, suffix) -> [ GoTo dest ], suffix

    let (|Rating|) (TOneOf [ "x"; "m"; "a"; "s" ] (category, TLit "=" (Int(value, suffix)))) = (category, value), suffix

    let rec (|Ratings|) (Rating(rating, suffix)) =
        match suffix with
        | TLit "," (Ratings(ratings, suffix)) -> rating :: ratings, suffix
        | _ -> [ rating ], suffix

    let convert (lines: string[]) =
        let lines =
            lines
            |> Array.choose (function
                | "" -> None
                | TWord(name, TLit "{" (Rules(rules, TLit "}" ""))) -> Some(Workflow(name, rules))
                | TLit "{" (Ratings(ratings, TLit "}" "")) -> Some(Part(Map.ofList ratings)))

        let system =
            lines
            |> Array.choose (function
                | Workflow(name, rules) -> Some(name, rules)
                | _ -> None)
            |> Map.ofArray

        system,
        lines
        |> Array.choose (function
            | Part part -> Some part
            | _ -> None)

    let sample =
        """
px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/19.txt") |> Seq.toArray |> convert

open Input19

type Problem19a() =
    let rec eval (system: Map<_, _>) ruleSet (part: Map<_, _>) =
        let op =
            function
            | '<' -> (<)
            | '>' -> (>)

        let rec evalRule =
            function
            | [ GoTo dest ] -> dest
            | Condition((category, condition, value), dest) :: _ when op condition part[category] value -> dest
            | _ :: rules -> evalRule rules

        match evalRule system[ruleSet] with
        | "A" -> Some part
        | "R" -> None
        | dest -> eval system dest part

    let calculate (system, parts) =
        parts
        |> Array.choose (eval system "in")
        |> Array.sumBy (fun part -> part |> Map.values |> Seq.sum)

    do ``assert`` 19114 (calculate sample)

    member _.answer = calculate data

type Problem19b() =
    let rec evalRule (part: Map<_, _>) results =
        function
        | [ GoTo dest ] -> (dest, part) :: results
        | Condition((category, condition, value), dest) :: rules ->
            let l, u = part[category]

            let (l', u'), (l, u) =
                match condition with
                | '<' -> (l, min (value - 1) u), (max value l, u)
                | '>' -> (max (value + 1) l, u), (l, min value u)

            let results =
                if l' <= u' then
                    (dest, part |> Map.add category (l', u')) :: results
                else
                    results

            if l <= u then
                evalRule (part |> Map.add category (l, u)) results rules
            else
                results

    let rec eval (system: Map<_, _>) ruleSet (part: Map<_, _>) =
        seq {
            for result in evalRule part [] system[ruleSet] do
                match result with
                | "A", part -> yield part
                | "R", _ -> ()
                | dest, part -> yield! eval system dest part
        }

    let calculate (system, _) =
        eval system "in" (Map.ofList [ "x", (1, 4000); "m", (1, 4000); "a", (1, 4000); "s", (1, 4000) ])
        |> Seq.sumBy (fun part ->
            part
            |> Map.toList
            |> List.map (fun (_, (l, u)) -> int64 (u - l + 1))
            |> List.reduce (*))


    do ``assert`` 167409079868000L (calculate sample)

    member _.answer = calculate data
