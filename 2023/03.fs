namespace AdventOfCode.Year2023

open System.Text.RegularExpressions
open Checked
open System
open System.IO
open AdventOfCode

module Input03 =
    type Schematic =
        | Number of Row: int * ColumnRange: (int * int) * Number: int
        | Symbol of Row: int * Column: int * Symbol: string

    let tokens = Regex(@"(?<Number>\d+)|(?<Symbol>[^\.])|\.+")

    let tokenize row value =
        let (|Group|_|) (name: string) (``match``: Match) =
            let group = ``match``.Groups[name]

            if group.Success then
                Some(group.Index, group.Value)
            else
                None

        tokens.Matches(value)
        |> Seq.choose (function
            | Group "Number" (index, value) -> Some(Number(row, (index, index + value.Length - 1), Int32.Parse value))
            | Group "Symbol" (index, value) -> Some(Symbol(row, index, value))
            | _ -> None)
        |> Seq.toArray

    let convert (lines: string[]) = lines |> Array.mapi tokenize

    let sample =
        """
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/03.txt") |> Seq.toArray |> convert

open Input03

type Problem03a() =
    let isAdjacent (x1, x2) x = x2 >= x - 1 && x1 <= x + 1

    let isPartNumber (x1, x2) schematics =
        schematics
        |> Array.exists (function
            | Symbol(_, x, _) -> isAdjacent (x1, x2) x
            | _ -> false)

    let calculate input =
        input
        |> Array.sumBy (fun line ->
            line
            |> Array.sumBy (function
                | Number(row, (x1, x2), n) when input[row - 1 .. row + 1] |> Array.exists (isPartNumber (x1, x2)) -> n
                | _ -> 0))

    do ``assert`` 4361 (calculate sample)

    member _.answer = calculate data

type Problem03b() =
    let isAdjacent (x1, x2) x = x2 >= x - 1 && x1 <= x + 1

    let partNumbers x schematics =
        schematics
        |> Array.choose (function
            | Number(_, (x1, x2), n) when isAdjacent (x1, x2) x -> Some n
            | _ -> None)

    let calculate input =
        input
        |> Array.sumBy (fun line ->
            line
            |> Array.sumBy (function
                | Symbol(row, x, "*") ->
                    match input[row - 1 .. row + 1] |> Array.collect (partNumbers x) with
                    | [| n1; n2 |] -> n1 * n2
                    | _ -> 0
                | _ -> 0))

    do ``assert`` 467835 (calculate sample)

    member _.answer = calculate data
