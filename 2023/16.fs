namespace AdventOfCode.Year2023

open Checked
open System.IO
open AdventOfCode

module Input16 =
    let convert (lines: string[]) =
        lines |> Array.map Seq.toArray |> Array2D.ofArrayRows

    let sample =
        """
.|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/16.txt") |> Seq.toArray |> convert

    let rec move contraption moves =
        function
        | [] -> (moves |> Set.map fst |> Set.count) - 1
        | ((x, y as c), (x', y' as d')) :: nextMoves ->
            if moves |> Set.contains (c, d') then
                move contraption moves nextMoves
            else
                let moves = moves |> Set.add (c, d')
                let x, y as c = (x + x', y + y')
                let m, n = Array2D.length contraption

                if x < 0 || x >= m || y < 0 || y >= n then
                    move contraption moves nextMoves
                else
                    let nextMoves' =
                        match contraption[x, y], d' with
                        | '.', _ -> [ c, d' ]
                        | '|', (_, 0)
                        | '-', (0, _) -> [ c, d' ]
                        | '|', (x', y')
                        | '-', (x', y') -> [ c, (y', x'); c, (-y', -x') ]
                        | '\\', (0, 1) -> [ c, (1, 0) ]
                        | '\\', (0, -1) -> [ c, (-1, 0) ]
                        | '\\', (1, 0) -> [ c, (0, 1) ]
                        | '\\', (-1, 0) -> [ c, (0, -1) ]
                        | '\\', (x', y') -> [ c, (y', x') ]
                        | '/', (0, 1) -> [ c, (-1, 0) ]
                        | '/', (0, -1) -> [ c, (1, 0) ]
                        | '/', (1, 0) -> [ c, (0, -1) ]
                        | '/', (-1, 0) -> [ c, (0, 1) ]
                        | '/', (x', y') -> [ c, (-y', -x') ]

                    move contraption moves (nextMoves' @ nextMoves)

open Input16

type Problem16a() =
    let calculate input =
        move input Set.empty [ (0, -1), (0, 1) ]

    do ``assert`` 46 (calculate sample)

    member _.answer = calculate data

type Problem16b() =
    let calculate input =
        seq {
            let m, n = Array2D.length input

            for x in 0 .. m - 1 do
                move input Set.empty [ (x, -1), (0, 1) ]
                move input Set.empty [ (x, n), (0, -1) ]

            for y in 0 .. n - 1 do
                move input Set.empty [ (-1, y), (1, 0) ]
                move input Set.empty [ (m, y), (-1, 0) ]
        }
        |> Seq.max

    do ``assert`` 51 (calculate sample)

    member _.answer = calculate data
