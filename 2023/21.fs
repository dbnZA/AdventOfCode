namespace AdventOfCode.Year2023

open System.Collections.Generic
open Checked
open System
open System.IO
open AdventOfCode

module Input21 =
    type BitArray2D =
        { Dimension: struct (int * int)
          Bits: uint array }

    module BitArray2D =
        let countTrue bitArray2d =
            let rec countBits bits value =
                if value = 0u then
                    int32 bits
                else
                    countBits (bits + (value &&& 1u)) (value >>> 1)

            Array.sumBy (countBits 0u) bitArray2d.Bits

        let empty =
            { Dimension = (0, 0)
              Bits = Array.empty }

        let isEmpty bitArray2d = Array.forall ((=) 0u) bitArray2d.Bits

        let setTrue x y bitArray2d =
            let struct (_, n) = bitArray2d.Dimension
            let z = x + y * n
            let index = z >>> 5
            let value = bitArray2d.Bits[index]
            bitArray2d.Bits[index] <- value ||| (1u <<< (z &&& 31))

        let zeroCreate m n =
            { Dimension = (m, n)
              Bits = Array.zeroCreate ((m * n + 31) >>> 5) }

    type Steps = BitArray2D

    type GridModel =
        { Interior: Steps * Steps
          Wall: (Steps * Steps) list list
          Offset: int }

    let convert (lines: string[]) =
        lines |> Array.map Seq.toArray |> Array2D.ofArrayRows

    let sample =
        """
...........
.....###.#.
.###.##..#.
..#.#...#..
....#.#....
.##..S####.
.##..#...#.
.......##..
.##.#.####.
.##..##.##.
...........
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/21.txt") |> Seq.toArray |> convert

open Input21

type Problem21a() =
    let anyStep map oldSteps =
        let m, n = Array2D.length map
        let m', n' = Array2D.length oldSteps
        let newSteps = Array2D.zeroCreate m' n'

        oldSteps
        |> Array2D.iteri (fun x y c ->
            if c then
                newSteps
                |> Array2D.permuteMoves (x, y)
                |> List.iter (fun (x, y) ->
                    if map[x % m, y % n] = '.' then
                        newSteps[x, y] <- true))

        newSteps

    let calculate n' input =
        let m, n = Array2D.length input
        let map = Array2D.copy input
        let steps = Array2D.zeroCreate m n

        map
        |> Array2D.iteri (fun x y c ->
            if c = 'S' then
                map[x, y] <- '.'
                steps[x, y] <- true)

        repeat n' (anyStep map) steps |> Array2D.sumBy Convert.ToInt32

    do ``assert`` 16 (calculate 6 sample)

    member _.answer = calculate 64 data

type Problem21b() =
    let (%) n d =
        let r = n % d
        if r < 0 then r + d else r

    let (/) n d =
        let struct (q, r) = Int32.DivRem(n, d)
        if r < 0 then q - 1 else q

    let anyStep map ((prevSteps: HashSet<_>, prevBuckets: Dictionary<_, _>), currSteps, newSteps: HashSet<_>) =
        let m, n = Array2D.length map

        let newSteps' = HashSet<_>()

        for struct (x, y) in newSteps do
            for x, y in [| x + 1, y; x - 1, y; x, y + 1; x, y - 1 |] do
                let coord = struct (x, y)

                if map[x % m, y % n] = '.' && not (prevSteps.Contains(coord)) then
                    newSteps'.Add(coord) |> ignore
                    prevSteps.Add(coord) |> ignore
                    let bucket = struct (x / m, y / n)

                    match prevBuckets.TryGetValue(bucket) with
                    | false, _ -> prevBuckets.Add(bucket, List([ coord ]))
                    | true, buckets -> buckets.Add(coord)

        currSteps, (prevSteps, prevBuckets), newSteps'

    let foreachDiag n predicate grid =
        let mid = (Array2D.length1 grid) / 2

        let rec foreachDiag i =
            if i >= min n mid then
                true
            elif
                predicate grid[mid - i, mid - n + i]
                && predicate grid[mid - n + i, mid + i]
                && predicate grid[mid + i, mid + n - i]
                && predicate grid[mid + n - i, mid - i]
            then
                foreachDiag (i + 1)
            else
                false

        foreachDiag (max 0 (n - mid))

    let rec interior n centre centre' grid =
        if foreachDiag n ((=) centre) grid then
            interior (n + 1) centre' centre grid
        else
            n

    let rec walls n wall grid =
        let mid = (Array2D.length1 grid) / 2
        let i = mid - n

        if foreachDiag n BitArray2D.isEmpty grid then
            i + 1, wall
        else
            let row =
                [ (if i < 0 then BitArray2D.empty else grid[mid, i]), grid[mid - 1, i + 1]
                  (if i < 0 then BitArray2D.empty else grid[i, mid]), grid[i + 1, mid + 1]
                  (if i < 0 then BitArray2D.empty else grid[mid, ^i]), grid[mid + 1, ^(1 + i)]
                  (if i < 0 then BitArray2D.empty else grid[^i, mid]), grid[^(1 + i), mid - 1] ]

            walls (n + 1) (row :: wall) grid

    let extrapolate grid (state: Dictionary<_, _>) n =
        let radius = (Array2D.length1 grid) / 2

        if radius < 2 then
            Error state
        else
            let centre = grid[radius, radius]
            let centre' = grid[radius, radius + 1]

            let gridModel =
                let offset, wall = walls (interior 1 centre' centre grid) [] grid

                { Interior = centre, centre'
                  Wall = wall
                  Offset = offset }

            match state.TryGetValue(gridModel) with
            | true, (radiusStart, countStart) ->
                let count n steps =
                    steps |> BitArray2D.countTrue |> int64 |> (*) n

                let length = state.Count - countStart
                let count' = n % length + countStart

                let gridModel, radius' =
                    state
                    |> Seq.pick (fun kvp ->
                        let gridModel, (radius', count) = kvp.Deconstruct()
                        if count = count' then Some(gridModel, radius') else None)

                let radius = n / length * (radius - radiusStart) + (radius' - radiusStart) + radius

                gridModel.Wall
                |> List.mapi (fun i row ->
                    let N = radius - gridModel.Offset - i - 1
                    row |> List.sumBy (fun (edgeId, lineId) -> count 1 edgeId + count N lineId))
                |> List.sum
                |> fun wall ->
                    let N = radius - gridModel.Offset - gridModel.Wall.Length + 1

                    let interior =
                        count (Seq.sum (seq { for n in 3L .. 2L .. N -> 4L * n - 4L }) + 1L) (fst gridModel.Interior)
                        + count (Seq.sum (seq { for n in 2L .. 2L .. N -> 4L * n - 4L })) (snd gridModel.Interior)

                    Ok(wall + interior)
            | false, _ ->
                state.Add(gridModel, (radius, state.Count))
                Error(state)

    let calculate n' input =
        let m, n = Array2D.length input
        let map = Array2D.copy input
        let x, y = map |> Array2D.findByColumn 'S'
        map[x, y] <- '.'

        let rec repeat n' (_, (currSteps: HashSet<_>, currBucket: Dictionary<_, _>), _ as steps) state =
            if n' = 0 then
                currSteps.Count |> int64
            else
                let radius =
                    currBucket.Keys
                    |> Seq.fold (fun n (struct (x', y')) -> max n (max (abs x') (abs y'))) 0

                let grid =
                    Array2D.init (radius * 2 + 1) (radius * 2 + 1) (fun _ _ -> BitArray2D.zeroCreate m n)

                for kvp in currBucket do
                    let (struct (x, y)), points = kvp.Deconstruct()

                    for struct (x', y') in points do
                        grid[radius + x, radius + y] |> BitArray2D.setTrue (x' - x * m) (y' - y * n)

                match extrapolate grid state n' with
                | Ok answer -> answer
                | Error cycles -> repeat (n' - 1) (anyStep map steps) cycles

        let start = struct (x, y)

        repeat
            n'
            ((HashSet(), Dictionary()),
             (HashSet([ start ]), Dictionary([ KeyValuePair(struct (0, 0), List([ start ])) ])),
             HashSet([ start ]))
            (Dictionary())

    do ``assert`` 16L (calculate 6 sample)

    do ``assert`` 50L (calculate 10 sample)

    do ``assert`` 1_594L (calculate 50 sample)

    do ``assert`` 6_536L (calculate 100 sample)

    do ``assert`` 167_004L (calculate 500 sample)

    do ``assert`` 668_697L (calculate 1000 sample)

    do ``assert`` 16_733_044L (calculate 5000 sample)

    member _.answer = calculate 26501365 data
