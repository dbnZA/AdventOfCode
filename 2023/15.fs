namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input15 =
    let convert (lines: string[]) = lines[0].Split(',')

    let sample =
        """
rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/15.txt") |> Seq.toArray |> convert

    let hash seq =
        let mutable hash = 0

        for c in seq do
            hash <- ((hash + int c) * 17) % 256

        hash

open Input15

type Problem15a() =
    let calculate input = input |> Array.sumBy hash

    do ``assert`` 52 (hash "HASH")

    do ``assert`` 1320 (calculate sample)

    member _.answer = calculate data

type Problem15b() =
    let (|Str|) predicate (str: string) =
        let rec find n =
            if n >= str.Length || not (predicate str[n]) then
                Str(str[.. n - 1], str[n..])
            else
                find (n + 1)

        find 0

    let (|Lit|_|) (value: string) (str: string) =
        if str.StartsWith(value) then
            Some str[value.Length ..]
        else
            None

    let (|Int|_|) (str: string) =
        match Int32.TryParse(str) with
        | true, n -> Some n
        | false, _ -> None

    let rec hashmap instructions (boxes: _ list[]) =
        match instructions with
        | [] -> boxes
        | Str Char.IsLetter (label, instruction) :: instructions ->
            let idx = hash label
            let box = boxes[idx]

            boxes[idx] <-
                match instruction with
                | Lit "=" (Int n) ->
                    if box |> List.exists (fst >> (=) label) then
                        box |> List.map (fun (l, _ as m) -> if l = label then l, n else m)
                    else
                        (label, n) :: box
                | Lit "-" "" -> box |> List.filter (fst >> (<>) label)

            hashmap instructions boxes

    let calculate input =
        Array.replicate 256 []
        |> hashmap (Array.toList input)
        |> Array.indexed
        |> Array.sumBy (fun (i, box) ->
            box
            |> List.rev
            |> List.indexed
            |> List.sumBy (fun (j, (_, n)) -> (i + 1) * (j + 1) * n))

    do ``assert`` 145 (calculate sample)

    member _.answer = calculate data
