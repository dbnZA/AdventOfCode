namespace AdventOfCode.Year2023

open System.Collections.Generic
open Checked
open System.IO
open AdventOfCode
open Microsoft.FSharp.Collections

module Input23 =
    let convert (lines: string[]) =
        lines |> Array.map Seq.toArray |> Array2D.ofArrayRows

    let sample =
        """
#.#####################
#.......#########...###
#######.#########.#.###
###.....#.>.>.###.#.###
###v#####.#v#.###.#.###
###.>...#.#.#.....#...#
###v###.#.#.#########.#
###...#.#.#.......#...#
#####.#.#.#######.#.###
#.....#.#.#.......#...#
#.#####.#.#.#########v#
#.#...#...#...###...>.#
#.#.#v#######v###.###v#
#...#.>.#...>.>.#.###.#
#####v#.#.###v#.#.###.#
#.....#...#...#.#.#...#
#.#########.###.#.#.###
#...###...#...#...#.###
###.###.#.###v#####v###
#...#...#.#.>.>.#.>.###
#.###.###.#.###.#.#v###
#.....###...###...#...#
#####################.#
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/23.txt") |> Seq.toArray |> convert

    [<Struct>]
    type Link =
        { Start: int * int
          End: int * int
          Length: int }

    let rec createSegments (input: _ array2d) canMove (segments: Map<_, _>) =
        function
        | [] -> Dictionary(segments)
        | struct ((x, y), _, _, _) :: paths when input[x, y] = 'O' -> createSegments input canMove segments paths
        | struct ((x, y as coord), prevCoord, start, length) :: paths ->
            input
            |> Array2D.permuteMoves coord
            |> List.filter (fun (x, y) -> input[x, y] <> '#' && (x, y) <> prevCoord)
            |> function
                | [ nextCoord ] ->
                    input[x, y] <- 'O'
                    createSegments input canMove segments ((nextCoord, coord, start, length + 1) :: paths)
                | nextPaths ->
                    let link =
                        { Start = start
                          End = coord
                          Length = length }

                    let addLink =
                        function
                        | Some links -> Some(Array.insertAt 0 link links)
                        | None -> Some [| link |]

                    let segments = segments |> Map.change start addLink

                    let segments =
                        if canMove coord prevCoord then
                            segments |> Map.change coord addLink
                        else
                            segments

                    let nextPaths =
                        nextPaths
                        |> List.choose (fun (x, y as nextCoord) ->
                            if input[x, y] <> 'O' && canMove coord nextCoord then
                                Some(struct (nextCoord, coord, coord, 0))
                            else
                                None)

                    createSegments input canMove segments (nextPaths @ paths)

    let maxPath input canMove =
        let segments =
            createSegments (Array2D.copy input) canMove Map.empty [ (1, 1), (0, 1), (0, 1), 0 ]

        let endRow = Array2D.length2 input - 1
        let input = Array2D.copy input

        let rec walk coord length =
            segments[coord]
            |> Array.map (fun link ->
                let x, y as dest = if coord = link.Start then link.End else link.Start

                if input[x, y] <> 'X' then
                    let length = length + link.Length + 1

                    if x = endRow then
                        length
                    else
                        input[x, y] <- 'X'
                        let length = walk dest length
                        input[x, y] <- '.'
                        length
                else
                    0)
            |> Array.reduce max

        walk (0, 1) 0

open Input23

type Problem23a() =
    let calculate input =
        maxPath input (fun (x1, y1) (x2, y2) ->
            match input[x2, y2], x2 - x1, y2 - y1 with
            | '.', _, _
            | '>', 0, +1
            | '<', 0, -1
            | 'v', +1, 0
            | '^', -1, 0 -> true
            | _ -> false)

    do ``assert`` 94 (calculate sample)

    member _.answer = calculate data

type Problem23b() =
    let calculate input = maxPath input (fun _ _ -> true)

    do ``assert`` 154 (calculate sample)

    member _.answer = calculate data
