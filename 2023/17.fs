namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input17 =
    let convert (lines: string[]) =
        lines
        |> Array.map Seq.toArray
        |> Array2D.ofArrayRows
        |> Array2D.map (fun x -> int x - int '0')

    let sample =
        """
2413432311323
3215453535623
3255245654254
3446585845452
4546657867536
1438598798454
4457876987766
3637877979653
4654967986887
4564679986453
1224686865563
2546548887735
4322674655533
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/17.txt") |> Seq.toArray |> convert

    let rec traverse minStep maxStep (input: _[,]) (heatLoss: _[][,]) moves =
        let m, n = Array2D.length input
        let inline isValid struct (x, y) = x >= 0 && x < m && y >= 0 && y < n

        let rec move3Steps (struct (struct (x, y), (struct (x', y') as d'), loss, steps)) moves =
            let struct (x, y) as c = struct (x + x', y + y')
            let steps = steps + 1

            if steps > maxStep || not (isValid c) then
                moves
            else
                let idx = abs x'
                let loss = loss + input[x, y]

                let moves =
                    if steps >= minStep && loss < heatLoss[x, y][idx] then
                        heatLoss[x, y][idx] <- loss

                        struct (c, struct (y', x'), loss, 0)
                        :: struct (c, struct (-y', -x'), loss, 0)
                        :: moves
                    else
                        moves

                move3Steps (c, d', loss, steps) moves

        match moves with
        | [] -> ()
        | moves ->
            moves
            |> List.collect (fun move -> move3Steps move [])
            |> List.sortBy (fun struct (_, _, loss, _) -> loss)
            |> traverse minStep maxStep input heatLoss

    let minLoss minStep maxStep input =
        let m, n = Array2D.length input
        let heatLoss = Array2D.init m n (fun _ _ -> Array.create 2 Int32.MaxValue)

        [ struct (1, 0); struct (0, 1) ]
        |> List.map (fun d' -> struct (struct (0, 0), d', 0, 0))
        |> traverse minStep maxStep input heatLoss

        heatLoss[m - 1, n - 1] |> Array.min

open Input17

type Problem17a() =
    let calculate = minLoss 1 3

    do ``assert`` 102 (calculate sample)

    member _.answer = calculate data

type Problem17b() =
    let calculate = minLoss 4 10

    do ``assert`` 94 (calculate sample)

    member _.answer = calculate data
