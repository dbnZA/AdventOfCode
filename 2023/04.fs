namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input04 =
    let convert (lines: string[]) =
        let toNumbers (line: string) =
            line.Split(" ", StringSplitOptions.RemoveEmptyEntries) |> Array.map Int32.Parse

        lines
        |> Array.map (fun line ->
            let [| winning; have |] = line.Split(": ", 2).[1].Split(" | ")
            toNumbers winning, toNumbers have)

    let sample =
        """
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/04.txt") |> Seq.toArray |> convert

open Input04

type Problem04a() =
    let calculate input =
        input
        |> Array.sumBy (fun (winning, have) ->
            Set.intersect (Set.ofArray winning) (Set.ofArray have)
            |> Set.count
            |> fun n -> if n = 0 then 0. else 2. ** float (n - 1))
        |> int

    do ``assert`` 13 (calculate sample)

    member _.answer = calculate data

type Problem04b() =
    let calculate input =
        input
        |> Array.indexed
        |> Array.fold
            (fun (counts: int[]) (i, (winning, have)) ->
                let matches = Set.intersect (Set.ofArray winning) (Set.ofArray have) |> Set.count

                for j in i + 1 .. i + matches do
                    counts[j] <- counts[j] + counts[i]

                counts)
            (Array.create input.Length 1)
        |> Array.sum

    do ``assert`` 30 (calculate sample)

    member _.answer = calculate data
