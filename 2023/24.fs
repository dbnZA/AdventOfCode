namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input24 =
    let convert (lines: string[]) =
        let (|Split|) (sep: char) (value: string) = value.Split(sep)
        let (|DoubleT|) (value: string) = Double.Parse(value.Trim())

        lines
        |> Array.map (function
            | Split '@' [| Split ',' [| DoubleT x; DoubleT y; DoubleT z |]
                           Split ',' [| DoubleT dx; DoubleT dy; DoubleT dz |] |] -> (x, dx), (y, dy), (z, dz))

    let sample =
        """
19, 13, 30 @ -2,  1, -2
18, 19, 22 @ -1, -1, -2
20, 25, 34 @ -2, -2, -4
12, 31, 28 @ -1, -2, -1
20, 19, 15 @  1, -5, -3
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/24.txt") |> Seq.toArray |> convert

open Input24

type Problem24a() =
    let (|Line|) ((x, dx), (y, dy), _) =
        let m = dy / dx
        y - m * x, m, (fun x' -> sign dx = sign (x' - x))

    let calculate input (min, max) =
        Array.allPairs input input
        |> Array.sumBy (fun (Line(c1, m1, isX1Future), Line(c2, m2, isX2Future)) ->
            if m1 <> m2 then
                let x = (c1 - c2) / (m2 - m1)
                let y = m1 * x + c1

                (isX1Future x && isX2Future x && x >= min && x <= max && y >= min && y <= max)
                |> Convert.ToInt32
            else
                0)
        |> fun n -> n / 2

    do ``assert`` 2 (calculate sample (7., 27.))

    member _.answer = calculate data (200000000000000., 400000000000000.)

type Problem24b() =
    let t ((x, dx), (y, dy), (z, dz)) ((x1, dx1), (y1, dy1), (z1, dz1)) =
        let ((x, dx), (y, dy), (z, dz)), ((x1, dx1), (y1, dy1), (z1, dz1)) =
            ((Frac.ofFloat x, Frac.ofFloat dx), (Frac.ofFloat y, Frac.ofFloat dy), (Frac.ofFloat z, Frac.ofFloat dz)),
            ((Frac.ofFloat x1, Frac.ofFloat dx1),
             (Frac.ofFloat y1, Frac.ofFloat dy1),
             (Frac.ofFloat z1, Frac.ofFloat dz1))

        match (x - x1) / (dx1 - dx), (y - y1) / (dy1 - dy), (z - z1) / (dz1 - dz) with
        | Frac.IsInteger tx, Frac.IsInteger ty, Frac.IsInteger tz -> tx = ty && ty = tz
        | _, Frac.IsInteger ty, Frac.IsInteger tz when dx1 = dx -> ty = tz
        | Frac.IsInteger tx, _, Frac.IsInteger tz when dy1 = dy -> tx = tz
        | Frac.IsInteger tx, Frac.IsInteger ty, _ when dz1 = dz -> tx = ty
        | _ -> false

    let solve (dx, dy) ((x1, dx1), (y1, dy1), (z1, dz1)) ((x2, dx2), (y2, dy2), (z2, dz2)) =
        let (dx, dy), ((x1, dx1), (y1, dy1), (z1, dz1)), ((x2, dx2), (y2, dy2), (z2, dz2)) =
            (Frac.ofFloat dx, Frac.ofFloat dy),
            ((Frac.ofFloat x1, Frac.ofFloat dx1),
             (Frac.ofFloat y1, Frac.ofFloat dy1),
             (Frac.ofFloat z1, Frac.ofFloat dz1)),
            ((Frac.ofFloat x2, Frac.ofFloat dx2),
             (Frac.ofFloat y2, Frac.ofFloat dy2),
             (Frac.ofFloat z2, Frac.ofFloat dz2))

        let o1 = (dy - dy1) / (dx - dx1)
        let o2 = (dy - dy2) / (dx - dx2)
        let x = ((y1 - y2) - (x1 * o1 - x2 * o2)) / (o2 - o1)
        let y = (x - x1) * o1 + y1

        let a1 = (x - x1) / (dx - dx1)
        let a2 = (x - x2) / (dx - dx2)
        let dz = ((z1 - z2) - (a1 * dz1 - a2 * dz2)) / (a2 - a1)
        let z = a1 * (dz - dz1) + z1

        match x, y, z, dz with
        | Frac.IsInteger x, Frac.IsInteger y, Frac.IsInteger z, Frac.IsInteger dz ->
            Some(float x, float y, (float z, float dz))
        | _ -> None

    let calculate input pad =
        let dx = input |> Array.map (fun ((_, dx), _, _) -> dx)
        let dy = input |> Array.map (fun (_, (_, dy), _) -> dy)

        [ for dx in Array.min dx - pad .. Array.max dx + pad do
              for dy in Array.min dy - pad .. Array.max dy + pad do
                  match solve (dx, dy) input[0] input[1] with
                  | Some(x, y, (z, dz)) when input |> Array.forall (t ((x, dx), (y, dy), (z, dz))) -> x + y + z
                  | _ -> () ]
        |> List.exactlyOne


    do ``assert`` 47. (calculate sample 10.)

    member _.answer = calculate data 100.
