namespace AdventOfCode.Year2023

open Checked
open System.IO
open AdventOfCode

module Input14 =
    let convert (lines: string[]) =
        lines |> Array.map Seq.toArray |> Array2D.ofArrayRows

    let sample =
        """
O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/14.txt") |> Seq.toArray |> convert

open Input14

module Common14 =
    let rec rollSlice available next slice =
        if next = Array.length slice then
            slice
        else
            match slice[available], slice[next] with
            | '.', 'O' ->
                slice[available] <- 'O'
                slice[next] <- '.'
                rollSlice (available + 1) (next + 1) slice
            | _, 'O'
            | _, '#' -> rollSlice (next + 1) (next + 1) slice
            | '.', '.' -> rollSlice available (next + 1) slice
            | _, '.' -> rollSlice next (next + 1) slice

    let rollNorth platform =
        platform
        |> Array2D.toColumns
        |> Array.map (rollSlice 0 0)
        |> Array2D.ofArrayColumns

    let northLoad platform =
        let n = platform |> Array2D.rowLength

        platform
        |> Array2D.toRows
        |> Array.mapi (fun i column -> (n - i) * Array.sumBy (fun c -> if c = 'O' then 1 else 0) column)
        |> Array.sum

open Common14

type Problem14a() =
    let calculate input = input |> rollNorth |> northLoad

    do ``assert`` 136 (calculate sample)

    member _.answer = calculate data

type Problem14b() =
    let cycle platform =
        repeat 4 (rollNorth >> Array2D.rotateRight) platform

    let calculate input =
        input |> repeat 1_000_000_000 cycle |> northLoad

    do ``assert`` 64 (calculate sample)

    member _.answer = calculate data
