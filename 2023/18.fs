namespace AdventOfCode.Year2023

open Checked
open System
open System.IO
open AdventOfCode

module Input18 =
    let convert (lines: string[]) =
        let (|Split|) (sep: string) (value: string) = value.Split(sep)
        let (|Int64|) (value: string) = Int64.Parse(value)
        let dir = Map.ofList [ '0', "R"; '1', "D"; '2', "L"; '3', "U" ]

        lines
        |> Array.map (function
            | Split " " [| d; Int64 n; h |] -> (d, n), (dir[h[^1]], Convert.ToInt64(h[2..^2], fromBase = 16)))

    let sample =
        """
R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)
        """
            .Split("\n")
        |> fun lines -> lines[1..^1]
        |> convert

    let data = File.ReadLines("2023/18.txt") |> Seq.toArray |> convert

    let lagoon input =
        let coords =
            input
            |> Array.scan
                (fun (x, y) ->
                    function
                    | "U", n -> x - n, y
                    | "D", n -> x + n, y
                    | "L", n -> x, y - n
                    | "R", n -> x, y + n)
                (0L, 0L)
            |> Array.pairwise
            |> Array.choose (fun ((x1, y1), (x2, y2)) ->
                if y1 = y2 then
                    if x1 < x2 then Some((x1, x2), y1) else Some((x2, x1), y1)
                else
                    None)

        let rec segmentArea oldHorizontal dx value newHorizontal =
            function
            | [] -> value, newHorizontal
            | (_, y1) :: (_, y2) :: lines ->
                let overlap =
                    oldHorizontal
                    |> List.sumBy (fun (y1', y2') -> max 0L (min y2 y2' - (max y1 y1') + 1L))

                segmentArea oldHorizontal dx (value + (y2 - y1 + 1L) * dx - overlap) ((y1, y2) :: newHorizontal) lines

        let rec area value oldHorizontal =
            function
            | [ _ ] -> value
            | x :: (x' :: _ as nodes) ->
                let lines =
                    coords
                    |> Array.filter (fun ((x1, x2), _) -> x1 <= x && x < x2)
                    |> Array.sortBy snd

                let value', newHorizontal =
                    segmentArea oldHorizontal (x' - x + 1L) 0L [] (Array.toList lines)

                area (value + value') newHorizontal nodes

        coords
        |> Array.collect (fun ((x1, x2), _) -> [| x1; x2 |])
        |> Array.distinct
        |> Array.sort
        |> Array.toList
        |> area 0L []

open Input18

type Problem18a() =
    let calculate input = input |> Array.map fst |> lagoon

    do ``assert`` 62L (calculate sample)

    member _.answer = calculate data

type Problem18b() =
    let calculate input = input |> Array.map snd |> lagoon

    do ``assert`` 952408144115L (calculate sample)

    member _.answer = calculate data
