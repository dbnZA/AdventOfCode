namespace AdventOfCode

[<RequireQualifiedAccess>]
module Array2D =
    let inline rowLength array = Array2D.length1 array

    let inline columnLength array = Array2D.length2 array

    let inline length array =
        array |> Array2D.length1, array |> Array2D.length2

    let inline ``base`` array =
        array |> Array2D.base1, array |> Array2D.base2

    let inline countIf ([<InlineIfLambda>] predicate) (array: _[,]) =
        let mutable count = 0
        let x0, y0 = ``base`` array
        let m, n = array |> length

        for i = 0 to m - 1 do
            for j = 0 to n - 1 do
                if predicate array[x0 + i, y0 + j] then
                    count <- count + 1

        count

    let fold folder state array =
        let mutable acc = state
        let x0, y0 = ``base`` array
        let m, n = array |> length

        for i = 0 to m - 1 do
            for j = 0 to n - 1 do
                acc <- folder acc array[x0 + i, y0 + j]

        acc

    let foldi folder state array =
        let mutable acc = state
        let m, n = array |> length

        for i = 0 to m - 1 do
            for j = 0 to n - 1 do
                acc <- folder i j acc array[i, j]

        acc

    let ofArrayRows rows =
        let rowLength = rows |> Array.length
        let columnLength = rows[0] |> Array.length
        let array = Array2D.zeroCreate rowLength columnLength

        for y = 0 to rowLength - 1 do
            for x = 0 to columnLength - 1 do
                array[y, x] <- rows[y][x]

        array

    let ofArrayColumns columns =
        let columnLength = columns |> Array.length
        let rowLength = columns[0] |> Array.length
        let array = Array2D.zeroCreate rowLength columnLength

        for y = 0 to rowLength - 1 do
            for x = 0 to columnLength - 1 do
                array[y, x] <- columns[x][y]

        array

    let permuteMoves (i, j) array =
        let x0, y0 = array |> ``base``
        let m, n = array |> length

        [ -1, 0; 1, 0; 0, -1; 0, 1 ]
        |> List.map (fun (id, jd) -> i + id, j + jd)
        |> List.filter (fun (i, j) -> i >= x0 && i < x0 + m && j >= y0 && j < y0 + n)

    let inline sum (array: 'T[,]) =
        array |> fold (+) LanguagePrimitives.GenericZero<'U>

    let inline sumBy (projection: 'T -> 'U) (array: 'T[,]) =
        array
        |> fold (fun acc v -> acc + projection v) LanguagePrimitives.GenericZero<'U>

    let toRows array =
        let x0, y0 = ``base`` array
        let m, n = array |> length
        let rows = Array.zeroCreate m

        for x = 0 to m - 1 do
            let row = Array.zeroCreate n
            rows[x] <- row

            for y = 0 to n - 1 do
                row[y] <- array[x0 + x, y0 + y]

        rows

    let toColumns array =
        let x0, y0 = ``base`` array
        let m, n = array |> length
        let columns = Array.zeroCreate n

        for y = 0 to n - 1 do
            let column = Array.zeroCreate m
            columns[y] <- column

            for x = 0 to m - 1 do
                column[x] <- array[x0 + x, y0 + y]

        columns

    let transpose array =
        let m, n = array |> length
        let transposedArray = Array2D.zeroCreate n m
        array |> Array2D.iteri (fun x y v -> transposedArray[y, x] <- v)
        transposedArray

    let tryGet (x, y) array =
        let x0, y0 = array |> ``base``
        let m, n = array |> length

        if x >= x0 && x < x0 + m && y >= y0 && y < y0 + n then
            Some array[x, y]
        else
            None

    let rotateLeft array =
        let m, n = array |> length
        let rotatedArray = Array2D.zeroCreate n m
        array |> Array2D.iteri (fun x y v -> rotatedArray[n - 1 - y, x] <- v)
        rotatedArray

    let rotateRight array =
        let m, n = array |> length
        let rotatedArray = Array2D.zeroCreate n m
        array |> Array2D.iteri (fun x y v -> rotatedArray[y, m - 1 - x] <- v)
        rotatedArray

    let max array =
        array |> toRows |> Array.map Array.max |> Array.max

    let findByRow value array =
        let xLen, yLen = length array

        seq {
            for y = 0 to yLen - 1 do
                for x = 0 to xLen - 1 do
                    if array[x, y] = value then
                        yield x, y
        }
        |> Seq.head

    let findByColumn value array =
        let xLen, yLen = length array

        seq {
            for x = 0 to xLen - 1 do
                for y = 0 to yLen - 1 do
                    if array[x, y] = value then
                        yield x, y
        }
        |> Seq.head
