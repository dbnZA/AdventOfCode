namespace AdventOfCode

[<RequireQualifiedAccess>]
module Array =
    let inline maxOf ([<InlineIfLambda>] projection) array =
        array |> Array.maxBy projection |> projection

    let inline minOf ([<InlineIfLambda>] projection) array =
        array |> Array.minBy projection |> projection

    let inline distinctOf ([<InlineIfLambda>] projection) array =
        array |> Array.distinctBy projection |> Array.map projection

    let inline countIf ([<InlineIfLambda>] predicate) (array: _ array) =
        let mutable count = 0

        for element in array do
            if predicate element then
                count <- count + 1

        count
