namespace AdventOfCode

module Array4D =
    let iteri action array =
        let d1 = array |> Array4D.length1
        let d2 = array |> Array4D.length2
        let d3 = array |> Array4D.length3
        let d4 = array |> Array4D.length4

        for i1 = 0 to d1 - 1 do
            for i2 = 0 to d2 - 1 do
                for i3 = 0 to d3 - 1 do
                    for i4 = 0 to d4 - 1 do
                        action i1 i2 i3 i4 array[i1, i2, i3, i4]

    let foldi folder state array =
        let d1 = array |> Array4D.length1
        let d2 = array |> Array4D.length2
        let d3 = array |> Array4D.length3
        let d4 = array |> Array4D.length4
        let mutable acc = state

        for i1 = 0 to d1 - 1 do
            for i2 = 0 to d2 - 1 do
                for i3 = 0 to d3 - 1 do
                    for i4 = 0 to d4 - 1 do
                        acc <- folder acc i1 i2 i3 i4 array[i1, i2, i3, i4]

        acc
