namespace AdventOfCode

[<RequireQualifiedAccess>]
module List =
    let inline maxOf ([<InlineIfLambda>] projection) list =
        list |> List.maxBy projection |> projection

    let inline minOf ([<InlineIfLambda>] projection) list =
        list |> List.minBy projection |> projection

    let inline distinctOf ([<InlineIfLambda>] projection) array =
        array |> List.distinctBy projection |> List.map projection

    let inline countIf ([<InlineIfLambda>] predicate) (array: _ list) =
        let mutable count = 0

        for element in array do
            if predicate element then
                count <- count + 1

        count
