namespace AdventOfCode

module Array3D =
    let fold folder state array =
        let d1 = array |> Array3D.length1
        let d2 = array |> Array3D.length2
        let d3 = array |> Array3D.length3
        let mutable acc = state

        for i1 = 0 to d1 - 1 do
            for i2 = 0 to d2 - 1 do
                for i3 = 0 to d3 - 1 do
                    acc <- folder acc array[i1, i2, i3]

        acc
